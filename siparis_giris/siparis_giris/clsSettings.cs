﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataSiparis;

namespace siparis_giris
{
    public class clsSettings
    {
        // mssql baglantı bilgileri
        private static string _kullaniciAdi;
        public static string KullaniciAdi { get { return _kullaniciAdi; } set { _kullaniciAdi = value; } }

        private static string _server;
        public static string Server { get { return _server; } set { _server = value; } }

        private static string _sifre;
        public static string Sifre { get { return _sifre; } set { _sifre = value; } }

        private static string _veritabani;
        public static string Veritabani { get { return _veritabani; } set { _veritabani = value; } }

        private static string _kullanici;
        public static string Kullanici { get { return _kullanici; } set { _kullanici = value; } }

    }
}
