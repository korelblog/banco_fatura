﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;

namespace siparis_giris
{
    public partial class frmFisFiltre : Form
    {
        public frmFisFiltre()
        {
            InitializeComponent();
        }

        private void FisTipiDoldur(List<tbFisTipi> listFisTipi)
        {
            ListViewItem _item = null;
            listViewFisTipi.Items.Clear();
            listViewFisTipi.Tag = null;
            for (int i = 0; i < listFisTipi.Count; i++)
            {
                _item = new ListViewItem();
                tbFisTipi _fisTipi = listFisTipi[i];
                _item.Text = (i + 1).ToString();
                _item.SubItems.Add(_fisTipi.sFisTipi);
                _item.Tag = _fisTipi;
                listViewFisTipi.Items.Add(_item);
            }
            listViewFisTipi.Tag = listFisTipi;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewFisTipi);
        }

        private void SeciliFisTipiDoldur(List<tbFisTipi> listFisTipi)
        {
            ListViewItem _item = null;
            listViewSecilenFisTipi.Items.Clear();
            listViewSecilenFisTipi.Tag = null;
            for (int i = 0; i < listFisTipi.Count; i++)
            {
                _item = new ListViewItem();
                tbFisTipi _fisTipi = listFisTipi[i];
                _item.Text = (i + 1).ToString();
                _item.SubItems.Add(_fisTipi.sFisTipi);
                _item.Tag = _fisTipi;
                listViewSecilenFisTipi.Items.Add(_item);
            }
            listViewSecilenFisTipi.Tag = listFisTipi;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenFisTipi);
        }

        #region KeyAsagi

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            for (int i = 0; i < _control.Controls.Count; i++)
                KeyAsagi(_control.Controls[i]);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    toolUrunSiparisiniSil_Click(null, null);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    Cikis();
                    break;
                case Keys.Delete:
                    toolStripButton4_Click(null, null);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    toolStripButton3_Click(null, null);
                    e.Handled = true;
                    break;
                default:
                    break;
            }
        }

        #endregion

        public List<tbFisTipi> listFisTipi = null;
        private void frmSiparisGonder_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);
            DepolariGetir();
            KullanilanDepoListesi();
        }

        private void KullanilanDepoListesi()
        {
            if (listFisTipi != null)
            {
                ListViewItem item = null;
                for (int i = 0; i < listFisTipi.Count; i++)
                {
                    item = new ListViewItem();
                    item.Text = (i + 1).ToString();
                    item.SubItems.Add(listFisTipi[i].sFisTipi);
                    item.Tag = listFisTipi[i];
                    listViewSecilenFisTipi.Items.Add(item);
                }
                listViewSecilenFisTipi.Tag = listFisTipi;
                ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenFisTipi);
            }
        }

        private void toolKapat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolUrunSiparisiniSil_Click(object sender, EventArgs e)
        {
            if (listViewFisTipi.SelectedItems.Count != 0)
            {
                ListViewItem item = null;
                bool _eklendi = false;
                for (int j = 0; j < listViewFisTipi.SelectedItems.Count; j++)
                {
                    tbFisTipi _fisTipiEkle = (tbFisTipi)listViewFisTipi.SelectedItems[j].Tag;
                    _eklendi = false;
                    for (int i = 0; i < listViewSecilenFisTipi.Items.Count; i++)
                    {
                        tbFisTipi _fisTipi = (tbFisTipi)listViewSecilenFisTipi.Items[i].Tag;
                        if (_fisTipi.sFisTipi == _fisTipiEkle.sFisTipi)
                        {
                            MessageBox.Show("Seçmiş olduğunuz " + _fisTipi.sFisTipi + " seçilen fiş tipi listesinde bulunuyor.Başka bir fiş tipi seçimi yapınız", "Fiş Tipi Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            _eklendi = true;
                            break;
                        }
                    }
                    if (!_eklendi)
                    {
                        item = new ListViewItem();
                        item.Text = ((listViewSecilenFisTipi.Items.Count + 1).ToString());
                        item.SubItems.Add(_fisTipiEkle.sFisTipi);
                        item.Tag = _fisTipiEkle;
                        listViewSecilenFisTipi.Items.Add(item);
                        List<tbFisTipi> listFisTipi = new List<tbFisTipi>();
                        for (int i = 0; i < listViewSecilenFisTipi.Items.Count; i++)
                            listFisTipi.Add((tbFisTipi)listViewSecilenFisTipi.Items[i].Tag);
                        listViewSecilenFisTipi.Tag = listFisTipi;
                        ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenFisTipi);
                    }
                }
            }
            else
                MessageBox.Show("Filtreye eklemek istediğiniz fiş tipi seçiniz.", "Fiş Tipi Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnSaticiAra_Click(object sender, EventArgs e)
        {
            DepolariGetir();
        }

        private void DepolariGetir()
        {
            this.Cursor = Cursors.WaitCursor;
            List<tbFisTipi> listFisTipi = FisTipiProvider.FisTipiListesi(txtFisTipiAd.Text);
            if (listFisTipi != null)
                FisTipiDoldur(listFisTipi);
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Aradığınız değerlere göre fiş tipi bulunamıyor.", "Fiş Tipi Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewFisTipi);
            this.Cursor = Cursors.Default;
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (listViewSecilenFisTipi.SelectedItems.Count != 0)
            {
                listViewSecilenFisTipi.Items.RemoveAt(listViewSecilenFisTipi.SelectedItems[0].Index);
                ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenFisTipi);
            }
            else
                MessageBox.Show("Filtreden çıkartmak istediğiniz fiş tipi seçiniz.", "Fiş Tipi Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Cikis()
        {
            if (listViewSecilenFisTipi.Items.Count != 0)
            {
                if (listFisTipi != null)
                    listFisTipi.Clear();
                else
                    listFisTipi = new List<tbFisTipi>();
                tbFisTipi _fisTipi = null;
                for (int i = 0; i < listViewSecilenFisTipi.Items.Count; i++)
                {
                    _fisTipi = (tbFisTipi)listViewSecilenFisTipi.Items[i].Tag;
                    listFisTipi.Add(_fisTipi);
                }
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                listFisTipi = null;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Cikis();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Cikis();
        }

        private bool _sortColumn = false;
        private void listViewSatici_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (listViewFisTipi.Tag == null)
                return;

            List<tbFisTipi> listFisTipi = null;


            try
            {
                if (listViewFisTipi.Tag.GetType() == typeof(List<tbDepo>))
                    listFisTipi = (List<tbFisTipi>)listViewFisTipi.Tag;
                else
                {
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            catch
            {
                this.Cursor = Cursors.Default;
                return;
            }

            if (listFisTipi != null)
            {
                switch (e.Column)
                {
                    case 0:
                        if (_sortColumn)
                            listFisTipi = listFisTipi.OrderBy(o => o.sFisTipi).ToList<tbFisTipi>();
                        else
                            listFisTipi = listFisTipi.OrderByDescending(o => o.sFisTipi).ToList<tbFisTipi>();
                        FisTipiDoldur(listFisTipi);
                        _sortColumn = !_sortColumn;
                        break;
                    case 1:
                        if (_sortColumn)
                            listFisTipi = listFisTipi.OrderBy(o => o.sFisTipi).ToList<tbFisTipi>();
                        else
                            listFisTipi = listFisTipi.OrderByDescending(o => o.sFisTipi).ToList<tbFisTipi>();
                        FisTipiDoldur(listFisTipi);
                        _sortColumn = !_sortColumn;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
                return;
            }
            this.Cursor = Cursors.Default;
        }

        private void listViewSecilenSatici_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (listViewSecilenFisTipi.Tag == null)
                return;

            List<tbFisTipi> listFisTipi = null;


            try
            {
                if (listViewSecilenFisTipi.Tag.GetType() == typeof(List<tbFisTipi>))
                    listFisTipi = (List<tbFisTipi>)listViewSecilenFisTipi.Tag;
                else
                {
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            catch
            {
                this.Cursor = Cursors.Default;
                return;
            }

            if (listFisTipi != null)
            {
                switch (e.Column)
                {
                    case 0:
                        if (_sortColumn)
                            listFisTipi = listFisTipi.OrderBy(o => o.sFisTipi).ToList<tbFisTipi>();
                        else
                            listFisTipi = listFisTipi.OrderByDescending(o => o.sFisTipi).ToList<tbFisTipi>();
                        SeciliFisTipiDoldur(listFisTipi);
                        _sortColumn = !_sortColumn;
                        break;
                    case 1:
                        if (_sortColumn)
                            listFisTipi = listFisTipi.OrderBy(o => o.sFisTipi).ToList<tbFisTipi>();
                        else
                            listFisTipi = listFisTipi.OrderByDescending(o => o.sFisTipi).ToList<tbFisTipi>();
                        SeciliFisTipiDoldur(listFisTipi);
                        _sortColumn = !_sortColumn;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
                return;
            }
            this.Cursor = Cursors.Default;
        }

        private void listViewSatici_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            toolUrunSiparisiniSil_Click(null, null);
        }

        private void listViewSecilenSatici_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            toolStripButton4_Click(null, null);
        }
    }
}
