﻿namespace siparis_giris
{
    partial class frmDepoFiltre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDepoFiltre));
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelAltCaption = new System.Windows.Forms.Label();
            this.labelCaption = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDepoAra = new System.Windows.Forms.Button();
            this.txtDepoAd = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listViewDepo = new System.Windows.Forms.ListView();
            this.columnBos = new System.Windows.Forms.ColumnHeader();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.listViewSecilenDepo = new System.Windows.Forms.ListView();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolUrunSiparisiniSil = new System.Windows.Forms.ToolStripButton();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.labelAltCaption);
            this.panel3.Controls.Add(this.labelCaption);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(901, 52);
            this.panel3.TabIndex = 1;
            // 
            // labelAltCaption
            // 
            this.labelAltCaption.AutoSize = true;
            this.labelAltCaption.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelAltCaption.Location = new System.Drawing.Point(37, 27);
            this.labelAltCaption.Name = "labelAltCaption";
            this.labelAltCaption.Size = new System.Drawing.Size(435, 14);
            this.labelAltCaption.TabIndex = 1;
            this.labelAltCaption.Text = "Çoklu olarak depo filtresini yapmak istediğiniz depo listesini seçiniz. ";
            // 
            // labelCaption
            // 
            this.labelCaption.AutoSize = true;
            this.labelCaption.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelCaption.Location = new System.Drawing.Point(12, 7);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(168, 14);
            this.labelCaption.TabIndex = 0;
            this.labelCaption.Text = "Çoklu Depo Listesi Filtre";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnDepoAra);
            this.groupBox1.Controls.Add(this.txtDepoAd);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(12, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(882, 62);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Depo Ara";
            // 
            // btnDepoAra
            // 
            this.btnDepoAra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDepoAra.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.btnDepoAra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDepoAra.Location = new System.Drawing.Point(629, 17);
            this.btnDepoAra.Name = "btnDepoAra";
            this.btnDepoAra.Size = new System.Drawing.Size(247, 35);
            this.btnDepoAra.TabIndex = 7;
            this.btnDepoAra.Text = "Depo Ara";
            this.btnDepoAra.UseVisualStyleBackColor = true;
            this.btnDepoAra.Click += new System.EventHandler(this.btnSaticiAra_Click);
            // 
            // txtDepoAd
            // 
            this.txtDepoAd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDepoAd.BackColor = System.Drawing.Color.White;
            this.txtDepoAd.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtDepoAd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDepoAd.Location = new System.Drawing.Point(95, 24);
            this.txtDepoAd.MaxLength = 50;
            this.txtDepoAd.Name = "txtDepoAd";
            this.txtDepoAd.Size = new System.Drawing.Size(528, 22);
            this.txtDepoAd.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(12, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 14);
            this.label6.TabIndex = 3;
            this.label6.Text = "Depo :";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listViewDepo);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(417, 305);
            this.panel1.TabIndex = 60;
            // 
            // listViewDepo
            // 
            this.listViewDepo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnBos,
            this.columnHeader1});
            this.listViewDepo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewDepo.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.listViewDepo.FullRowSelect = true;
            this.listViewDepo.GridLines = true;
            this.listViewDepo.HideSelection = false;
            this.listViewDepo.Location = new System.Drawing.Point(0, 0);
            this.listViewDepo.Name = "listViewDepo";
            this.listViewDepo.Size = new System.Drawing.Size(417, 280);
            this.listViewDepo.TabIndex = 60;
            this.listViewDepo.UseCompatibleStateImageBehavior = false;
            this.listViewDepo.View = System.Windows.Forms.View.Details;
            this.listViewDepo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewSatici_MouseDoubleClick);
            this.listViewDepo.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewSatici_ColumnClick);
            // 
            // columnBos
            // 
            this.columnBos.Text = "Sınıf Kodu";
            this.columnBos.Width = 100;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Depo Adı";
            this.columnHeader1.Width = 215;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolUrunSiparisiniSil});
            this.toolStrip1.Location = new System.Drawing.Point(0, 280);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(417, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(423, 326);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Depo Listesi";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(12, 126);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(423, 326);
            this.panel2.TabIndex = 63;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.groupBox3);
            this.panel4.Location = new System.Drawing.Point(441, 126);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(453, 326);
            this.panel4.TabIndex = 64;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.panel5);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(453, 326);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Seçilen Depo Listesi";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.listViewSecilenDepo);
            this.panel5.Controls.Add(this.toolStrip4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 18);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(447, 305);
            this.panel5.TabIndex = 60;
            // 
            // listViewSecilenDepo
            // 
            this.listViewSecilenDepo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5});
            this.listViewSecilenDepo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSecilenDepo.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.listViewSecilenDepo.FullRowSelect = true;
            this.listViewSecilenDepo.GridLines = true;
            this.listViewSecilenDepo.HideSelection = false;
            this.listViewSecilenDepo.Location = new System.Drawing.Point(0, 0);
            this.listViewSecilenDepo.MultiSelect = false;
            this.listViewSecilenDepo.Name = "listViewSecilenDepo";
            this.listViewSecilenDepo.Size = new System.Drawing.Size(447, 280);
            this.listViewSecilenDepo.TabIndex = 60;
            this.listViewSecilenDepo.UseCompatibleStateImageBehavior = false;
            this.listViewSecilenDepo.View = System.Windows.Forms.View.Details;
            this.listViewSecilenDepo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewSecilenSatici_MouseDoubleClick);
            this.listViewSecilenDepo.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewSecilenSatici_ColumnClick);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Sınıf Kodu";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Depo Adı";
            this.columnHeader5.Width = 215;
            // 
            // toolStrip4
            // 
            this.toolStrip4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4});
            this.toolStrip4.Location = new System.Drawing.Point(0, 280);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(447, 25);
            this.toolStrip4.TabIndex = 3;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // toolStrip3
            // 
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStrip3.Location = new System.Drawing.Point(0, 455);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(901, 25);
            this.toolStrip3.TabIndex = 65;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::siparis_giris.Properties.Resources.close_2;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(117, 22);
            this.toolStripButton2.Text = " Kapat ( ESC )";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton3.Image = global::siparis_giris.Properties.Resources.Ok;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(188, 22);
            this.toolStripButton3.Text = "Depo Listesi Seçildi ( F5 )";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton4.Image = global::siparis_giris.Properties.Resources.close_2;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(200, 22);
            this.toolStripButton4.Text = " Seçilen Depo Sil ( DELETE )";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolUrunSiparisiniSil
            // 
            this.toolUrunSiparisiniSil.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolUrunSiparisiniSil.Image = global::siparis_giris.Properties.Resources.Ok;
            this.toolUrunSiparisiniSil.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolUrunSiparisiniSil.Name = "toolUrunSiparisiniSil";
            this.toolUrunSiparisiniSil.Size = new System.Drawing.Size(128, 22);
            this.toolUrunSiparisiniSil.Text = "Depo Ekle ( F3 )";
            this.toolUrunSiparisiniSil.Click += new System.EventHandler(this.toolUrunSiparisiniSil_Click);
            // 
            // frmDepoFiltre
            // 
            this.AcceptButton = this.btnDepoAra;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 480);
            this.Controls.Add(this.toolStrip3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDepoFiltre";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "  Çoklu Depo Listesi Filtre";
            this.Load += new System.EventHandler(this.frmSiparisGonder_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelAltCaption;
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDepoAd;
        private System.Windows.Forms.Button btnDepoAra;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolUrunSiparisiniSil;
        private System.Windows.Forms.ListView listViewDepo;
        private System.Windows.Forms.ColumnHeader columnBos;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListView listViewSecilenDepo;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
    }
}