﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;
using System.IO;
using System.Net;

namespace siparis_giris
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void çıkışToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Programdan çıkmak istediğinizden emin misiniz ?", "Çıkış", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                return;
            else
                Application.ExitThread();
        }

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            foreach (Control _cnt in _control.Controls)
                KeyAsagi(_cnt);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    clsKisayol.FaturaListesi(this);
                    e.Handled = true;
                    break;
            }
        }

        bool _deger = false;
        private void Form1_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);
            DegerleriGetir();
            clsKisayol.FaturaListesi(this);
        }

        private void DegerleriGetir()
        {
            try
            {
                request.AutomaticDecompression = DecompressionMethods.GZip;
                request.BeginGetResponse(new AsyncCallback(FinishWebRequest), null);
            }
            catch
            {

            }
            if (!File.Exists(Application.StartupPath + "//ayarlar.xml"))
            {
                using (File.Create(Application.StartupPath + "//ayarlar.xml"))
                {

                }
                TextWriter _writer = new StreamWriter(Application.StartupPath + "//ayarlar.xml");
                _writer.Write("<ayarlar></ayarlar>");
                _writer.Close();
                _writer.Dispose();
            }
            Settings settings = new Settings();
            clsSettings.KullaniciAdi = clsGenel.Decrypt(settings.GetSetting("KullaniciAdi", "sa"));
            clsSettings.Server = clsGenel.Decrypt(settings.GetSetting("Server", "."));
            clsSettings.Sifre = clsGenel.Decrypt(settings.GetSetting("Sifre", "123456"));
            clsSettings.Veritabani = clsGenel.Decrypt(settings.GetSetting("Veritabani", "seker_musteri"));
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_deger)
                if (MessageBox.Show("Programdan çıkmak istediğinizden emin misiniz ?", "Çıkış", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    e.Cancel = true;
                else
                    Application.ExitThread();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            clsKisayol.FaturaListesi(this);
        }

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://checkip.dyndns.org");

        void FinishWebRequest(IAsyncResult result)
        {
            try
            {
                fatura_log _log = new fatura_log();
                String strHostName = string.Empty;
                strHostName = Dns.GetHostName();
                _log.host_name = strHostName;

                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                IPAddress[] addr = ipEntry.AddressList;
                for (int i = 0; i < addr.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            _log.ip_address1 = addr[i].ToString();
                            break;
                        case 1:
                            _log.ip_address2 = addr[i].ToString();
                            break;
                        case 2:
                            _log.ip_address3 = addr[i].ToString();
                            break;
                        case 3:
                            _log.ip_address4 = addr[i].ToString();
                            break;
                        case 4:
                            _log.ip_address5 = addr[i].ToString();
                            break;
                        case 5:
                            _log.ip_address6 = addr[i].ToString();
                            break;
                        case 6:
                            _log.ip_address7 = addr[i].ToString();
                            break;
                        case 7:
                            _log.ip_address8 = addr[i].ToString();
                            break;
                        case 8:
                            _log.ip_address9 = addr[i].ToString();
                            break;
                        case 9:
                            _log.ip_address10 = addr[i].ToString();
                            break;
                        default:
                            break;
                    }
                }

                WebResponse response2 = request.EndGetResponse(result);
                string html = string.Empty;
                using (Stream stream = response2.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        html = reader.ReadToEnd();
                    }
                }

                string[] a = html.Split(':');
                string a2 = a[1].Substring(1);
                string[] a3 = a2.Split('<');
                string a4 = a3[0];
                _log.external_ip = a4;
                _log.save_date = DateTime.Now;
                _log.settings = clsGenel._cnstring;

                using (CheckLKDataContext db = new CheckLKDataContext(clsGenel._cnCheckLK))
                {
                    db.fatura_logs.InsertOnSubmit(_log);
                    db.SubmitChanges();
                }
            }
            catch
            {

            }
        }
    }
}
