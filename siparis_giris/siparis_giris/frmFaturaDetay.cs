﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;
using CrystalDecisions.CrystalReports.Engine;

namespace siparis_giris
{
    public partial class frmFaturaDetay : Form
    {
        public frmFaturaDetay()
        {
            InitializeComponent();
        }

        #region KeyAsagi

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            for (int i = 0; i < _control.Controls.Count; i++)
                KeyAsagi(_control.Controls[i]);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                default:
                    break;
            }
            if ((ModifierKeys.Equals(Keys.Control) & e.KeyCode.Equals(Keys.F)))
                toolFaturaGoster_Click(null, null);
            if ((ModifierKeys.Equals(Keys.Control) & e.KeyCode.Equals(Keys.K)))
                toolStripButton2_Click(null, null);
        }

        #endregion

        public tbFatura _fatura;
        private void frmSiparisGonder_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);
            List<tbFaturaDetay> listFaturaDetayBugun = null;
            List<tbFaturaDetay> listFaturaDetay = null;
            if (_fatura.dteFisTarihi.HasValue)
            {
                if (_fatura.dteFisTarihi.Value.Date == DateTime.Now.Date)
                {
                    listFaturaDetay = new List<tbFaturaDetay>();
                    listFaturaDetayBugun = tbFaturaDetayProvider.PFFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura detay listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    listFaturaDetayBugun = tbFaturaDetayProvider.PKFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                    listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            }
            if (listFaturaDetay != null)
                FaturalariDoldur(listFaturaDetay);
            labelFaturaNumarasi.Text = _fatura.InvoiceNumber;
        }

        private void FaturalariDoldur(List<tbFaturaDetay> listFaturaDetay)
        {
            listViewUrun.Items.Clear();
            listViewUrun.Tag = null;

            ListViewItem item = null;
            for (int i = 0; i < listFaturaDetay.Count; i++)
            {
                item = new ListViewItem();
                item.Text = "";
                item.SubItems.Add(listFaturaDetay[i].InvoiceNumber);
                item.SubItems.Add(listFaturaDetay[i].sKodu);
                item.SubItems.Add(listFaturaDetay[i].sAciklama);
                item.SubItems.Add(listFaturaDetay[i].lCikisMiktar1.ToString("N0"));
                item.SubItems.Add(listFaturaDetay[i].lCikisFiyat1.ToString("N2"));
                item.SubItems.Add(listFaturaDetay[i].lCikisTutar.ToString("N2"));
                item.SubItems.Add(listFaturaDetay[i].nKdvOrani.ToString("N2"));
                item.Tag = listFaturaDetay[i];
                listViewUrun.Items.Add(item);
            }
            listViewUrun.Tag = listFaturaDetay;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
        }


        private void toolKapat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool _sortColumn = false;
        private void listViewUrun_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            List<tbFaturaDetay> listFaturaDetay = (List<tbFaturaDetay>)listViewUrun.Tag;
            switch (e.Column)
            {
                case 1: // stok kodu
                    if (_sortColumn)
                        listFaturaDetay = listFaturaDetay.OrderBy(o => o.sKodu).ToList<tbFaturaDetay>();
                    else
                        listFaturaDetay = listFaturaDetay.OrderByDescending(o => o.sKodu).ToList<tbFaturaDetay>();
                    FaturalariDoldur(listFaturaDetay);
                    _sortColumn = !_sortColumn;
                    break;
                case 2: // stok adı
                    if (_sortColumn)
                        listFaturaDetay = listFaturaDetay.OrderBy(o => o.sAciklama).ToList<tbFaturaDetay>();
                    else
                        listFaturaDetay = listFaturaDetay.OrderByDescending(o => o.sAciklama).ToList<tbFaturaDetay>();
                    FaturalariDoldur(listFaturaDetay);
                    _sortColumn = !_sortColumn;
                    break;
                case 3:
                    if (_sortColumn)
                        listFaturaDetay = listFaturaDetay.OrderBy(o => o.lCikisMiktar1).ToList<tbFaturaDetay>();
                    else
                        listFaturaDetay = listFaturaDetay.OrderByDescending(o => o.lCikisMiktar1).ToList<tbFaturaDetay>();
                    FaturalariDoldur(listFaturaDetay);
                    _sortColumn = !_sortColumn;
                    break;
                case 4:
                    if (_sortColumn)
                        listFaturaDetay = listFaturaDetay.OrderBy(o => o.lCikisFiyat1).ToList<tbFaturaDetay>();
                    else
                        listFaturaDetay = listFaturaDetay.OrderByDescending(o => o.lCikisFiyat1).ToList<tbFaturaDetay>();
                    FaturalariDoldur(listFaturaDetay);
                    _sortColumn = !_sortColumn;
                    break;
                case 5:
                    if (_sortColumn)
                        listFaturaDetay = listFaturaDetay.OrderBy(o => o.lCikisTutar).ToList<tbFaturaDetay>();
                    else
                        listFaturaDetay = listFaturaDetay.OrderByDescending(o => o.lCikisTutar).ToList<tbFaturaDetay>();
                    FaturalariDoldur(listFaturaDetay);
                    _sortColumn = !_sortColumn;
                    break;
                case 6:
                    if (_sortColumn)
                        listFaturaDetay = listFaturaDetay.OrderBy(o => o.nKdvOrani).ToList<tbFaturaDetay>();
                    else
                        listFaturaDetay = listFaturaDetay.OrderByDescending(o => o.nKdvOrani).ToList<tbFaturaDetay>();
                    FaturalariDoldur(listFaturaDetay);
                    _sortColumn = !_sortColumn;
                    break;
                default:
                    break;
            }
            this.Cursor = Cursors.Default;
        }

        EFatura reportFatura = null;
        EArsiv reportArsiv = null;
        EITArsiv reportInternet = null;
        bool _arsiv = false;
        private void toolFaturaGoster_Click(object sender, EventArgs e)
        {
            tbFatura _faturaSecili = _fatura;
            if (_faturaSecili.eInvoice.Contains("0"))
                _arsiv = false;
            else
                _arsiv = true;

            tbFaturaDepo _faturaDepo = tbFaturaDepoProvider.FaturaDepolariGetir(_fatura.sDepo);
            //List<tbFatura> listFaturaListesi = tbFaturaProvider.FaturaListesi(_fatura.InvoiceNumber);
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            List<tbFatura> listFaturaBugun = null;
            List<tbFatura> listFaturaGecmis = null;
            if (_fatura.dteFisTarihi.Value.Date >= DateTime.Now.Date)
            {
                listFaturaBugun = tbFaturaProvider.BugunFaturalariGetir(_fatura.InvoiceNumber,null,null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFFaturalariGetir(_fatura.InvoiceNumber, null, null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFASFaturalariGetir(_fatura.InvoiceNumber, null, null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(_fatura.InvoiceNumber, null);
                if (listFaturaGecmis != null)
                    listFaturaListesi.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //List<tbFaturaDetay> listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            List<tbFaturaDetay> listFaturaDetayBugun = null;
            List<tbFaturaDetay> listFaturaDetay = null;
            if (_fatura.dteFisTarihi.HasValue)
            {
                if (_fatura.dteFisTarihi.Value.Date == DateTime.Now.Date)
                {
                    listFaturaDetay = new List<tbFaturaDetay>();
                    listFaturaDetayBugun = tbFaturaDetayProvider.PFFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura detay listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    listFaturaDetayBugun = tbFaturaDetayProvider.PKFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                    listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            }

            if (_arsiv)
            {
                if (_fatura.InvoiceNumber.Contains("ITK"))
                {
                    reportInternet = new EITArsiv();
                    if (_faturaDepo != null)
                    {
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                    }
                    else
                    {
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                    }

                    ((TextObject)reportInternet.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                    ((TextObject)reportInternet.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                    ((TextObject)reportInternet.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                    ((TextObject)reportInternet.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                    if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                        ((TextObject)reportInternet.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                    else
                        ((TextObject)reportInternet.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                    switch (_fatura.Senaryo)
                    {
                        case "TEMELFATURA":
                        case "TEMEL":
                            ((TextObject)reportInternet.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                            break;
                        case "TICARIFATURA":
                        case "TICARI":
                            ((TextObject)reportInternet.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                            break;
                    }

                    if (_fatura.nGirisCikis > 0)
                    {
                        switch (_fatura.nGirisCikis)
                        {
                            case 3:
                                if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                                else
                                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                            case 2:
                                ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                                break;
                            default:
                                ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                        }
                    }
                    else
                        ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                    ((TextObject)reportInternet.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                    StringBuilder _builder = new StringBuilder();
                    _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                    _builder.Append("<br>e-Arşiv izni kapsamında elektronik ortamda iletilmiştir.");
                    _builder.Append("<br>Bu satış internet üzerinden yapılmıştır.");
                    _builder.Append("<br># Ödeme Şekli : " + _fatura.sFisTipi);
                    _builder.Append("<br># Ödeme Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : ""));
                    _builder.Append("<br># Satışın Yapıldığı Web Adres : http://www.banco.com.tr");
                    _builder.Append("<br># Gönderiyi Taşıyan VKN / TCKN : 9860008925");
                    _builder.Append("<br># Gönderiyi Taşıyan Ünvan / Ad Soyad : YURTİÇİ KARGO SERVİSİ A.Ş.");
                    _builder.Append("<br># Ürün Gönderim / Hizmet İfa Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : ""));
                    if (_fatura.nKdvOrani1 > 0)
                        _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                    if (_fatura.nKdvOrani2 > 0)
                        _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                    if (_fatura.FaturaTipi.Contains("ISTISNA"))
                        _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                    if (listFaturaListesi != null)
                    {
                        for (int i = 0; i < listFaturaListesi.Count; i++)
                            if (listFaturaListesi[i].lPesinat > 0)
                                _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                    }
                    _builder.Append("<br>" + _fatura.StokAciklama + "</div>");

                    if (listFaturaDetay != null)
                    {
                        if (listFaturaDetay.Count > 0)
                        {
                            DataTable _table = new DataTable("dtTablo");
                            _table.Columns.Add("UrunKodu", typeof(string));
                            _table.Columns.Add("MalHizmet", typeof(string));
                            _table.Columns.Add("Miktar", typeof(decimal));
                            _table.Columns.Add("BirimFiyat", typeof(decimal));
                            _table.Columns.Add("Tutar", typeof(decimal));
                            _table.Columns.Add("Note", typeof(string));
                            DataRow _row = null;
                            for (int i = 0; i < listFaturaDetay.Count; i++)
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                                _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                                _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                                _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                                _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            if (_fatura.FaturaTipi.Contains("ISTISNA"))
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = "-";
                                _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                                _row["Miktar"] = 0;
                                _row["BirimFiyat"] = 0;
                                _row["Tutar"] = 0;
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            reportInternet.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                            reportInternet.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                            reportInternet.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                            reportInternet.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                            reportInternet.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                            reportInternet.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                            reportInternet.SetDataSource(_table);
                        }
                    }
                }
                else
                {
                    reportArsiv = new EArsiv();
                    if (_faturaDepo != null)
                    {
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                    }
                    else
                    {
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                    }

                    ((TextObject)reportArsiv.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                    if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                    else
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                    switch (_fatura.Senaryo)
                    {
                        case "TEMELFATURA":
                        case "TEMEL":
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                            break;
                        case "TICARIFATURA":
                        case "TICARI":
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                            break;
                    }

                    if (_fatura.nGirisCikis > 0)
                    {
                        switch (_fatura.nGirisCikis)
                        {
                            case 3:
                                if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                                else
                                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                            case 2:
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                                break;
                            default:
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                        }
                    }
                    else
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                    ((TextObject)reportArsiv.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                    StringBuilder _builder = new StringBuilder();
                    _builder.Append("<div>YALNIZ : " + _fatura.sYaziIle);
                    if (_fatura.nKdvOrani1 > 0)
                        _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                    if (_fatura.nKdvOrani2 > 0)
                        _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                    if (_fatura.FaturaTipi.Contains("ISTISNA"))
                        _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                    if (listFaturaListesi != null)
                    {
                        for (int i = 0; i < listFaturaListesi.Count; i++)
                        {
                            if (listFaturaListesi[i].lPesinat > 0)
                                _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                        }
                    }

                    _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                    if (listFaturaDetay != null)
                    {
                        if (listFaturaDetay.Count > 0)
                        {
                            DataTable _table = new DataTable("dtTablo");
                            _table.Columns.Add("UrunKodu", typeof(string));
                            _table.Columns.Add("MalHizmet", typeof(string));
                            _table.Columns.Add("Miktar", typeof(decimal));
                            _table.Columns.Add("BirimFiyat", typeof(decimal));
                            _table.Columns.Add("Tutar", typeof(decimal));
                            _table.Columns.Add("Note", typeof(string));
                            DataRow _row = null;
                            for (int i = 0; i < listFaturaDetay.Count; i++)
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                                _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                                _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                                _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                                _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            if (_fatura.FaturaTipi.Contains("ISTISNA"))
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = "-";
                                _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                                _row["Miktar"] = 0;
                                _row["BirimFiyat"] = 0;
                                _row["Tutar"] = 0;
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            reportArsiv.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                            reportArsiv.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                            reportArsiv.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                            reportArsiv.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                            reportArsiv.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                            reportArsiv.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                            reportArsiv.SetDataSource(_table);
                        }
                    }
                }
            }
            else
            {
                reportFatura = new EFatura();
                if (_faturaDepo != null)
                {
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                }
                else
                {
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                }

                ((TextObject)reportFatura.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                ((TextObject)reportFatura.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                ((TextObject)reportFatura.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                ((TextObject)reportFatura.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                    ((TextObject)reportFatura.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                else
                    ((TextObject)reportFatura.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                switch (_fatura.Senaryo)
                {
                    case "TEMELFATURA":
                    case "TEMEL":
                        ((TextObject)reportFatura.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                        break;
                    case "TICARIFATURA":
                    case "TICARI":
                        ((TextObject)reportFatura.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                        break;
                }

                if (_fatura.nGirisCikis > 0)
                {
                    switch (_fatura.nGirisCikis)
                    {
                        case 3:
                            if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                            else
                                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                            break;
                        case 2:
                            ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                            break;
                        default:
                            ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                            break;
                    }
                }
                else
                    ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFatura.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFatura.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                ((TextObject)reportFatura.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                StringBuilder _builder = new StringBuilder();
                _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                if (_fatura.nKdvOrani1 > 0)
                    _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                if (_fatura.nKdvOrani2 > 0)
                    _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                if (_fatura.FaturaTipi.Contains("ISTISNA"))
                    _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                if (listFaturaListesi != null)
                {
                    for (int i = 0; i < listFaturaListesi.Count; i++)
                        if (listFaturaListesi[i].lPesinat > 0)
                            _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                }
                _builder.Append("<br>" + _fatura.StokAciklama + "</div>");

                if (listFaturaDetay != null)
                {
                    if (listFaturaDetay.Count > 0)
                    {
                        DataTable _table = new DataTable("dtTablo");
                        _table.Columns.Add("UrunKodu", typeof(string));
                        _table.Columns.Add("MalHizmet", typeof(string));
                        _table.Columns.Add("Miktar", typeof(decimal));
                        _table.Columns.Add("BirimFiyat", typeof(decimal));
                        _table.Columns.Add("Tutar", typeof(decimal));
                        _table.Columns.Add("Note", typeof(string));
                        DataRow _row = null;
                        for (int i = 0; i < listFaturaDetay.Count; i++)
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                            _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                            _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                            _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                            _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        if (_fatura.FaturaTipi.Contains("ISTISNA"))
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = "-";
                            _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                            _row["Miktar"] = 0;
                            _row["BirimFiyat"] = 0;
                            _row["Tutar"] = 0;
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        reportFatura.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                        reportFatura.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                        reportFatura.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                        reportFatura.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                        reportFatura.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                        reportFatura.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                        reportFatura.SetDataSource(_table);
                    }
                }
            }

            frmRaporGoster frm = new frmRaporGoster();
            frm._caption = "FATURA ÇIKTISI";
            if (_arsiv)
            {
                if (_fatura.InvoiceNumber.Contains("ITK"))
                    frm._report = reportInternet;
                else
                    frm._report = reportArsiv;
            }
            else
                frm._report = reportFatura;
            frm.WindowState = FormWindowState.Maximized;
            frm.Show();
        }

        FaturaSlip reportFaturaSlip = null;
        ArsivSlip reportArsivSlip = null;
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            tbFatura _faturaSecili = _fatura;
            if (_faturaSecili.eInvoice.Contains("0"))
                _arsiv = false;
            else
                _arsiv = true;

            tbFaturaDepo _faturaDepo = tbFaturaDepoProvider.FaturaDepolariGetir(_fatura.sDepo);
            //List<tbFatura> listFaturaListesi = tbFaturaProvider.FaturaListesi(_fatura.InvoiceNumber);
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            List<tbFatura> listFaturaBugun = null;
            List<tbFatura> listFaturaGecmis = null;
            if (_fatura.dteFisTarihi.Value.Date >= DateTime.Now.Date)
            {
                listFaturaBugun = tbFaturaProvider.BugunFaturalariGetir(_fatura.InvoiceNumber,null,null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFFaturalariGetir(_fatura.InvoiceNumber, null, null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFASFaturalariGetir(_fatura.InvoiceNumber, null, null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(_fatura.InvoiceNumber, null);
                if (listFaturaGecmis != null)
                    listFaturaListesi.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //List<tbFaturaDetay> listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            List<tbFaturaDetay> listFaturaDetayBugun = null;
            List<tbFaturaDetay> listFaturaDetay = null;
            if (_fatura.dteFisTarihi.HasValue)
            {
                if (_fatura.dteFisTarihi.Value.Date == DateTime.Now.Date)
                {
                    listFaturaDetay = new List<tbFaturaDetay>();
                    listFaturaDetayBugun = tbFaturaDetayProvider.PFFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura detay listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    listFaturaDetayBugun = tbFaturaDetayProvider.PKFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                    listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            }

            if (_arsiv)
            {
                reportArsivSlip = new ArsivSlip();
                if (_faturaDepo != null)
                {
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                }
                else
                {
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = "";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = "";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                }

                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtETTN"]).Text = _fatura.ETTN.ToUpper();
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtMusteriAd"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                    ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                else
                    ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;


                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                //((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                StringBuilder _builder = new StringBuilder();
                _builder.Append("<div style='line-height: 20px;line-height:20px;'>YALNIZ : " + _fatura.sYaziIle);
                //if (_fatura.nKdvOrani1 > 0)
                //    _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                //if (_fatura.nKdvOrani2 > 0)
                //    _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));


                //if (listFaturaListesi != null)
                //{
                //    for (int i = 0; i < listFaturaListesi.Count; i++)
                //    {
                //        if (listFaturaListesi[i].lPesinat > 0)
                //            _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                //    }
                //}

                _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                if (listFaturaDetay != null)
                {
                    if (listFaturaDetay.Count > 0)
                    {
                        DataTable _table = new DataTable("dtTablo");
                        _table.Columns.Add("UrunKodu", typeof(string));
                        _table.Columns.Add("MalHizmet", typeof(string));
                        _table.Columns.Add("Miktar", typeof(decimal));
                        _table.Columns.Add("BirimFiyat", typeof(decimal));
                        _table.Columns.Add("Tutar", typeof(decimal));
                        _table.Columns.Add("Note", typeof(string));
                        DataRow _row = null;
                        for (int i = 0; i < listFaturaDetay.Count; i++)
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                            _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                            _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                            _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                            _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        reportArsivSlip.DataDefinition.FormulaFields["strKod"].Text = "{dtTablo.UrunKodu}";
                        reportArsivSlip.DataDefinition.FormulaFields["strUrun"].Text = "{dtTablo.MalHizmet}";
                        reportArsivSlip.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                        reportArsivSlip.DataDefinition.FormulaFields["intFiyat"].Text = "{dtTablo.BirimFiyat}";
                        reportArsivSlip.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                        reportArsivSlip.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                        reportArsivSlip.SetDataSource(_table);
                    }
                }
            }
            else
            {
                reportFaturaSlip = new FaturaSlip();
                if (_faturaDepo != null)
                {
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                }
                else
                {
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = "";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = "";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                }

                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtETTN"]).Text = _fatura.ETTN.ToUpper();
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtMusteriAd"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                    ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                else
                    ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;


                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                //((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                StringBuilder _builder = new StringBuilder();
                _builder.Append("<div style='line-height: 20px;line-height:20px;'>YALNIZ : " + _fatura.sYaziIle);
                //if (_fatura.nKdvOrani1 > 0)
                //    _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                //if (_fatura.nKdvOrani2 > 0)
                //    _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));


                //if (listFaturaListesi != null)
                //{
                //    for (int i = 0; i < listFaturaListesi.Count; i++)
                //    {
                //        if (listFaturaListesi[i].lPesinat > 0)
                //            _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                //    }
                //}

                _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                if (listFaturaDetay != null)
                {
                    if (listFaturaDetay.Count > 0)
                    {
                        DataTable _table = new DataTable("dtTablo");
                        _table.Columns.Add("UrunKodu", typeof(string));
                        _table.Columns.Add("MalHizmet", typeof(string));
                        _table.Columns.Add("Miktar", typeof(decimal));
                        _table.Columns.Add("BirimFiyat", typeof(decimal));
                        _table.Columns.Add("Tutar", typeof(decimal));
                        _table.Columns.Add("Note", typeof(string));
                        DataRow _row = null;
                        for (int i = 0; i < listFaturaDetay.Count; i++)
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                            _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                            _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                            _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                            _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        reportFaturaSlip.DataDefinition.FormulaFields["strKod"].Text = "{dtTablo.UrunKodu}";
                        reportFaturaSlip.DataDefinition.FormulaFields["strUrun"].Text = "{dtTablo.MalHizmet}";
                        reportFaturaSlip.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                        reportFaturaSlip.DataDefinition.FormulaFields["intFiyat"].Text = "{dtTablo.BirimFiyat}";
                        reportFaturaSlip.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                        reportFaturaSlip.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                        reportFaturaSlip.SetDataSource(_table);
                    }
                }
            }


            frmRaporGoster frm = new frmRaporGoster();
            frm._caption = "SLIP FATURA ÇIKTISI";
            frm._slip = true;
            if (_arsiv)
                frm._report = reportArsivSlip;
            else
                frm._report = reportFaturaSlip;

            frm.WindowState = FormWindowState.Maximized;
            frm.Show();
        }
    }
}
