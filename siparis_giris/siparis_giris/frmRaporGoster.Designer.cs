﻿namespace siparis_giris
{
    partial class frmRaporGoster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRaporGoster));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolCikti = new System.Windows.Forms.ToolStripButton();
            this.toolKaydet = new System.Windows.Forms.ToolStripButton();
            this.toolKapat = new System.Windows.Forms.ToolStripButton();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolCikti,
            this.toolKaydet,
            this.toolKapat});
            this.toolStrip1.Location = new System.Drawing.Point(0, 443);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(891, 25);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolCikti
            // 
            this.toolCikti.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolCikti.Image = global::siparis_giris.Properties.Resources.printer;
            this.toolCikti.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolCikti.Name = "toolCikti";
            this.toolCikti.Size = new System.Drawing.Size(154, 22);
            this.toolCikti.Text = "Çıktı Al ( CTRL + P )";
            this.toolCikti.Click += new System.EventHandler(this.toolCikti_Click);
            // 
            // toolKaydet
            // 
            this.toolKaydet.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolKaydet.Image = global::siparis_giris.Properties.Resources.save;
            this.toolKaydet.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolKaydet.Name = "toolKaydet";
            this.toolKaydet.Size = new System.Drawing.Size(200, 22);
            this.toolKaydet.Text = "Dosya Kaydet ( CTRL + S )";
            this.toolKaydet.Click += new System.EventHandler(this.toolKaydet_Click);
            // 
            // toolKapat
            // 
            this.toolKapat.Image = global::siparis_giris.Properties.Resources.close_2;
            this.toolKapat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolKapat.Name = "toolKapat";
            this.toolKapat.Size = new System.Drawing.Size(113, 22);
            this.toolKapat.Text = "Kapat ( ESC )";
            this.toolKapat.Click += new System.EventHandler(this.toolKapat_Click);
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.DisplayGroupTree = false;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.EnableDrillDown = false;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.SelectionFormula = "";
            this.crystalReportViewer1.Size = new System.Drawing.Size(891, 443);
            this.crystalReportViewer1.TabIndex = 12;
            this.crystalReportViewer1.ViewTimeSelectionFormula = "";
            // 
            // frmRaporGoster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 468);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRaporGoster";
            this.Text = "  Raporlama";
            this.Load += new System.EventHandler(this.frmRaporGoster_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolCikti;
        private System.Windows.Forms.ToolStripButton toolKaydet;
        private System.Windows.Forms.ToolStripButton toolKapat;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
    }
}