﻿namespace siparis_giris
{
    partial class frmFaturaDetay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFaturaDetay));
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelAltCaption = new System.Windows.Forms.Label();
            this.labelCaption = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listViewUrun = new System.Windows.Forms.ListView();
            this.columnSira = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnBos = new System.Windows.Forms.ColumnHeader();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelFaturaNumarasi = new System.Windows.Forms.Label();
            this.labelSatisElemani = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolKapat = new System.Windows.Forms.ToolStripButton();
            this.toolFaturaGoster = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.labelAltCaption);
            this.panel3.Controls.Add(this.labelCaption);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(949, 52);
            this.panel3.TabIndex = 1;
            // 
            // labelAltCaption
            // 
            this.labelAltCaption.AutoSize = true;
            this.labelAltCaption.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelAltCaption.Location = new System.Drawing.Point(37, 27);
            this.labelAltCaption.Name = "labelAltCaption";
            this.labelAltCaption.Size = new System.Drawing.Size(463, 14);
            this.labelAltCaption.TabIndex = 1;
            this.labelAltCaption.Text = "Seçilen fatura numarasına göre fatura detay listesi bilgisini görebilirsiniz.";
            // 
            // labelCaption
            // 
            this.labelCaption.AutoSize = true;
            this.labelCaption.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelCaption.Location = new System.Drawing.Point(12, 7);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(148, 14);
            this.labelCaption.TabIndex = 0;
            this.labelCaption.Text = "  Fatura Detay Listesi";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listViewUrun);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(907, 323);
            this.panel1.TabIndex = 60;
            // 
            // listViewUrun
            // 
            this.listViewUrun.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnSira,
            this.columnHeader6,
            this.columnBos,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listViewUrun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewUrun.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.listViewUrun.FullRowSelect = true;
            this.listViewUrun.GridLines = true;
            this.listViewUrun.HideSelection = false;
            this.listViewUrun.Location = new System.Drawing.Point(0, 0);
            this.listViewUrun.Name = "listViewUrun";
            this.listViewUrun.Size = new System.Drawing.Size(907, 323);
            this.listViewUrun.TabIndex = 60;
            this.listViewUrun.UseCompatibleStateImageBehavior = false;
            this.listViewUrun.View = System.Windows.Forms.View.Details;
            this.listViewUrun.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewUrun_ColumnClick);
            // 
            // columnSira
            // 
            this.columnSira.Text = "";
            this.columnSira.Width = 8;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Fatura Numarası";
            // 
            // columnBos
            // 
            this.columnBos.Text = "Stok Kodu";
            this.columnBos.Width = 118;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Stok Adı";
            this.columnHeader1.Width = 124;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Miktar";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader2.Width = 147;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Fiyat";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 125;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Tutar";
            this.columnHeader4.Width = 115;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Kdv Oranı";
            this.columnHeader5.Width = 181;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(4, 43);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(913, 344);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fatura Detay";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.labelFaturaNumarasi);
            this.panel2.Controls.Add(this.labelSatisElemani);
            this.panel2.Controls.Add(this.toolStrip2);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(12, 58);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(925, 421);
            this.panel2.TabIndex = 63;
            // 
            // labelFaturaNumarasi
            // 
            this.labelFaturaNumarasi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFaturaNumarasi.AutoSize = true;
            this.labelFaturaNumarasi.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelFaturaNumarasi.ForeColor = System.Drawing.Color.Red;
            this.labelFaturaNumarasi.Location = new System.Drawing.Point(148, 14);
            this.labelFaturaNumarasi.Name = "labelFaturaNumarasi";
            this.labelFaturaNumarasi.Size = new System.Drawing.Size(0, 14);
            this.labelFaturaNumarasi.TabIndex = 80;
            // 
            // labelSatisElemani
            // 
            this.labelSatisElemani.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSatisElemani.AutoSize = true;
            this.labelSatisElemani.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelSatisElemani.ForeColor = System.Drawing.Color.Red;
            this.labelSatisElemani.Location = new System.Drawing.Point(16, 14);
            this.labelSatisElemani.Name = "labelSatisElemani";
            this.labelSatisElemani.Size = new System.Drawing.Size(126, 14);
            this.labelSatisElemani.TabIndex = 79;
            this.labelSatisElemani.Text = "Fatura Numarası :";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolKapat,
            this.toolFaturaGoster,
            this.toolStripButton2});
            this.toolStrip2.Location = new System.Drawing.Point(0, 396);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(925, 25);
            this.toolStrip2.TabIndex = 62;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolKapat
            // 
            this.toolKapat.Image = global::siparis_giris.Properties.Resources.close_2;
            this.toolKapat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolKapat.Name = "toolKapat";
            this.toolKapat.Size = new System.Drawing.Size(117, 22);
            this.toolKapat.Text = " Kapat ( ESC )";
            this.toolKapat.Click += new System.EventHandler(this.toolKapat_Click);
            // 
            // toolFaturaGoster
            // 
            this.toolFaturaGoster.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolFaturaGoster.Image = global::siparis_giris.Properties.Resources.printer;
            this.toolFaturaGoster.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFaturaGoster.Name = "toolFaturaGoster";
            this.toolFaturaGoster.Size = new System.Drawing.Size(214, 22);
            this.toolFaturaGoster.Text = "A4 Fatura Çıkart ( CTRL + F )";
            this.toolFaturaGoster.Click += new System.EventHandler(this.toolFaturaGoster_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton2.Image = global::siparis_giris.Properties.Resources._1302562097_list_edit;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(220, 22);
            this.toolStripButton2.Text = "Slip Fatura Çıkart ( CTRL + K )";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // frmFaturaDetay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 487);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmFaturaDetay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "  Fatura Detay Listesi";
            this.Load += new System.EventHandler(this.frmSiparisGonder_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelAltCaption;
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolKapat;
        private System.Windows.Forms.ListView listViewUrun;
        private System.Windows.Forms.ColumnHeader columnSira;
        private System.Windows.Forms.ColumnHeader columnBos;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label labelSatisElemani;
        private System.Windows.Forms.Label labelFaturaNumarasi;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ToolStripButton toolFaturaGoster;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}