﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Management;

namespace siparis_giris
{
    public partial class frmRaporGoster : Form
    {
        public frmRaporGoster()
        {
            InitializeComponent();
        }

        public string _caption = "";
        public string _dosyaIsmi = "";
        public bool _slip = false;
        public ReportDocument _report;
        private void frmRaporGoster_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);
            crystalReportViewer1.ReportSource = _report;
        }

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            foreach (Control _cnt in _control.Controls)
                KeyAsagi(_cnt);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                default:
                    break;
            }
        }

        private void toolKaydet_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ExportReport();
        }

        static void printProps(ManagementObject o, string prop)
        {
            try { Console.WriteLine(prop + "|" + o[prop]); }
            catch (Exception e) { Console.Write(e.ToString()); }
        }

        private void toolCikti_Click(object sender, EventArgs e)
        {
            if (_slip)
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer where Default=True");

                string printerName = "";
                foreach (ManagementObject printer in searcher.Get())
                {
                    printerName = printer["Name"].ToString();
                    printProps(printer, "WorkOffline");
                    //Console.WriteLine();
                    switch (Int32.Parse(printer["PrinterStatus"].ToString()))
                    {
                        case 1: Console.WriteLine("Other"); break;
                        case 2: Console.WriteLine("Unknown"); break;
                        case 3: Console.WriteLine("Idle"); break;
                        case 4: Console.WriteLine("Printing"); break;
                        case 5: Console.WriteLine("Warmup"); break;
                        case 6: Console.WriteLine("Stopped printing"); break;
                        case 7: Console.WriteLine("Offline"); break;
                    }
                }

                _report.PrintOptions.PrinterName = printerName;
                _report.PrintOptions.PaperSource = CrystalDecisions.Shared.PaperSource.Tractor;
                _report.PrintToPrinter(1, true, 0, 0);
            }
            else
                crystalReportViewer1.PrintReport();
        }

        private void toolKapat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
