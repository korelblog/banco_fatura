﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;

namespace siparis_giris
{
    public partial class frmMusteriFiltre : Form
    {
        public frmMusteriFiltre()
        {
            InitializeComponent();
        }

        private void MusteriBilgileri()
        {
            listViewUrun.Items.Clear();
            if (!string.IsNullOrEmpty(txtMusteriAdSoyad.Text.Trim()) || !string.IsNullOrEmpty(txtMusteriKod.Text.Trim()) || !string.IsNullOrEmpty(txtTCKimlik.Text.Trim()))
            {
                this.Cursor = Cursors.WaitCursor;
                List<tbMusteri> listMusteri = tbMusteriProvider.MusterileriGetir(txtMusteriKod.Text, txtMusteriAdSoyad.Text, txtTCKimlik.Text);
                List<tbFirma> listFirma = tbFirmaProvider.FirmalariGetir(txtMusteriKod.Text, txtMusteriAdSoyad.Text, txtTCKimlik.Text);
                if (listFirma != null)
                {
                    tbMusteri _musteriEkle = null;
                    if (listMusteri == null)
                        listMusteri = new List<tbMusteri>();
                    for (int i = 0; i < listFirma.Count; i++)
                    {
                        _musteriEkle = new tbMusteri();
                        _musteriEkle.Ad = listFirma[i].sAciklama;
                        _musteriEkle.EvSemt = listFirma[i].Semt;
                        _musteriEkle.EvSehir = listFirma[i].Il;
                        _musteriEkle.MusteriID = listFirma[i].nFirmaID;
                        _musteriEkle.EvAdresi1 = listFirma[i].Adres1;
                        _musteriEkle.EvAdresi2 = listFirma[i].Adres2;
                        _musteriEkle.EvTelefon = "";
                        _musteriEkle.GSM = "";
                        _musteriEkle.KimlikResim = "";
                        _musteriEkle.MusteriKod = listFirma[i].sKodu;
                        _musteriEkle.Soyad = "";
                        _musteriEkle.MusteriAd = _musteriEkle.Ad + " " + _musteriEkle.Soyad;
                        listMusteri.Add(_musteriEkle);
                    }
                }
                listViewUrun.Items.Clear();
                if (listMusteri != null)
                    MusterileriDoldur(listMusteri);
                else
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Aradığınız değerlere göre müşteri listesi alınamıyor.", "Müşteri Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Cursor = Cursors.Default;
            }
            else
                MessageBox.Show("Müşteri kod veya ad soyad girişi yaptıktan sonra arama gerçekleştirebilirsiniz.", "Müşteri Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void MusterileriDoldur(List<tbMusteri> listMusteri)
        {
            listViewUrun.Items.Clear();
            listViewUrun.Tag = null;

            for (int i = 0; i < listMusteri.Count; i++)
                MusteriItemAdd(listMusteri[i]);
            listViewUrun.Tag = listMusteri;
            clsGenel.Boyama(listViewUrun);
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
        }

        private void MusteriItemAdd(tbMusteri _musteri)
        {
            ListViewItem _item = new ListViewItem();
            _item.Text = _musteri.MusteriKod;
            _item.SubItems.Add(_musteri.MusteriAd);
            _item.SubItems.Add(_musteri.GSM);
            _item.SubItems.Add(_musteri.EvTelefon);
            _item.SubItems.Add(_musteri.EvAdresi1);
            _item.SubItems.Add(_musteri.EvAdresi2);
            _item.SubItems.Add(_musteri.EvSehir);
            _item.SubItems.Add(_musteri.EvSemt);
            _item.Tag = _musteri;
            listViewUrun.Items.Add(_item);
        }

        #region KeyAsagi

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            for (int i = 0; i < _control.Controls.Count; i++)
                KeyAsagi(_control.Controls[i]);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    toolUrunSiparisiniSil_Click(null, null);
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F5:
                    toolStripButton3_Click(null, null);
                    break;
                default:
                    break;
            }
        }

        #endregion

        private void frmSiparisGonder_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);
        }

        private void toolKapat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolUrunSiparisiniSil_Click(object sender, EventArgs e)
        {

        }

        private bool _sortColumn = false;
        private void listViewUrun_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (listViewUrun.Tag == null)
                return;

            List<tbMusteri> listMusteri = null;


            try
            {
                if (listViewUrun.Tag.GetType() == typeof(List<tbMusteri>))
                    listMusteri = (List<tbMusteri>)listViewUrun.Tag;
                else
                {
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            catch
            {
                this.Cursor = Cursors.Default;
                return;
            }

            if (listMusteri != null)
            {
                switch (e.Column)
                {
                    case 0:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.MusteriKod).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.MusteriKod).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    case 1:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.MusteriAd).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.MusteriAd).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    case 2:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.GSM).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.GSM).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    case 3:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.EvTelefon).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.EvTelefon).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    case 4:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.EvAdresi1).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.EvAdresi1).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    case 5:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.EvAdresi2).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.EvAdresi2).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    case 6:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.EvSehir).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.EvSehir).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    case 7:
                        if (_sortColumn)
                            listMusteri = listMusteri.OrderBy(o => o.EvSemt).ToList<tbMusteri>();
                        else
                            listMusteri = listMusteri.OrderByDescending(o => o.EvSemt).ToList<tbMusteri>();
                        MusterileriDoldur(listMusteri);
                        _sortColumn = !_sortColumn;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
                return;
            }
            this.Cursor = Cursors.Default;
        }

        public tbMusteri _musteri = null;
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (listViewUrun.SelectedItems.Count != 1)
            {
                MessageBox.Show("Müşteri seçimini yapınız.", "Müşteri Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _musteri = (tbMusteri)listViewUrun.SelectedItems[0].Tag;
            this.DialogResult = DialogResult.Yes;
        }

        private void listViewUrun_DoubleClick(object sender, EventArgs e)
        {
            if (listViewUrun.SelectedItems.Count != 1)
                return;
            _musteri = (tbMusteri)listViewUrun.SelectedItems[0].Tag;
            this.DialogResult = DialogResult.Yes;
        }

        private void btnBolumAra_Click(object sender, EventArgs e)
        {
            MusteriBilgileri();
        }
    }
}
