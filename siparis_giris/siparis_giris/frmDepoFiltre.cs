﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;

namespace siparis_giris
{
    public partial class frmDepoFiltre : Form
    {
        public frmDepoFiltre()
        {
            InitializeComponent();
        }

        private void DepoDoldur(List<tbDepo> listDepo)
        {
            ListViewItem _item = null;
            listViewDepo.Items.Clear();
            listViewDepo.Tag = null;
            for (int i = 0; i < listDepo.Count; i++)
            {
                _item = new ListViewItem();
                tbDepo _depo = listDepo[i];
                _item.Text = _depo.sDepo;
                _item.SubItems.Add(_depo.sAciklama);
                _item.Tag = _depo;
                listViewDepo.Items.Add(_item);
            }
            listViewDepo.Tag = listDepo;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewDepo);
        }

        private void SeciliDepoDoldur(List<tbDepo> listSecilenDepo)
        {
            ListViewItem _item = null;
            listViewSecilenDepo.Items.Clear();
            listViewSecilenDepo.Tag = null;
            for (int i = 0; i < listSecilenDepo.Count; i++)
            {
                _item = new ListViewItem();
                tbDepo _depo = listSecilenDepo[i];
                _item.Text = _depo.sDepo;
                _item.SubItems.Add(_depo.sAciklama);
                _item.Tag = _depo;
                listViewSecilenDepo.Items.Add(_item);
            }
            listViewSecilenDepo.Tag = listSecilenDepo;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenDepo);
        }

        #region KeyAsagi

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            for (int i = 0; i < _control.Controls.Count; i++)
                KeyAsagi(_control.Controls[i]);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    toolUrunSiparisiniSil_Click(null, null);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    Cikis();
                    break;
                case Keys.Delete:
                    toolStripButton4_Click(null, null);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    toolStripButton3_Click(null, null);
                    e.Handled = true;
                    break;
                default:
                    break;
            }
        }

        #endregion

        public List<tbDepo> listSecilenDepo = null;
        private void frmSiparisGonder_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);
            DepolariGetir();
            KullanilanDepoListesi();
        }

        private void KullanilanDepoListesi()
        {
            if (listSecilenDepo != null)
            {
                ListViewItem item = null;
                for (int i = 0; i < listSecilenDepo.Count; i++)
                {
                    item = new ListViewItem();
                    item.Text = listSecilenDepo[i].sDepo;
                    item.SubItems.Add(listSecilenDepo[i].sAciklama);
                    item.Tag = listSecilenDepo[i];
                    listViewSecilenDepo.Items.Add(item);
                }
                listViewSecilenDepo.Tag = listSecilenDepo;
                ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenDepo);
            }
        }

        private void toolKapat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolUrunSiparisiniSil_Click(object sender, EventArgs e)
        {
            if (listViewDepo.SelectedItems.Count != 0)
            {
                ListViewItem item = null;
                bool _eklendi = false;
                for (int j = 0; j < listViewDepo.SelectedItems.Count; j++)
                {
                    tbDepo _depoEkle = (tbDepo)listViewDepo.SelectedItems[j].Tag;
                    _eklendi = false;
                    for (int i = 0; i < listViewSecilenDepo.Items.Count; i++)
                    {
                        tbDepo _depo = (tbDepo)listViewSecilenDepo.Items[i].Tag;
                        if (_depo.sDepo == _depoEkle.sDepo)
                        {
                            MessageBox.Show("Seçmiş olduğunuz " + _depo.sAciklama + " seçilen depo listesinde bulunuyor.Başka bir depo seçimi yapınız", "Depo Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            _eklendi = true;
                            break;
                        }
                    }
                    if (!_eklendi)
                    {
                        item = new ListViewItem();
                        item.Text = _depoEkle.sDepo;
                        item.SubItems.Add(_depoEkle.sAciklama);
                        item.Tag = _depoEkle;
                        listViewSecilenDepo.Items.Add(item);
                        List<tbDepo> listDepoTagla = new List<tbDepo>();
                        for (int i = 0; i < listViewSecilenDepo.Items.Count; i++)
                            listDepoTagla.Add((tbDepo)listViewSecilenDepo.Items[i].Tag);
                        listViewSecilenDepo.Tag = listDepoTagla;
                        ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenDepo);
                    }
                }
            }
            else
                MessageBox.Show("Filtreye eklemek istediğiniz depo seçiniz.", "Depo Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnSaticiAra_Click(object sender, EventArgs e)
        {
            DepolariGetir();
        }

        private void DepolariGetir()
        {
            this.Cursor = Cursors.WaitCursor;
            List<tbDepo> listDepo = tbDepoProvider.DepolariniGetir(txtDepoAd.Text);
            if (listDepo != null)
                DepoDoldur(listDepo);
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Aradığınız değerlere göre depo bulunamıyor.", "Depo Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewDepo);
            this.Cursor = Cursors.Default;
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (listViewSecilenDepo.SelectedItems.Count != 0)
            {
                listViewSecilenDepo.Items.RemoveAt(listViewSecilenDepo.SelectedItems[0].Index);
                ListViewItemComparer.AutoResizeByContentAndHeader(listViewSecilenDepo);
            }
            else
                MessageBox.Show("Filtreden çıkartmak istediğiniz depo seçiniz.", "Depo Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Cikis()
        {
            if (listViewSecilenDepo.Items.Count != 0)
            {
                if (listSecilenDepo != null)
                    listSecilenDepo.Clear();
                else
                    listSecilenDepo = new List<tbDepo>();
                tbDepo _depo = null;
                for (int i = 0; i < listViewSecilenDepo.Items.Count; i++)
                {
                    _depo = (tbDepo)listViewSecilenDepo.Items[i].Tag;
                    listSecilenDepo.Add(_depo);
                }
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                listSecilenDepo = null;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Cikis();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Cikis();
        }

        private bool _sortColumn = false;
        private void listViewSatici_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (listViewDepo.Tag == null)
                return;

            List<tbDepo> listDepo = null;


            try
            {
                if (listViewDepo.Tag.GetType() == typeof(List<tbDepo>))
                    listDepo = (List<tbDepo>)listViewDepo.Tag;
                else
                {
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            catch
            {
                this.Cursor = Cursors.Default;
                return;
            }

            if (listDepo != null)
            {
                switch (e.Column)
                {
                    case 0:
                        if (_sortColumn)
                            listDepo = listDepo.OrderBy(o => o.sDepo).ToList<tbDepo>();
                        else
                            listDepo = listDepo.OrderByDescending(o => o.sDepo).ToList<tbDepo>();
                        DepoDoldur(listDepo);
                        _sortColumn = !_sortColumn;
                        break;
                    case 1:
                        if (_sortColumn)
                            listDepo = listDepo.OrderBy(o => o.sAciklama).ToList<tbDepo>();
                        else
                            listDepo = listDepo.OrderByDescending(o => o.sAciklama).ToList<tbDepo>();
                        DepoDoldur(listDepo);
                        _sortColumn = !_sortColumn;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
                return;
            }
            this.Cursor = Cursors.Default;
        }

        private void listViewSecilenSatici_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (listViewSecilenDepo.Tag == null)
                return;

            List<tbDepo> listDepo = null;


            try
            {
                if (listViewSecilenDepo.Tag.GetType() == typeof(List<tbDepo>))
                    listDepo = (List<tbDepo>)listViewSecilenDepo.Tag;
                else
                {
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            catch
            {
                this.Cursor = Cursors.Default;
                return;
            }

            if (listDepo != null)
            {
                switch (e.Column)
                {
                    case 0:
                        if (_sortColumn)
                            listDepo = listDepo.OrderBy(o => o.sDepo).ToList<tbDepo>();
                        else
                            listDepo = listDepo.OrderByDescending(o => o.sDepo).ToList<tbDepo>();
                        SeciliDepoDoldur(listDepo);
                        _sortColumn = !_sortColumn;
                        break;
                    case 1:
                        if (_sortColumn)
                            listDepo = listDepo.OrderBy(o => o.sAciklama).ToList<tbDepo>();
                        else
                            listDepo = listDepo.OrderByDescending(o => o.sAciklama).ToList<tbDepo>();
                        SeciliDepoDoldur(listDepo);
                        _sortColumn = !_sortColumn;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
                return;
            }
            this.Cursor = Cursors.Default;
        }

        private void listViewSatici_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            toolUrunSiparisiniSil_Click(null, null);
        }

        private void listViewSecilenSatici_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            toolStripButton4_Click(null, null);
        }
    }
}
