﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;
using System.Data.SqlClient;

namespace siparis_giris
{
    public partial class frmBaglantiAyarlari : Form
    {
        public frmBaglantiAyarlari()
        {
            InitializeComponent();
        }

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            for (int i = 0; i < _control.Controls.Count; i++)
                KeyAsagi(_control.Controls[i]);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    e.Handled = true;
                    break;
                case Keys.Enter:
                    SendKeys.Send("{TAB}");
                    e.Handled = true;
                    break;
                case Keys.F5:
                    toolKaydet_Click(null, null);
                    e.Handled = true;
                    break;
                case Keys.F3:
                    toolAktivasyon_Click(null, null);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    toolAyarlariSina_Click(null, null);
                    e.Handled = true;
                    break;
                default:
                    break;
            }
            if ((ModifierKeys.Equals(Keys.Control) & e.KeyCode.Equals(Keys.A)))
            {
                if (sender.GetType() == typeof(TextBox))
                {
                    TextBox _text = (TextBox)sender;
                    _text.Select(0, _text.Text.Length);
                }
            }
        }

        private void frmBaglaniAyarlari_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);

            txtKullaniciAdi.Leave += clsGenel.txtDegistir_Leave;
            txtSifre.Leave += clsGenel.txtDegistir_Leave;
            txtSunucu.Leave += clsGenel.txtDegistir_Leave;

            txtKullaniciAdi.Enter += clsGenel.txtDegistir_Enter;
            txtSifre.Enter += clsGenel.txtDegistir_Enter;
            txtSunucu.Enter += clsGenel.txtDegistir_Enter;

            string _str = "";
            try
            {
                string _server = clsSettings.Server;
                string _kullaniciAdi = clsSettings.KullaniciAdi;
                string _sifre = clsSettings.Sifre;
                string _veritabani = clsSettings.Veritabani;

                _str = "Data Source=" + _server + ";Initial Catalog=" + _veritabani + ";Persist Security Info=true;" + "Uid=" + _kullaniciAdi + ";Pwd=" + _sifre + ";";
                txtKullaniciAdi.Text = _kullaniciAdi;
                txtSifre.Text = _sifre;
                txtSunucu.Text = _server;
                comboDatabase.Text = _veritabani;
            }
            catch
            {
                clsGenel._cnstring = _str;
            }
            finally
            {
                clsGenel._cnstring = _str;
            }
            NormalAyarlariSina();
        }

        private void ButtonEnable(bool _deger)
        {
            toolAyarlariSina.Enabled = _deger;
            toolVeritabanlariniGetir.Enabled = _deger;
            toolKaydet.Enabled = _deger;
            toolBilgisayarimaKur.Enabled = _deger;
        }


        private void AyarlariSina()
        {
            labelHata.Text = "SQL Server veritabanına bağlanmaya çalışıyor...";
            string _str = "Data Source=" + txtSunucu.Text + ";Persist Security Info=true;Uid=" + txtKullaniciAdi.Text + ";Pwd=" + txtSifre.Text + ";";
            SqlConnection con = new SqlConnection(_str);
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                labelHata.Text = "SQL Server veritabanı bağlantısı sağlanmıştır.";
            }
            catch
            {
                labelHata.Text = "Girilen bilgilere göre SQL Server veritabanına bağlanamıyor.";
            }
            finally
            {
                con.Close();
            }
        }

        private void NormalAyarlariSina()
        {
            labelHata.Text = "SQL Server veritabanına bağlanmaya çalışıyor...";
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                labelHata.Text = "SQL Server veritabanı bağlantısı sağlanmıştır.";
            }
            catch
            {
                labelHata.Text = "Girilen bilgilere göre SQL Server veritabanına bağlanamıyor.";
            }
            finally
            {
                con.Close();
            }
        }

        private void Kaydet()
        {
            this.Cursor = Cursors.WaitCursor;
            string _str = "Data Source=" + txtSunucu.Text + ";Initial Catalog=" + comboDatabase.Text + ";Persist Security Info=true;" + "Uid=" + txtKullaniciAdi.Text + ";Pwd=" + txtSifre.Text + ";";
            RegistryProvider.BaglantiAyarlari(txtSunucu.Text, txtKullaniciAdi.Text, txtSifre.Text, comboDatabase.Text, this);
            clsGenel._cnstring = _str;
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void VeritabanlariniGetir()
        {
            string _str = "Data Source=" + txtSunucu.Text + ";Uid=" + txtKullaniciAdi.Text + ";Persist Security Info=true;Pwd=" + txtSifre.Text + ";";
            string command = "SELECT Name FROM sysdatabases";
            DataSet ds = new DataSet();
            SqlConnection oleConn = new SqlConnection(_str);
            SqlDataAdapter oleAdap = new SqlDataAdapter(command, oleConn);
            DataTable _table = new DataTable("dsTable");
            _table.Columns.Add("ID", typeof(string));
            _table.Columns.Add("DataBase", typeof(string));
            try
            {
                oleAdap.Fill(ds);
                DataRow _row;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    _row = _table.NewRow();
                    string _strCommand = "Data Source=" + txtSunucu.Text + ";Initial Catalog=" + ds.Tables[0].Rows[i][0].ToString() + ";Persist Security Info=true;Uid=" + txtKullaniciAdi.Text + ";Pwd=" + txtSifre.Text + ";";
                    SqlConnection _sqlConnection = new SqlConnection(_strCommand);
                    SqlCommand cmd = new SqlCommand("select count(*) from tbSiparisGiris", _sqlConnection);
                    if (_sqlConnection.State == ConnectionState.Closed)
                        _sqlConnection.Open();
                    try
                    {
                        cmd.ExecuteScalar();
                    }
                    catch
                    {
                        continue;
                    }
                    _row[0] = ds.Tables[0].Rows[i][0].ToString();
                    _row[1] = ds.Tables[0].Rows[i][0].ToString();
                    _table.Rows.Add(_row);
                }
                comboDatabase.DataSource = _table;
                comboDatabase.DisplayMember = "DataBase";
                comboDatabase.ValueMember = ds.Tables[0].Columns[0].ToString();
                oleConn.Close();
            }
            catch
            {
                labelHata.Text = "Girilen bilgilere göre SQL Server veritabanına bağlanamıyor.";
            }
            finally
            {
                oleConn.Close();
            }
        }

        string connectionstring = "";

        private void toolAktivasyon_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            labelHata.Text = "Veritabanları getiriliyor.";
            labelHata.Refresh();
            VeritabanlariniGetir();
            if (comboDatabase.Items.Count != 0)
                labelHata.Text = "Veritabanları getirildi.";
            else
                labelHata.Text = "Veritabanı bulunamadı.";
            this.Cursor = Cursors.Default;
        }

        private void toolAyarlariSina_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            labelHata.Text = "Bağlantı kontrol ediliyor.";
            labelHata.Refresh();
            ButtonEnable(false);
            AyarlariSina();
            ButtonEnable(true);
            this.Cursor = Cursors.Default;
        }

        private void toolKaydet_Click(object sender, EventArgs e)
        {
            Kaydet();
        }

        private void toolKapat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
