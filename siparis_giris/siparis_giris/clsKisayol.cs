﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace siparis_giris
{
    public class clsKisayol
    {
        public static void FaturaListesi(Form _frm)
        {
            try
            {
                for (int i = 0; i < _frm.MdiChildren.Count(); i++)
                    _frm.MdiChildren[i].Close();
            }
            catch
            {
                return;
            }
            frmFaturaRapor frm = (frmFaturaRapor)_frm.MdiChildren.SingleOrDefault(o => o.Text == "  E-Arşiv ve E-Fatura Listesi");
            if (frm != null)
                frm.Close();
            frm = new frmFaturaRapor();
            frm.MdiParent = _frm;
            frm.MinimizeBox = false;
            frm.WindowState = FormWindowState.Maximized;
            frm.Show();
            _frm.Width = _frm.Width + 1;
        }
    }
}
