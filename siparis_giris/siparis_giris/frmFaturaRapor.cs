﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;
using CrystalDecisions.CrystalReports.Engine;
using System.Drawing.Printing;
using System.Management;

namespace siparis_giris
{
    public partial class frmFaturaRapor : Form
    {
        public frmFaturaRapor()
        {
            InitializeComponent();
        }

        private void btnUrunAra_Click(object sender, EventArgs e)
        {
            listViewUrun.Items.Clear();
            this.Cursor = Cursors.WaitCursor;
            List<tbFatura> listFatura = new List<tbFatura>();
            List<tbFatura> listFaturaBugun = null;
            List<tbFatura> listFaturaGecmis = null;
            string _invoiceNumber = null;
            bool _ara = false;
            if (listFisTipi != null)
            {
                if (listFisTipi.Count > 0)
                    _ara = true;
            }
            if (!_ara)
            {
                MessageBox.Show("Fiş filtresinden fiş tipi seçmelisiniz.", "Fiş Tipi Seçiniz", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Default;
                return;
            }
            if (dateSatisTarihiBit.Value.Date >= DateTime.Now.Date)
            {
                listFaturaBugun = tbFaturaProvider.BugunFaturalariGetir(_invoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFatura.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFFaturalariGetir(_invoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFatura.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFASFaturalariGetir(_invoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFatura.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(dateSatisTarihiBas.Value.Date, DateTime.Now.Date.AddDays(-1).Date, _invoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaGecmis != null)
                    listFatura.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            else
            {
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(dateSatisTarihiBas.Value.Date, dateSatisTarihiBit.Value.Date, _invoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaGecmis != null)
                    listFatura.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            if (radioDetayli.Checked)
                FaturaView();
            else if (radioKumulatif.Checked)
                FaturaKumulatifView();
            else
                FaturaView();
            if (listFatura != null)
            {
                if (radioDetayli.Checked)
                    FaturalariDoldur(listFatura.OrderBy(o => o.dteFisTarihi).ToList<tbFatura>());
                else if (radioKumulatif.Checked)
                    FaturaKumulatifleriDoldur(listFatura.OrderBy(o => o.dteFisTarihi).ToList<tbFatura>());
                else
                    FaturalariDoldur(listFatura.OrderBy(o => o.dteFisTarihi).ToList<tbFatura>());
            }
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Aradığınız değerlere göre fatura listesi alınamıyor.", "E-Arşiv ve E-Fatura Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Cursor = Cursors.Default;
        }

        // Fatura Hedef
        private void FaturaView()
        {
            listViewUrun.Items.Clear();
            listViewUrun.Columns.Clear();

            groupBox2.Text = "E-Arşiv ve E-Fatura ";
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Sıra" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Invoice Number" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sFisTipi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "dteFisTarihi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lToplamMiktar" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdv1" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "nKdvOrani1" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdvMatrahi1" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv1 Toplam" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdv2" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "nKdvOrani2" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdvMatrahi2" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv2 Toplam" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdvsiz Toplam" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv Toplam" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lNetTutar" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sYaziIle" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "EINVOICE" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sKodu" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sAdi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sSoyadi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sCuzdanKayitNo" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sVergiDairesi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sVergiNo" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lPesinat" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sAciklama" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "lPesinat1" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "sAciklama1" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "dteFisKayitTarihi" });
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
        }

        private void FaturaKumulatifView()
        {
            listViewUrun.Items.Clear();
            listViewUrun.Columns.Clear();

            groupBox2.Text = "E-Arşiv ve E-Fatura ";
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Sıra" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Fatura No" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "sFisTipi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Fatura Tarihi" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lToplamMiktar" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdv1" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "nKdvOrani1" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdvMatrahi1" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv1 Toplam" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdv2" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "nKdvOrani2" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lKdvMatrahi2" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv2 Toplam" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdvsiz Toplam" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv Toplam" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lNetTutar" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "sYaziIle" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "EINVOICE" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kodu" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Adı Soyadı" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "sSoyadi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "TC Kimlik No" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Vergi Dairesi" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Vergi No" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lPesinat" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "sAciklama" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv Toplam" }) // olucak
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdvsiz Toplam" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kdv Toplam" });
            //listViewUrun.Columns.Add(new ColumnHeader() { Text = "lNetTutar" }); // olucak
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Net Tutar" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Kayıt Tarihi" });
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
        }

        private void FaturalariDoldur(List<tbFatura> listFatura)
        {
            listViewUrun.Items.Clear();
            listViewUrun.Tag = null;

            txtToplamKdv.Text = "";
            txtToplamKdvsiz.Text = "";
            txtToplamTutar.Text = "";

            List<tbFatura> listFaturaGoster = new List<tbFatura>();
            List<tbFatura> listFaturaDuzen = null;
            decimal _krediKart = 0;
            decimal _nakit = 0;
            for (int j = 0; j < listFatura.Count; j++)
            {
                _krediKart = 0;
                _nakit = 0;
                if (listFaturaGoster.Where(o => o.InvoiceNumber == listFatura[j].InvoiceNumber).Count<tbFatura>() != 0)
                    continue;
                listFaturaDuzen = listFatura.Where(o => o.InvoiceNumber == listFatura[j].InvoiceNumber).ToList<tbFatura>();
                if (listFaturaDuzen.Count > 1)
                {
                    _krediKart = listFaturaDuzen.Where(o => o.OdemeTipi == 2).Sum(o => o.lPesinat);
                    _nakit = listFaturaDuzen.Where(o => o.OdemeTipi == 1).Sum(o => o.lPesinat);
                    if (_krediKart > 0 || _nakit > 0)
                    {
                        listFatura[j].lPesinat = _nakit;
                        listFatura[j].OdemeAciklama = "NAKİT";
                        listFatura[j].lPesinat1 = _krediKart;
                        listFatura[j].OdemeAciklama1 = "POS";
                    }
                }
                listFaturaGoster.Add(listFatura[j]);
            }

            ListViewItem item = null;
            decimal _toplamKdvsiz = 0;
            decimal _toplamKdv = 0;
            decimal _toplamTutar = 0;
            int _toplamFaturaAdedi = 0;
            for (int i = 0; i < listFaturaGoster.Count; i++)
            {
                item = new ListViewItem();
                item.Text = (i + 1).ToString();
                item.SubItems.Add(listFaturaGoster[i].InvoiceNumber.Replace(" ", ""));
                switch (listFaturaGoster[i].sFisTip)
                {
                    case "SP":
                    case "P":
                        if (listFaturaGoster[i].lPesinat > listFaturaGoster[i].lPesinat1)
                            item.SubItems.Add("PEŞİN NAKİT");
                        else
                            item.SubItems.Add("PEŞİN KREDİ KARTI");
                        break;
                    default:
                        item.SubItems.Add(listFaturaGoster[i].sFisTipi);
                        break;
                }
                //switch (listFaturaGoster[i].OdemeTipi)
                //{
                //    case 1:
                //    case 2:

                //        break;
                //    default:
                //        item.SubItems.Add(listFaturaGoster[i].sFisTipi);
                //        break;
                //}
                item.SubItems.Add(listFaturaGoster[i].dteFisTarihi.HasValue ? listFaturaGoster[i].dteFisTarihi.Value.Date.ToShortDateString() : "");
                item.SubItems.Add(listFaturaGoster[i].lToplamMiktar > 0 ? listFaturaGoster[i].lToplamMiktar.ToString("N2") : (listFaturaGoster[i].lToplamMiktar * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lKdv1 > 0 ? listFaturaGoster[i].lKdv1.ToString("N2") : (listFaturaGoster[i].lKdv1 * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].nKdvOrani1 > 0 ? listFaturaGoster[i].nKdvOrani1.ToString("N2") : (listFaturaGoster[i].nKdvOrani1 * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lKdvMatrahi1 > 0 ? listFaturaGoster[i].lKdvMatrahi1.ToString("N2") : (listFaturaGoster[i].lKdvMatrahi1 * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lKdv1Toplam > 0 ? listFaturaGoster[i].lKdv1Toplam.ToString("N2") : (listFaturaGoster[i].lKdv1Toplam * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lKdv2 > 0 ? listFaturaGoster[i].lKdv2.ToString("N2") : (listFaturaGoster[i].lKdv2 * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].nKdvOrani2 > 0 ? listFaturaGoster[i].nKdvOrani2.ToString("N2") : (listFaturaGoster[i].nKdvOrani2 * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lKdvMatrahi2 > 0 ? listFaturaGoster[i].lKdvMatrahi2.ToString("N2") : (listFaturaGoster[i].lKdvMatrahi2 * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lKdv2Toplam > 0 ? listFaturaGoster[i].lKdv2Toplam.ToString("N2") : (listFaturaGoster[i].lKdv2Toplam * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].KdvsizToplam > 0 ? listFaturaGoster[i].KdvsizToplam.ToString("N2") : (listFaturaGoster[i].KdvsizToplam * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].KdvToplam > 0 ? listFaturaGoster[i].KdvToplam.ToString("N2") : (listFaturaGoster[i].KdvToplam * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lNetTutar > 0 ? listFaturaGoster[i].lNetTutar.ToString("N2") : (listFaturaGoster[i].lNetTutar * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].sYaziIle);
                item.SubItems.Add(listFaturaGoster[i].eInvoice);
                item.SubItems.Add(listFaturaGoster[i].sKodu);
                item.SubItems.Add(listFaturaGoster[i].sAciklama);
                item.SubItems.Add(listFaturaGoster[i].sSoyadi);
                item.SubItems.Add(listFaturaGoster[i].sCuzdanKayitNO);
                item.SubItems.Add(listFaturaGoster[i].sVergiDairesi);
                item.SubItems.Add(listFaturaGoster[i].sVergiNo);
                if (listFaturaGoster[i].lPesinat > 0)
                {
                    item.SubItems.Add(listFaturaGoster[i].lPesinat > 0 ? listFaturaGoster[i].lPesinat.ToString("N2") : (listFaturaGoster[i].lPesinat * (-1)).ToString("N2"));
                    item.SubItems.Add(listFaturaGoster[i].OdemeAciklama);
                }
                else
                {
                    if (listFaturaGoster[i].lPesinat1 > 0)
                    {
                        item.SubItems.Add(listFaturaGoster[i].lPesinat1 > 0 ? listFaturaGoster[i].lPesinat1.ToString("N2") : (listFaturaGoster[i].lPesinat1 * (-1)).ToString("N2"));
                        item.SubItems.Add(listFaturaGoster[i].OdemeAciklama1);
                    }
                    else
                    {
                        item.SubItems.Add("");
                        item.SubItems.Add("");
                    }
                }
                if (listFaturaGoster[i].lPesinat > 0)
                {
                    if (listFaturaGoster[i].lPesinat1 > 0)
                    {
                        item.SubItems.Add(listFaturaGoster[i].lPesinat1 > 0 ? listFaturaGoster[i].lPesinat1.ToString("N2") : (listFaturaGoster[i].lPesinat1 * (-1)).ToString("N2"));
                        item.SubItems.Add(listFaturaGoster[i].OdemeAciklama1);
                    }
                    else
                    {
                        item.SubItems.Add("");
                        item.SubItems.Add("");
                    }
                }
                else
                {
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                }
                item.SubItems.Add(listFaturaGoster[i].dteFisKayitTarihi.HasValue ? listFaturaGoster[i].dteFisKayitTarihi.Value.ToShortDateString() + " - " + listFaturaGoster[i].dteFisKayitTarihi.Value.ToShortTimeString() : "");
                _toplamKdvsiz += listFaturaGoster[i].KdvsizToplam > 0 ? listFaturaGoster[i].KdvsizToplam : (listFaturaGoster[i].KdvsizToplam * (-1));
                _toplamKdv += listFaturaGoster[i].KdvToplam > 0 ? listFaturaGoster[i].KdvToplam : (listFaturaGoster[i].KdvToplam * (-1));
                _toplamTutar += listFaturaGoster[i].lNetTutar > 0 ? listFaturaGoster[i].lNetTutar : (listFaturaGoster[i].lNetTutar * (-1));
                _toplamFaturaAdedi++;
                item.Tag = listFaturaGoster[i];
                listViewUrun.Items.Add(item);
            }
            listViewUrun.Tag = listFaturaGoster;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
            clsGenel.Boyama(listViewUrun);
            txtToplamKdv.Text = _toplamKdv.ToString("N2");
            txtToplamKdvsiz.Text = _toplamKdvsiz.ToString("N2");
            txtToplamTutar.Text = _toplamTutar.ToString("N2");
            txtFaturaAdedi.Text = _toplamFaturaAdedi.ToString("N0");
        }

        private void FaturaKumulatifleriDoldur(List<tbFatura> listFatura)
        {
            listViewUrun.Items.Clear();
            listViewUrun.Tag = null;

            txtToplamKdv.Text = "";
            txtToplamKdvsiz.Text = "";
            txtToplamTutar.Text = "";

            List<tbFatura> listFaturaGoster = new List<tbFatura>();
            for (int j = 0; j < listFatura.Count; j++)
            {
                if (listFaturaGoster.Where(o => o.InvoiceNumber == listFatura[j].InvoiceNumber).Count<tbFatura>() != 0)
                    continue;
                listFaturaGoster.Add(listFatura[j]);
            }

            ListViewItem item = null;
            decimal _toplamKdvsiz = 0;
            decimal _toplamKdv = 0;
            decimal _toplamTutar = 0;
            int _toplamFaturaAdedi = 0;
            for (int i = 0; i < listFaturaGoster.Count; i++)
            {
                item = new ListViewItem();
                item.Text = (i + 1).ToString();
                item.SubItems.Add(listFaturaGoster[i].InvoiceNumber.Replace(" ", ""));
                item.SubItems.Add(listFaturaGoster[i].dteFisTarihi.HasValue ? listFaturaGoster[i].dteFisTarihi.Value.Date.ToShortDateString() : "");
                item.SubItems.Add(listFaturaGoster[i].sKodu);
                item.SubItems.Add(listFaturaGoster[i].sAciklama + " " + listFaturaGoster[i].sSoyadi);
                item.SubItems.Add(listFaturaGoster[i].sCuzdanKayitNO);
                item.SubItems.Add(listFaturaGoster[i].sVergiDairesi);
                item.SubItems.Add(listFaturaGoster[i].sVergiNo);
                item.SubItems.Add(listFaturaGoster[i].KdvsizToplam > 0 ? listFaturaGoster[i].KdvsizToplam.ToString("N2") : (listFaturaGoster[i].KdvsizToplam * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].KdvToplam > 0 ? listFaturaGoster[i].KdvToplam.ToString("N2") : (listFaturaGoster[i].KdvToplam * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].lNetTutar > 0 ? listFaturaGoster[i].lNetTutar.ToString("N2") : (listFaturaGoster[i].lNetTutar * (-1)).ToString("N2"));
                item.SubItems.Add(listFaturaGoster[i].dteFisKayitTarihi.HasValue ? listFaturaGoster[i].dteFisKayitTarihi.Value.ToShortDateString() + " - " + listFaturaGoster[i].dteFisKayitTarihi.Value.ToShortTimeString() : "");
                _toplamKdvsiz += listFaturaGoster[i].KdvsizToplam > 0 ? listFaturaGoster[i].KdvsizToplam : (listFaturaGoster[i].KdvsizToplam * (-1));
                _toplamKdv += listFaturaGoster[i].KdvToplam > 0 ? listFaturaGoster[i].KdvToplam : (listFaturaGoster[i].KdvToplam * (-1));
                _toplamTutar += listFaturaGoster[i].lNetTutar > 0 ? listFaturaGoster[i].lNetTutar : (listFaturaGoster[i].lNetTutar * (-1));
                _toplamFaturaAdedi++;
                item.Tag = listFaturaGoster[i];
                listViewUrun.Items.Add(item);
            }
            listViewUrun.Tag = listFaturaGoster;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
            clsGenel.Boyama(listViewUrun);
            listViewUrun.Columns[3].Width = 100;
            listViewUrun.Columns[4].Width = 150;
            txtToplamKdv.Text = _toplamKdv.ToString("N2");
            txtToplamKdvsiz.Text = _toplamKdvsiz.ToString("N2");
            txtToplamTutar.Text = _toplamTutar.ToString("N2");
            txtFaturaAdedi.Text = _toplamFaturaAdedi.ToString("N0");
        }

        #region KeyAsagi

        private void KeyAsagi(Control _control)
        {
            _control.KeyDown += new KeyEventHandler(_control_KeyDown);
            for (int i = 0; i < _control.Controls.Count; i++)
                KeyAsagi(_control.Controls[i]);
        }

        void _control_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnUrunAra_Click(null, null);
                    break;
                case Keys.F1:
                    clsKisayol.FaturaListesi(this.MdiParent);
                    break;
                case Keys.F3:
                    toolFiltrele_Click(null, null);
                    break;
                case Keys.F5:
                    toolStripButton3_Click(null, null);
                    break;
                default:
                    break;
            }
            if ((ModifierKeys.Equals(Keys.Control) & e.KeyCode.Equals(Keys.S)))
                toolStripButton1_Click(null, null);
            if ((ModifierKeys.Equals(Keys.Control) & e.KeyCode.Equals(Keys.F)))
                toolFaturaGoster_Click(null, null);
            if ((ModifierKeys.Equals(Keys.Control) & e.KeyCode.Equals(Keys.P)))
                toolFaturaCikart_Click(null, null);
            if ((ModifierKeys.Equals(Keys.Control) & e.KeyCode.Equals(Keys.K)))
                toolStripButton2_Click(null, null);
        }

        #endregion

        private void frmSiparisGonder_Load(object sender, EventArgs e)
        {
            KeyAsagi(this);
            dateSatisTarihiBas.Value = DateTime.Now;
            dateSatisTarihiBit.Value = DateTime.Now;
            FaturaKumulatifView();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            clsGenel.Export2Excel(listViewUrun, "xls");
        }

        private void listViewUrun_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listViewUrun.SelectedItems.Count != 1)
                return;
            if (listViewUrun.SelectedItems[0].Tag.GetType() != typeof(tbFatura))
                return;
            tbFatura _fatura = (tbFatura)listViewUrun.SelectedItems[0].Tag;
            if (_fatura != null)
            {
                using (frmFaturaDetay _frm = new frmFaturaDetay())
                {
                    _frm._fatura = _fatura;
                    _frm.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Fatura bilgisine ulaşılamamıştır.", "Fatura Detay Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        EFatura reportFatura = null;
        EArsiv reportArsiv = null;
        EITArsiv reportInternet = null;
        bool _arsiv = false;
        private void toolFaturaGoster_Click(object sender, EventArgs e)
        {
            if (listViewUrun.SelectedItems.Count != 1)
                return;
            tbFatura _fatura = (tbFatura)listViewUrun.SelectedItems[0].Tag;
            if (_fatura.eInvoice.Contains("0"))
                _arsiv = false;
            else
                _arsiv = true;

            tbFaturaDepo _faturaDepo = tbFaturaDepoProvider.FaturaDepolariGetir(_fatura.sDepo);

            //List<tbFatura> listFaturaListesi = tbFaturaProvider.FaturaListesi(_fatura.InvoiceNumber);
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            List<tbFatura> listFaturaBugun = null;
            List<tbFatura> listFaturaGecmis = null;
            if (dateSatisTarihiBit.Value.Date >= DateTime.Now.Date)
            {
                listFaturaBugun = tbFaturaProvider.BugunFaturalariGetir(_fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFFaturalariGetir(_fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFASFaturalariGetir(_fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(dateSatisTarihiBas.Value.Date, DateTime.Now.Date.AddDays(-1).Date, _fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaGecmis != null)
                    listFaturaListesi.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            else
            {
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(dateSatisTarihiBas.Value.Date, dateSatisTarihiBit.Value.Date, _fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaGecmis != null)
                    listFaturaListesi.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            //List<tbFaturaDetay> listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            List<tbFaturaDetay> listFaturaDetayBugun = null;
            List<tbFaturaDetay> listFaturaDetay = null;
            if (_fatura.dteFisTarihi.HasValue)
            {
                if (_fatura.dteFisTarihi.Value.Date == DateTime.Now.Date)
                {
                    listFaturaDetay = new List<tbFaturaDetay>();
                    listFaturaDetayBugun = tbFaturaDetayProvider.PFFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura detay listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    listFaturaDetayBugun = tbFaturaDetayProvider.PKFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                    listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            }

            if (_arsiv)
            {
                if (_fatura.InvoiceNumber.Contains("ITK"))
                {
                    reportInternet = new EITArsiv();
                    if (_faturaDepo != null)
                    {
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                        ((TextObject)reportInternet.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                    }
                    else
                    {
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                        ((TextObject)reportInternet.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                    }

                    ((TextObject)reportInternet.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                    ((TextObject)reportInternet.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                    ((TextObject)reportInternet.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                    ((TextObject)reportInternet.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                    if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                        ((TextObject)reportInternet.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                    else
                        ((TextObject)reportInternet.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                    switch (_fatura.Senaryo)
                    {
                        case "TEMELFATURA":
                        case "TEMEL":
                            ((TextObject)reportInternet.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                            break;
                        case "TICARIFATURA":
                        case "TICARI":
                            ((TextObject)reportInternet.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                            break;
                    }

                    if (_fatura.nGirisCikis > 0)
                    {
                        switch (_fatura.nGirisCikis)
                        {
                            case 3:
                                if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                                else
                                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                            case 2:
                                ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                                break;
                            default:
                                ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                        }
                    }
                    else
                        ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                    ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                    ((TextObject)reportInternet.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                    ((TextObject)reportInternet.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                    ((TextObject)reportInternet.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                    StringBuilder _builder = new StringBuilder();
                    _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                    _builder.Append("<br>e-Arşiv izni kapsamında elektronik ortamda iletilmiştir.");
                    _builder.Append("<br>Bu satış internet üzerinden yapılmıştır.");
                    _builder.Append("<br># Ödeme Şekli : " + _fatura.sFisTipi);
                    _builder.Append("<br># Ödeme Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : ""));
                    _builder.Append("<br># Satışın Yapıldığı Web Adres : http://www.banco.com.tr");
                    _builder.Append("<br># Gönderiyi Taşıyan VKN / TCKN : 9860008925");
                    _builder.Append("<br># Gönderiyi Taşıyan Ünvan / Ad Soyad : YURTİÇİ KARGO SERVİSİ A.Ş.");
                    _builder.Append("<br># Ürün Gönderim / Hizmet İfa Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : ""));
                    if (_fatura.nKdvOrani1 > 0)
                        _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                    if (_fatura.nKdvOrani2 > 0)
                        _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                    if (_fatura.FaturaTipi.Contains("ISTISNA"))
                        _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                    if (listFaturaListesi != null)
                    {
                        for (int i = 0; i < listFaturaListesi.Count; i++)
                            if (listFaturaListesi[i].lPesinat > 0)
                                _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                    }
                    _builder.Append("<br>" + _fatura.StokAciklama + "</div>");

                    if (listFaturaDetay != null)
                    {
                        if (listFaturaDetay.Count > 0)
                        {
                            DataTable _table = new DataTable("dtTablo");
                            _table.Columns.Add("UrunKodu", typeof(string));
                            _table.Columns.Add("MalHizmet", typeof(string));
                            _table.Columns.Add("Miktar", typeof(decimal));
                            _table.Columns.Add("BirimFiyat", typeof(decimal));
                            _table.Columns.Add("Tutar", typeof(decimal));
                            _table.Columns.Add("Note", typeof(string));
                            DataRow _row = null;
                            for (int i = 0; i < listFaturaDetay.Count; i++)
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                                _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                                _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                                _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                                _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            if (_fatura.FaturaTipi.Contains("ISTISNA"))
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = "-";
                                _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                                _row["Miktar"] = 0;
                                _row["BirimFiyat"] = 0;
                                _row["Tutar"] = 0;
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            reportInternet.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                            reportInternet.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                            reportInternet.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                            reportInternet.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                            reportInternet.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                            reportInternet.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                            reportInternet.SetDataSource(_table);
                        }
                    }
                }
                else
                {
                    reportArsiv = new EArsiv();
                    if (_faturaDepo != null)
                    {
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                    }
                    else
                    {
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                    }

                    ((TextObject)reportArsiv.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                    if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                    else
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                    switch (_fatura.Senaryo)
                    {
                        case "TEMELFATURA":
                        case "TEMEL":
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                            break;
                        case "TICARIFATURA":
                        case "TICARI":
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                            break;
                    }

                    if (_fatura.nGirisCikis > 0)
                    {
                        switch (_fatura.nGirisCikis)
                        {
                            case 3:
                                if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                                else
                                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                            case 2:
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                                break;
                            default:
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                break;
                        }
                    }
                    else
                        ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                    ((TextObject)reportArsiv.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                    ((TextObject)reportArsiv.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                    StringBuilder _builder = new StringBuilder();
                    _builder.Append("<div>YALNIZ : " + _fatura.sYaziIle);
                    if (_fatura.nKdvOrani1 > 0)
                        _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                    if (_fatura.nKdvOrani2 > 0)
                        _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                    if (_fatura.FaturaTipi.Contains("ISTISNA"))
                        _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                    if (listFaturaListesi != null)
                    {
                        for (int i = 0; i < listFaturaListesi.Count; i++)
                        {
                            if (listFaturaListesi[i].lPesinat > 0)
                                _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                        }
                    }

                    _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                    if (listFaturaDetay != null)
                    {
                        if (listFaturaDetay.Count > 0)
                        {
                            DataTable _table = new DataTable("dtTablo");
                            _table.Columns.Add("UrunKodu", typeof(string));
                            _table.Columns.Add("MalHizmet", typeof(string));
                            _table.Columns.Add("Miktar", typeof(decimal));
                            _table.Columns.Add("BirimFiyat", typeof(decimal));
                            _table.Columns.Add("Tutar", typeof(decimal));
                            _table.Columns.Add("Note", typeof(string));
                            DataRow _row = null;
                            for (int i = 0; i < listFaturaDetay.Count; i++)
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                                _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                                _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                                _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                                _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            if (_fatura.FaturaTipi.Contains("ISTISNA"))
                            {
                                _row = _table.NewRow();
                                _row["UrunKodu"] = "-";
                                _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                                _row["Miktar"] = 0;
                                _row["BirimFiyat"] = 0;
                                _row["Tutar"] = 0;
                                _row["Note"] = _builder.ToString();
                                _table.Rows.Add(_row);
                            }
                            reportArsiv.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                            reportArsiv.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                            reportArsiv.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                            reportArsiv.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                            reportArsiv.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                            reportArsiv.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                            reportArsiv.SetDataSource(_table);
                        }
                    }
                }
            }
            else
            {
                reportFatura = new EFatura();
                if (_faturaDepo != null)
                {
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                    ((TextObject)reportFatura.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                }
                else
                {
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                    ((TextObject)reportFatura.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                }

                ((TextObject)reportFatura.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                ((TextObject)reportFatura.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                ((TextObject)reportFatura.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                ((TextObject)reportFatura.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                    ((TextObject)reportFatura.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                else
                    ((TextObject)reportFatura.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                switch (_fatura.Senaryo)
                {
                    case "TEMELFATURA":
                    case "TEMEL":
                        ((TextObject)reportFatura.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                        break;
                    case "TICARIFATURA":
                    case "TICARI":
                        ((TextObject)reportFatura.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                        break;
                }

                if (_fatura.nGirisCikis > 0)
                {
                    switch (_fatura.nGirisCikis)
                    {
                        case 3:
                            if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                            else
                                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                            break;
                        case 2:
                            ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                            break;
                        default:
                            ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                            break;
                    }
                }
                else
                    ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFatura.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFatura.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                ((TextObject)reportFatura.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                ((TextObject)reportFatura.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                ((TextObject)reportFatura.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                StringBuilder _builder = new StringBuilder();
                _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                if (_fatura.nKdvOrani1 > 0)
                    _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                if (_fatura.nKdvOrani2 > 0)
                    _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                if (_fatura.FaturaTipi.Contains("ISTISNA"))
                    _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                if (listFaturaListesi != null)
                {
                    for (int i = 0; i < listFaturaListesi.Count; i++)
                        if (listFaturaListesi[i].lPesinat > 0)
                            _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                }
                _builder.Append("<br>" + _fatura.StokAciklama + "</div>");

                if (listFaturaDetay != null)
                {
                    if (listFaturaDetay.Count > 0)
                    {
                        DataTable _table = new DataTable("dtTablo");
                        _table.Columns.Add("UrunKodu", typeof(string));
                        _table.Columns.Add("MalHizmet", typeof(string));
                        _table.Columns.Add("Miktar", typeof(decimal));
                        _table.Columns.Add("BirimFiyat", typeof(decimal));
                        _table.Columns.Add("Tutar", typeof(decimal));
                        _table.Columns.Add("Note", typeof(string));
                        DataRow _row = null;
                        for (int i = 0; i < listFaturaDetay.Count; i++)
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                            _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                            _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                            _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                            _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        if (_fatura.FaturaTipi.Contains("ISTISNA"))
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = "-";
                            _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                            _row["Miktar"] = 0;
                            _row["BirimFiyat"] = 0;
                            _row["Tutar"] = 0;
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        reportFatura.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                        reportFatura.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                        reportFatura.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                        reportFatura.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                        reportFatura.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                        reportFatura.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                        reportFatura.SetDataSource(_table);
                    }
                }
            }

            frmRaporGoster frm = new frmRaporGoster();
            frm._caption = "FATURA ÇIKTISI";
            if (_arsiv)
            {
                if (_fatura.InvoiceNumber.Contains("ITK"))
                    frm._report = reportInternet;
                else
                    frm._report = reportArsiv;
            }
            else
                frm._report = reportFatura;
            frm.WindowState = FormWindowState.Maximized;
            frm.Show();
        }

        private void toolFaturaCikart_Click(object sender, EventArgs e)
        {
            if (listViewUrun.CheckedItems.Count > 0)
            {
                for (int i = 0; i < listViewUrun.CheckedItems.Count; i++)
                {
                    tbFatura _fatura = (tbFatura)listViewUrun.CheckedItems[i].Tag;
                    if (_fatura.eInvoice.Contains("0"))
                        _arsiv = false;
                    else
                        _arsiv = true;
                    tbFaturaDepo _faturaDepo = tbFaturaDepoProvider.FaturaDepolariGetir(_fatura.sDepo);
                    List<tbFatura> listFaturaListesi = tbFaturaProvider.FaturaListesi(_fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null);
                    List<tbFaturaDetay> listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);

                    if (_arsiv)
                    {
                        if (_fatura.InvoiceNumber.Contains("ITK"))
                        {
                            reportInternet = new EITArsiv();
                            if (_faturaDepo != null)
                            {
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                                ((TextObject)reportInternet.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                                ((TextObject)reportInternet.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                            }
                            else
                            {
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                                ((TextObject)reportInternet.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN :";
                                ((TextObject)reportInternet.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                                ((TextObject)reportInternet.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                            }

                            ((TextObject)reportInternet.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                            ((TextObject)reportInternet.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                            ((TextObject)reportInternet.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                            ((TextObject)reportInternet.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                            if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                                ((TextObject)reportInternet.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                            else
                                ((TextObject)reportInternet.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                            switch (_fatura.Senaryo)
                            {
                                case "TEMELFATURA":
                                case "TEMEL":
                                    ((TextObject)reportInternet.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                                    break;
                                case "TICARIFATURA":
                                case "TICARI":
                                    ((TextObject)reportInternet.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                                    break;
                            }

                            if (_fatura.nGirisCikis > 0)
                            {
                                switch (_fatura.nGirisCikis)
                                {
                                    case 3:
                                        if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                            ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                                        else
                                            ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                        break;
                                    case 2:
                                        ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                                        break;
                                    default:
                                        ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                        break;
                                }
                            }
                            else
                                ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                            ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                            ((TextObject)reportInternet.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                            ((TextObject)reportInternet.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                            ((TextObject)reportInternet.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                            ((TextObject)reportInternet.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                            ((TextObject)reportInternet.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                            ((TextObject)reportInternet.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                            ((TextObject)reportInternet.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                            ((TextObject)reportInternet.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                            ((TextObject)reportInternet.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                            ((TextObject)reportInternet.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                            ((TextObject)reportInternet.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                            StringBuilder _builder = new StringBuilder();
                            _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                            _builder.Append("<br>e-Arşiv izni kapsamında elektronik ortamda iletilmiştir.");
                            _builder.Append("<br>Bu satış internet üzerinden yapılmıştır.");
                            _builder.Append("<br># Ödeme Şekli : " + _fatura.sFisTipi);
                            _builder.Append("<br># Ödeme Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : ""));
                            _builder.Append("<br># Satışın Yapıldığı Web Adres : http://www.banco.com.tr");
                            _builder.Append("<br># Gönderiyi Taşıyan VKN / TCKN : 9860008925");
                            _builder.Append("<br># Gönderiyi Taşıyan Ünvan / Ad Soyad : YURTİÇİ KARGO SERVİSİ A.Ş.");
                            _builder.Append("<br># Ürün Gönderim / Hizmet İfa Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : ""));

                            if (_fatura.nKdvOrani1 > 0)
                                _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                            if (_fatura.nKdvOrani2 > 0)
                                _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                            if (_fatura.FaturaTipi.Contains("ISTISNA"))
                                _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                            if (listFaturaListesi != null)
                            {
                                for (int j = 0; j < listFaturaListesi.Count; j++)
                                    if (listFaturaListesi[j].lPesinat > 0)
                                        _builder.Append("<br>" + listFaturaListesi[j].OdemeAciklama + " : " + listFaturaListesi[j].lPesinat.ToString("N2") + " TRY");
                            }
                            _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                            if (listFaturaDetay != null)
                            {
                                if (listFaturaDetay.Count > 0)
                                {
                                    DataTable _table = new DataTable("dtTablo");
                                    _table.Columns.Add("UrunKodu", typeof(string));
                                    _table.Columns.Add("MalHizmet", typeof(string));
                                    _table.Columns.Add("Miktar", typeof(decimal));
                                    _table.Columns.Add("BirimFiyat", typeof(decimal));
                                    _table.Columns.Add("Tutar", typeof(decimal));
                                    _table.Columns.Add("Note", typeof(string));
                                    DataRow _row = null;
                                    for (int j = 0; j < listFaturaDetay.Count; j++)
                                    {
                                        _row = _table.NewRow();
                                        _row["UrunKodu"] = listFaturaDetay[j].sKodu;
                                        _row["MalHizmet"] = listFaturaDetay[j].sAciklama;
                                        _row["Miktar"] = listFaturaDetay[j].lCikisMiktar1.ToString("N2");
                                        _row["BirimFiyat"] = listFaturaDetay[j].lCikisFiyat1.ToString("N2");
                                        _row["Tutar"] = listFaturaDetay[j].lCikisTutar.ToString("N2");
                                        _row["Note"] = _builder.ToString();
                                        _table.Rows.Add(_row);
                                    }
                                    if (_fatura.FaturaTipi.Contains("ISTISNA"))
                                    {
                                        _row = _table.NewRow();
                                        _row["UrunKodu"] = "-";
                                        _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                                        _row["Miktar"] = 0;
                                        _row["BirimFiyat"] = 0;
                                        _row["Tutar"] = 0;
                                        _row["Note"] = _builder.ToString();
                                        _table.Rows.Add(_row);
                                    }
                                    reportInternet.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                                    reportInternet.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                                    reportInternet.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                                    reportInternet.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                                    reportInternet.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                                    reportInternet.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                                    reportInternet.SetDataSource(_table);
                                }
                            }
                        }
                        else
                        {
                            reportArsiv = new EArsiv();
                            if (_faturaDepo != null)
                            {
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                            }
                            else
                            {
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN :";
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                            }

                            ((TextObject)reportArsiv.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                            if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                            else
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                            switch (_fatura.Senaryo)
                            {
                                case "TEMELFATURA":
                                case "TEMEL":
                                    ((TextObject)reportArsiv.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                                    break;
                                case "TICARIFATURA":
                                case "TICARI":
                                    ((TextObject)reportArsiv.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                                    break;
                            }

                            if (_fatura.nGirisCikis > 0)
                            {
                                switch (_fatura.nGirisCikis)
                                {
                                    case 3:
                                        if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                            ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                                        else
                                            ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                        break;
                                    case 2:
                                        ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                                        break;
                                    default:
                                        ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                        break;
                                }
                            }
                            else
                                ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                            ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                            ((TextObject)reportArsiv.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                            ((TextObject)reportArsiv.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                            StringBuilder _builder = new StringBuilder();
                            _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                            if (_fatura.nKdvOrani1 > 0)
                                _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                            if (_fatura.nKdvOrani2 > 0)
                                _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                            if (_fatura.FaturaTipi.Contains("ISTISNA"))
                                _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");

                            if (listFaturaListesi != null)
                            {
                                for (int j = 0; j < listFaturaListesi.Count; j++)
                                {
                                    if (listFaturaListesi[j].lPesinat > 0)
                                        _builder.Append("<br>" + listFaturaListesi[j].OdemeAciklama + " : " + listFaturaListesi[j].lPesinat.ToString("N2") + " TRY");
                                }
                            }
                            _builder.Append("<br>" + _fatura.StokAciklama + "</div>");

                            if (listFaturaDetay != null)
                            {
                                if (listFaturaDetay.Count > 0)
                                {
                                    DataTable _table = new DataTable("dtTablo");
                                    _table.Columns.Add("UrunKodu", typeof(string));
                                    _table.Columns.Add("MalHizmet", typeof(string));
                                    _table.Columns.Add("Miktar", typeof(decimal));
                                    _table.Columns.Add("BirimFiyat", typeof(decimal));
                                    _table.Columns.Add("Tutar", typeof(decimal));
                                    _table.Columns.Add("Note", typeof(string));
                                    DataRow _row = null;
                                    for (int j = 0; j < listFaturaDetay.Count; j++)
                                    {
                                        _row = _table.NewRow();
                                        _row["UrunKodu"] = listFaturaDetay[j].sKodu;
                                        _row["MalHizmet"] = listFaturaDetay[j].sAciklama;
                                        _row["Miktar"] = listFaturaDetay[j].lCikisMiktar1.ToString("N2");
                                        _row["BirimFiyat"] = listFaturaDetay[j].lCikisFiyat1.ToString("N2");
                                        _row["Tutar"] = listFaturaDetay[j].lCikisTutar.ToString("N2");
                                        _row["Note"] = _builder.ToString();
                                        _table.Rows.Add(_row);
                                    }
                                    if (_fatura.FaturaTipi.Contains("ISTISNA"))
                                    {
                                        _row = _table.NewRow();
                                        _row["UrunKodu"] = "-";
                                        _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                                        _row["Miktar"] = 0;
                                        _row["BirimFiyat"] = 0;
                                        _row["Tutar"] = 0;
                                        _row["Note"] = _builder.ToString();
                                        _table.Rows.Add(_row);
                                    }
                                    reportArsiv.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                                    reportArsiv.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                                    reportArsiv.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                                    reportArsiv.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                                    reportArsiv.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                                    reportArsiv.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                                    reportArsiv.SetDataSource(_table);
                                }
                            }
                        }
                    }
                    else
                    {
                        reportFatura = new EFatura();
                        if (_faturaDepo != null)
                        {
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                            ((TextObject)reportFatura.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                            ((TextObject)reportFatura.Section2.ReportObjects["txtOzellestirmeNo"]).Text = _faturaDepo.Versiyon.Trim();
                        }
                        else
                        {
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres1"]).Text = "";
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoAdres2"]).Text = "";
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                            ((TextObject)reportFatura.Section2.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN :";
                            ((TextObject)reportFatura.Section2.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                            ((TextObject)reportFatura.Section2.ReportObjects["txtOzellestirmeNo"]).Text = "";
                        }

                        ((TextObject)reportFatura.Section2.ReportObjects["txtETTN"]).Text = "ETTN : " + _fatura.ETTN.ToUpper();
                        ((TextObject)reportFatura.Section2.ReportObjects["txtMusteriAdSoyad"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                        ((TextObject)reportFatura.Section2.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                        ((TextObject)reportFatura.Section2.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                        if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                            ((TextObject)reportFatura.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                        else
                            ((TextObject)reportFatura.Section2.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;

                        switch (_fatura.Senaryo)
                        {
                            case "TEMELFATURA":
                            case "TEMEL":
                                ((TextObject)reportFatura.Section2.ReportObjects["txtSenaryo"]).Text = "TEMEL FATURA";
                                break;
                            case "TICARIFATURA":
                            case "TICARI":
                                ((TextObject)reportFatura.Section2.ReportObjects["txtSenaryo"]).Text = "TİCARİ FATURA";
                                break;
                        }

                        if (_fatura.nGirisCikis > 0)
                        {
                            switch (_fatura.nGirisCikis)
                            {
                                case 3:
                                    if (string.IsNullOrEmpty(_fatura.FaturaTipi))
                                        ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";
                                    else
                                        ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                    break;
                                case 2:
                                    ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "İADE";
                                    break;
                                default:
                                    ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = _fatura.FaturaTipi.Trim();
                                    break;
                            }
                        }
                        else
                            ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTipi"]).Text = "SATIŞ";

                        ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                        ((TextObject)reportFatura.Section2.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                        ((TextObject)reportFatura.Section2.ReportObjects["txtDuzenlenmeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                        ((TextObject)reportFatura.Section2.ReportObjects["txtDuzenlenmeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                        ((TextObject)reportFatura.Section2.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                        ((TextObject)reportFatura.Section2.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                        ((TextObject)reportFatura.Section2.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                        ((TextObject)reportFatura.Section2.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                        ((TextObject)reportFatura.Section2.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                        ((TextObject)reportFatura.Section2.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                        ((TextObject)reportFatura.Section2.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                        ((TextObject)reportFatura.Section2.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                        StringBuilder _builder = new StringBuilder();
                        _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                        if (_fatura.nKdvOrani1 > 0)
                            _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                        if (_fatura.nKdvOrani2 > 0)
                            _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));
                        if (_fatura.FaturaTipi.Contains("ISTISNA"))
                            _builder.Append("<br>Vergi İstisna Muafiyet Sebebi : 209 Kodlu Kısmi İstisna");


                        if (listFaturaListesi != null)
                        {
                            for (int j = 0; j < listFaturaListesi.Count; j++)
                                if (listFaturaListesi[j].lPesinat > 0)
                                    _builder.Append("<br>" + listFaturaListesi[j].OdemeAciklama + " : " + listFaturaListesi[j].lPesinat.ToString("N2") + " TRY");
                        }
                        _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                        if (listFaturaDetay != null)
                        {
                            if (listFaturaDetay.Count > 0)
                            {
                                DataTable _table = new DataTable("dtTablo");
                                _table.Columns.Add("UrunKodu", typeof(string));
                                _table.Columns.Add("MalHizmet", typeof(string));
                                _table.Columns.Add("Miktar", typeof(decimal));
                                _table.Columns.Add("BirimFiyat", typeof(decimal));
                                _table.Columns.Add("Tutar", typeof(decimal));
                                _table.Columns.Add("Note", typeof(string));
                                DataRow _row = null;
                                for (int j = 0; j < listFaturaDetay.Count; j++)
                                {
                                    _row = _table.NewRow();
                                    _row["UrunKodu"] = listFaturaDetay[j].sKodu;
                                    _row["MalHizmet"] = listFaturaDetay[j].sAciklama;
                                    _row["Miktar"] = listFaturaDetay[j].lCikisMiktar1.ToString("N2");
                                    _row["BirimFiyat"] = listFaturaDetay[j].lCikisFiyat1.ToString("N2");
                                    _row["Tutar"] = listFaturaDetay[j].lCikisTutar.ToString("N2");
                                    _row["Note"] = _builder.ToString();
                                    _table.Rows.Add(_row);
                                }
                                if (_fatura.FaturaTipi.Contains("ISTISNA"))
                                {
                                    _row = _table.NewRow();
                                    _row["UrunKodu"] = "-";
                                    _row["MalHizmet"] = "Kod: 209 209 Kodlu Kısmi İstisna";
                                    _row["Miktar"] = 0;
                                    _row["BirimFiyat"] = 0;
                                    _row["Tutar"] = 0;
                                    _row["Note"] = _builder.ToString();
                                    _table.Rows.Add(_row);
                                }
                                reportFatura.DataDefinition.FormulaFields["strUrunKodu"].Text = "{dtTablo.UrunKodu}";
                                reportFatura.DataDefinition.FormulaFields["strMalHizmet"].Text = "{dtTablo.MalHizmet}";
                                reportFatura.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                                reportFatura.DataDefinition.FormulaFields["intBirimFiyat"].Text = "{dtTablo.BirimFiyat}";
                                reportFatura.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                                reportFatura.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                                reportFatura.SetDataSource(_table);
                            }
                        }
                    }

                    if (_arsiv)
                    {
                        if (_fatura.InvoiceNumber.Contains("ITK"))
                            reportInternet.PrintToPrinter(1, false, 0, 0);
                        else
                            reportArsiv.PrintToPrinter(1, false, 0, 0);
                    }
                    else
                        reportFatura.PrintToPrinter(1, false, 0, 0);

                }
                MessageBox.Show("Seçili faturalarınızın yazdırma işlemi başarıyla tamamlanmıştır.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Yazdırmak istediğiniz faturaların seçimini yapmalısınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void checkFaturaSec_CheckedChanged(object sender, EventArgs e)
        {
            if (checkFaturaSec.Checked)
            {
                for (int i = 0; i < listViewUrun.Items.Count; i++)
                    listViewUrun.Items[i].Checked = true;
            }
            else
            {
                for (int i = 0; i < listViewUrun.Items.Count; i++)
                    listViewUrun.Items[i].Checked = false;
            }
        }

        FaturaSlip reportFaturaSlip = null;
        ArsivSlip reportArsivSlip = null;
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (listViewUrun.SelectedItems.Count != 1)
                return;
            tbFatura _fatura = (tbFatura)listViewUrun.SelectedItems[0].Tag;
            if (_fatura.eInvoice.Contains("0"))
                _arsiv = false;
            else
                _arsiv = true;

            tbFaturaDepo _faturaDepo = tbFaturaDepoProvider.FaturaDepolariGetir(_fatura.sDepo);
            //List<tbFatura> listFaturaListesi = tbFaturaProvider.FaturaListesi(_fatura.InvoiceNumber);
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            List<tbFatura> listFaturaBugun = null;
            List<tbFatura> listFaturaGecmis = null;
            if (dateSatisTarihiBit.Value.Date >= DateTime.Now.Date)
            {
                listFaturaBugun = tbFaturaProvider.BugunFaturalariGetir(_fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFFaturalariGetir(_fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaBugun = tbFaturaProvider.BugunPFASFaturalariGetir(_fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaBugun != null)
                    listFaturaListesi.AddRange(listFaturaBugun);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(dateSatisTarihiBas.Value.Date, DateTime.Now.Date.AddDays(-1).Date, _fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaGecmis != null)
                    listFaturaListesi.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            else
            {
                listFaturaGecmis = tbFaturaProvider.FaturaListesi(dateSatisTarihiBas.Value.Date, dateSatisTarihiBit.Value.Date, _fatura.InvoiceNumber, listDepo != null ? listDepo.Select(o => o.sDepo).ToList<string>() : null, listFisTipi != null ? listFisTipi.Select(o => o.sFisTipi).ToList<string>() : null);
                if (listFaturaGecmis != null)
                    listFaturaListesi.AddRange(listFaturaGecmis);
                else
                {
                    MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            //List<tbFaturaDetay> listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            List<tbFaturaDetay> listFaturaDetayBugun = null;
            List<tbFaturaDetay> listFaturaDetay = null;
            if (_fatura.dteFisTarihi.HasValue)
            {
                if (_fatura.dteFisTarihi.Value.Date == DateTime.Now.Date)
                {
                    listFaturaDetay = new List<tbFaturaDetay>();
                    listFaturaDetayBugun = tbFaturaDetayProvider.PFFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura detay listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    listFaturaDetayBugun = tbFaturaDetayProvider.PKFaturaDetayGetir(_fatura.InvoiceNumber);
                    if (listFaturaDetayBugun != null)
                        listFaturaDetay.AddRange(listFaturaDetayBugun);
                    else
                    {
                        MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                    listFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariniGetir(_fatura.InvoiceNumber);
            }

            if (_arsiv)
            {
                reportArsivSlip = new ArsivSlip();
                if (_faturaDepo != null)
                {
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                }
                else
                {
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = "";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = "";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                    ((TextObject)reportArsivSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                }

                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtETTN"]).Text = _fatura.ETTN.ToUpper();
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtMusteriAd"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                    ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                else
                    ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;


                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                ((TextObject)reportArsivSlip.PageHeaderSection1.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                //((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                StringBuilder _builder = new StringBuilder();
                _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                //if (_fatura.nKdvOrani1 > 0)
                //    _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                //if (_fatura.nKdvOrani2 > 0)
                //    _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));


                //if (listFaturaListesi != null)
                //{
                //    for (int i = 0; i < listFaturaListesi.Count; i++)
                //    {
                //        if (listFaturaListesi[i].lPesinat > 0)
                //            _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                //    }
                //}

                _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                if (listFaturaDetay != null)
                {
                    if (listFaturaDetay.Count > 0)
                    {
                        DataTable _table = new DataTable("dtTablo");
                        _table.Columns.Add("UrunKodu", typeof(string));
                        _table.Columns.Add("MalHizmet", typeof(string));
                        _table.Columns.Add("Miktar", typeof(decimal));
                        _table.Columns.Add("BirimFiyat", typeof(decimal));
                        _table.Columns.Add("Tutar", typeof(decimal));
                        _table.Columns.Add("Note", typeof(string));
                        DataRow _row = null;
                        for (int i = 0; i < listFaturaDetay.Count; i++)
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                            _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                            _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                            _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                            _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        reportArsivSlip.DataDefinition.FormulaFields["strKod"].Text = "{dtTablo.UrunKodu}";
                        reportArsivSlip.DataDefinition.FormulaFields["strUrun"].Text = "{dtTablo.MalHizmet}";
                        reportArsivSlip.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                        reportArsivSlip.DataDefinition.FormulaFields["intFiyat"].Text = "{dtTablo.BirimFiyat}";
                        reportArsivSlip.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                        reportArsivSlip.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                        reportArsivSlip.SetDataSource(_table);
                    }
                }
            }
            else
            {
                reportFaturaSlip = new FaturaSlip();
                if (_faturaDepo != null)
                {
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = _faturaDepo.StreetName + " NO : " + _faturaDepo.BuildingNumber;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = _faturaDepo.PostalZone + " " + _faturaDepo.CitySubDivisionName + " / " + _faturaDepo.CityName;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : " + _faturaDepo.Telephone + " Fax : " + _faturaDepo.Telefax;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : " + _faturaDepo.ElectronicMail;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : " + _faturaDepo.TaxScheme + " / VKN : " + _faturaDepo.VKN;
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : " + _faturaDepo.MersisNo;
                }
                else
                {
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres1"]).Text = "";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoAdres2"]).Text = "";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoTelefon"]).Text = "Tel : / Fax : ";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoEmail"]).Text = "Eposta : ";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtDepoVergiDairesi"]).Text = "VD : / VKN : ";
                    ((TextObject)reportFaturaSlip.ReportHeaderSection1.ReportObjects["txtMersisNO"]).Text = "MERSIS NO : ";
                }

                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtETTN"]).Text = _fatura.ETTN.ToUpper();
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtMusteriAd"]).Text = _fatura.sAciklama + " " + _fatura.sSoyadi;
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtAdres"]).Text = string.IsNullOrEmpty(_fatura.sEvAdresi1) ? _fatura.sEvAdresi2 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl : _fatura.sEvAdresi1 + " " + _fatura.sEvSemt + " / " + _fatura.sEvIl;
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtTelefon"]).Text = "Tel : " + _fatura.sGSM;
                if (string.IsNullOrEmpty(_fatura.sVergiDairesi))
                    ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "TCKN : " + _fatura.sCuzdanKayitNO + " / MÜŞTERİ NO : " + _fatura.sKodu;
                else
                    ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtVergiTCMusteriNO"]).Text = "VD : " + _fatura.sVergiDairesi + " / " + _fatura.sVergiNo;


                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtFaturaNo"]).Text = _fatura.InvoiceNumber;
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtFaturaTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeTarihi"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() : "";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtDuzenlemeSaati"]).Text = _fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "";

                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtToplamTutar"]).Text = _fatura.KdvsizToplam < 0 ? ((-1) * _fatura.KdvsizToplam).ToString("N2") : _fatura.KdvsizToplam.ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV1Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani1 < 0 ? ((-1) * _fatura.nKdvOrani1).ToString("N0") : _fatura.nKdvOrani1.ToString("N0")) + " ) : ";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV1"]).Text = _fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1).ToString("N2") : _fatura.lKdv1.ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV2Deger"]).Text = "K.D.V ( %" + (_fatura.nKdvOrani2 < 0 ? ((-1) * _fatura.nKdvOrani2).ToString("N0") : _fatura.nKdvOrani2.ToString("N0")) + " ) : ";
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtKDV2"]).Text = _fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2).ToString("N2") : _fatura.lKdv2.ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtToplamVergi"]).Text = ((_fatura.lKdv1 < 0 ? ((-1) * _fatura.lKdv1) : _fatura.lKdv1) + (_fatura.lKdv2 < 0 ? ((-1) * _fatura.lKdv2) : _fatura.lKdv2)).ToString("N2");
                ((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtVergilerDahilToplam"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");
                //((TextObject)reportFaturaSlip.PageHeaderSection1.ReportObjects["txtOdenecekTutar"]).Text = _fatura.lNetTutar < 0 ? ((-1) * _fatura.lNetTutar).ToString("N2") : _fatura.lNetTutar.ToString("N2");

                StringBuilder _builder = new StringBuilder();
                _builder.Append("<div style='line-height: 20px;'>YALNIZ : " + _fatura.sYaziIle);
                //if (_fatura.nKdvOrani1 > 0)
                //    _builder.Append("<br>" + "KDV 1 Matrah %" + _fatura.nKdvOrani1.ToString("N2") + " ile satış " + (_fatura.lKdv1Toplam < 0 ? ((-1) * _fatura.lKdv1Toplam).ToString("N2") : _fatura.lKdv1Toplam.ToString("N2")));
                //if (_fatura.nKdvOrani2 > 0)
                //    _builder.Append("<br>" + "KDV 2 Matrah %" + _fatura.nKdvOrani2.ToString("N2") + " ile satış " + (_fatura.lKdv2Toplam < 0 ? ((-1) * _fatura.lKdv2Toplam).ToString("N2") : _fatura.lKdv2Toplam.ToString("N2")));


                //if (listFaturaListesi != null)
                //{
                //    for (int i = 0; i < listFaturaListesi.Count; i++)
                //    {
                //        if (listFaturaListesi[i].lPesinat > 0)
                //            _builder.Append("<br>" + listFaturaListesi[i].OdemeAciklama + " : " + listFaturaListesi[i].lPesinat.ToString("N2") + " TRY");
                //    }
                //}

                _builder.Append("<br>" + _fatura.StokAciklama + "</div>");
                if (listFaturaDetay != null)
                {
                    if (listFaturaDetay.Count > 0)
                    {
                        DataTable _table = new DataTable("dtTablo");
                        _table.Columns.Add("UrunKodu", typeof(string));
                        _table.Columns.Add("MalHizmet", typeof(string));
                        _table.Columns.Add("Miktar", typeof(decimal));
                        _table.Columns.Add("BirimFiyat", typeof(decimal));
                        _table.Columns.Add("Tutar", typeof(decimal));
                        _table.Columns.Add("Note", typeof(string));
                        DataRow _row = null;
                        for (int i = 0; i < listFaturaDetay.Count; i++)
                        {
                            _row = _table.NewRow();
                            _row["UrunKodu"] = listFaturaDetay[i].sKodu;
                            _row["MalHizmet"] = listFaturaDetay[i].sAciklama;
                            _row["Miktar"] = listFaturaDetay[i].lCikisMiktar1.ToString("N2");
                            _row["BirimFiyat"] = listFaturaDetay[i].lCikisFiyat1.ToString("N2");
                            _row["Tutar"] = listFaturaDetay[i].lCikisTutar.ToString("N2");
                            _row["Note"] = _builder.ToString();
                            _table.Rows.Add(_row);
                        }
                        reportFaturaSlip.DataDefinition.FormulaFields["strKod"].Text = "{dtTablo.UrunKodu}";
                        reportFaturaSlip.DataDefinition.FormulaFields["strUrun"].Text = "{dtTablo.MalHizmet}";
                        reportFaturaSlip.DataDefinition.FormulaFields["intMiktar"].Text = "{dtTablo.Miktar}";
                        reportFaturaSlip.DataDefinition.FormulaFields["intFiyat"].Text = "{dtTablo.BirimFiyat}";
                        reportFaturaSlip.DataDefinition.FormulaFields["intTutar"].Text = "{dtTablo.Tutar}";
                        reportFaturaSlip.DataDefinition.FormulaFields["strNot"].Text = "{dtTablo.Note}";
                        reportFaturaSlip.SetDataSource(_table);
                    }
                }
            }


            frmRaporGoster frm = new frmRaporGoster();
            frm._caption = "SLIP FATURA ÇIKTISI";
            frm._slip = true;
            if (_arsiv)
                frm._report = reportArsivSlip;
            else
                frm._report = reportFaturaSlip;

            frm.WindowState = FormWindowState.Maximized;
            frm.Show();
        }

        // Fatura XML Ara

        private void FaturaXMLView()
        {
            listViewUrun.Items.Clear();
            listViewUrun.Columns.Clear();

            groupBox2.Text = "Fatura XML Listesi ";
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "VKN/TCKN" });
            listViewUrun.Columns.Add(new ColumnHeader() { Text = "Ünvan" });
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
        }

        private void btnUnvanTemizle_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUnvan.Text.Trim()))
            {
                List<tbEFaturaXML> listFaturaXML = tbEFaturaXMLProvider.FaturaXMLListesiniGetir(txtUnvan.Text);
                if (listFaturaXML == null)
                {
                    MessageBox.Show("Fatura xml listesinde herhangi bir kayıt bulunamadı.", "Fatura XML Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUnvan.Text = "";
                    txtUnvan.Focus();
                    this.Cursor = Cursors.Default;
                    return;
                }
                FaturaXMLView();
                if (listFaturaXML != null)
                {
                    XMLFaturalariDoldur(listFaturaXML.ToList<tbEFaturaXML>());
                    txtUnvan.Text = "";
                }
                else
                {
                    MessageBox.Show("Fatura xml listesinde herhangi bir kayıt bulunamadı.", "Fatura XML Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUnvan.Text = "";
                    txtUnvan.Focus();
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Fatura xml listesinde aranacak tckn veya ünvan girmelisiniz.", "Fatura XML Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUnvan.Focus();
            }
        }

        private void XMLFaturalariDoldur(List<tbEFaturaXML> listFatura)
        {
            listViewUrun.Items.Clear();
            listViewUrun.Tag = null;

            txtToplamKdv.Text = "";
            txtToplamKdvsiz.Text = "";
            txtToplamTutar.Text = "";
            txtFaturaAdedi.Text = "";


            ListViewItem item = null;
            for (int i = 0; i < listFatura.Count; i++)
            {
                item = new ListViewItem();
                item.Text = listFatura[i].Identifier.ToString();
                item.SubItems.Add(listFatura[i].Title);
                item.Tag = listFatura[i];
                listViewUrun.Items.Add(item);
            }
            listViewUrun.Tag = listFatura;
            ListViewItemComparer.AutoResizeByContentAndHeader(listViewUrun);
            clsGenel.Boyama(listViewUrun);
            txtToplamKdv.Text = "";
            txtToplamKdvsiz.Text = "";
            txtToplamTutar.Text = "";
            txtFaturaAdedi.Text = "";
        }

        private bool _sortColumn = false;
        private void listViewUrun_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (listViewUrun.Tag == null)
                return;

            List<tbFatura> listFatura = null;
            List<tbEFaturaXML> listFaturaXML = null;
            if (listViewUrun.Tag.GetType() == typeof(List<tbFatura>))
                listFatura = (List<tbFatura>)listViewUrun.Tag;
            else if (listViewUrun.Tag.GetType() == typeof(List<tbEFaturaXML>))
                listFaturaXML = (List<tbEFaturaXML>)listViewUrun.Tag;
            else
                return;

            if (listFatura != null)
            {
                if (radioDetayli.Checked)
                {
                    switch (e.Column)
                    {
                        case 0:
                            break;
                        case 1:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.InvoiceNumber).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.InvoiceNumber).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 2:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sFisTipi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sFisTipi).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 3:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.dteFisTarihi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.dteFisTarihi).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 4:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lToplamMiktar).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lToplamMiktar).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 5:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lKdv1).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lKdv1).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 6:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.nKdvOrani1).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.nKdvOrani1).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 7:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lKdvMatrahi1).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lKdvMatrahi1).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 8:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lKdv1Toplam).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lKdv1Toplam).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 9:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lKdv2).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lKdv2).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 10:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.nKdvOrani2).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.nKdvOrani2).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 11:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lKdvMatrahi2).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lKdvMatrahi2).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 12:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lKdv2Toplam).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lKdv2Toplam).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 13:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.KdvsizToplam).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.KdvsizToplam).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 14:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.KdvToplam).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.KdvToplam).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 15:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lNetTutar).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lNetTutar).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 16:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sYaziIle).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sYaziIle).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 17:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.eInvoice).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.eInvoice).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 18:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sKodu).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sKodu).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 19:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sAciklama).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sAciklama).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 20:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sSoyadi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sSoyadi).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 21:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sCuzdanKayitNO).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sCuzdanKayitNO).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 22:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sVergiDairesi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sVergiDairesi).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 23:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sVergiNo).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sVergiNo).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 24:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lPesinat).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lPesinat).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 25:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.OdemeAciklama).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.OdemeAciklama).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 26:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.dteFisKayitTarihi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.OdemeAciklama).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 27:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sDepo).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sDepo).ToList<tbFatura>();
                            FaturalariDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        default:
                            break;
                    }
                }
                else if (radioKumulatif.Checked)
                {
                    switch (e.Column)
                    {
                        case 0:
                            break;
                        case 1:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.InvoiceNumber).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.InvoiceNumber).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 2:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.dteFisTarihi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.dteFisTarihi).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 3:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sKodu).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sKodu).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 4:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => (o.sAciklama + " " + o.sSoyadi)).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => (o.sAciklama + " " + o.sSoyadi)).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 5:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sCuzdanKayitNO).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sCuzdanKayitNO).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 6:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sVergiDairesi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sVergiDairesi).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 7:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.sVergiNo).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.sVergiNo).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 8:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.KdvsizToplam).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.KdvsizToplam).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 9:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.lNetTutar).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.lNetTutar).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        case 10:
                            if (_sortColumn)
                                listFatura = listFatura.OrderBy(o => o.dteFisKayitTarihi).ToList<tbFatura>();
                            else
                                listFatura = listFatura.OrderByDescending(o => o.OdemeAciklama).ToList<tbFatura>();
                            FaturaKumulatifleriDoldur(listFatura);
                            _sortColumn = !_sortColumn;
                            break;
                        default:
                            break;
                    }
                }
            }
            else if (listFaturaXML != null)
            {
                switch (e.Column)
                {
                    case 0:
                        if (_sortColumn)
                            listFaturaXML = listFaturaXML.OrderBy(o => o.Identifier).ToList<tbEFaturaXML>();
                        else
                            listFaturaXML = listFaturaXML.OrderByDescending(o => o.Identifier).ToList<tbEFaturaXML>();
                        XMLFaturalariDoldur(listFaturaXML);
                        _sortColumn = !_sortColumn;
                        break;
                    case 1:
                        if (_sortColumn)
                            listFaturaXML = listFaturaXML.OrderBy(o => o.Title).ToList<tbEFaturaXML>();
                        else
                            listFaturaXML = listFaturaXML.OrderByDescending(o => o.Title).ToList<tbEFaturaXML>();
                        XMLFaturalariDoldur(listFaturaXML);
                        _sortColumn = !_sortColumn;
                        break;
                    default:
                        break;
                }
            }
            this.Cursor = Cursors.Default;
        }

        private void frmFaturaRapor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Programdan çıkmak istediğinizden emin misiniz ?", "Çıkış", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
            else
                Application.ExitThread();
        }

        public List<tbDepo> listDepo = null;
        private void toolFiltrele_Click(object sender, EventArgs e)
        {
            using (frmDepoFiltre frm = new frmDepoFiltre())
            {
                frm.listSecilenDepo = listDepo;
                frm.ShowDialog();
                listDepo = frm.listSecilenDepo;
            }
        }

        public List<tbFisTipi> listFisTipi = null;
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            using (frmFisFiltre frm = new frmFisFiltre())
            {
                frm.listFisTipi = listFisTipi;
                frm.ShowDialog();
                listFisTipi = frm.listFisTipi;
            }
        }

        public tbMusteri _musteri = null;
        private void btnMusteriAra_Click(object sender, EventArgs e)
        {
            using (frmMusteriFiltre frm = new frmMusteriFiltre())
            {
                _musteri = null;
                if (frm.ShowDialog() == DialogResult.Yes)
                {
                    _musteri = frm._musteri;
                    this.Cursor = Cursors.WaitCursor;
                    listViewUrun.Items.Clear();
                    List<tbFatura> listFaturaGecmis = tbFaturaProvider.FaturaListesi(_musteri);
                    List<tbFatura> listFatura = new List<tbFatura>();
                    if (listFaturaGecmis != null)
                        listFatura.AddRange(listFaturaGecmis);
                    else
                    {
                        MessageBox.Show("Fatura listesi alınamıyor.Bağlantı sağlanamıyor.", "Bağlantı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    FaturaKumulatifView();
                    if (listFatura != null)
                        FaturaKumulatifleriDoldur(listFatura.OrderBy(o => o.dteFisTarihi).ToList<tbFatura>());
                    else
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show("Aradığınız değerlere göre fatura listesi alınamıyor.", "E-Arşiv ve E-Fatura Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }


    }
}
