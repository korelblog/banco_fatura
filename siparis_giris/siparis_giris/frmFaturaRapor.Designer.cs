﻿namespace siparis_giris
{
    partial class frmFaturaRapor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFaturaRapor));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnMusteriAra = new System.Windows.Forms.Button();
            this.btnUnvanTemizle = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUnvan = new System.Windows.Forms.TextBox();
            this.dateSatisTarihiBit = new System.Windows.Forms.DateTimePicker();
            this.labelIslemTarihiTire = new System.Windows.Forms.Label();
            this.dateSatisTarihiBas = new System.Windows.Forms.DateTimePicker();
            this.labelIslemTarihi = new System.Windows.Forms.Label();
            this.btnUrunAra = new System.Windows.Forms.Button();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.panelMagazaEnvanter = new System.Windows.Forms.Panel();
            this.txtFaturaAdedi = new System.Windows.Forms.Label();
            this.txtToplamTutar = new System.Windows.Forms.Label();
            this.txtToplamKdv = new System.Windows.Forms.Label();
            this.txtToplamKdvsiz = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.checkFaturaSec = new System.Windows.Forms.CheckBox();
            this.radioDetayli = new System.Windows.Forms.RadioButton();
            this.radioKumulatif = new System.Windows.Forms.RadioButton();
            this.listViewUrun = new System.Windows.Forms.ListView();
            this.columnBos = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader21 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader22 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader23 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader17 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader18 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader19 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader20 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toolFiltrele = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolFaturaGoster = new System.Windows.Forms.ToolStripButton();
            this.toolFaturaCikart = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.panelMagazaEnvanter.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnMusteriAra);
            this.groupBox1.Controls.Add(this.btnUnvanTemizle);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtUnvan);
            this.groupBox1.Controls.Add(this.dateSatisTarihiBit);
            this.groupBox1.Controls.Add(this.labelIslemTarihiTire);
            this.groupBox1.Controls.Add(this.dateSatisTarihiBas);
            this.groupBox1.Controls.Add(this.labelIslemTarihi);
            this.groupBox1.Controls.Add(this.btnUrunAra);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1187, 63);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "E-Fatura Mükellef Sorgulama";
            // 
            // btnMusteriAra
            // 
            this.btnMusteriAra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMusteriAra.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.btnMusteriAra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMusteriAra.Location = new System.Drawing.Point(1012, 17);
            this.btnMusteriAra.Name = "btnMusteriAra";
            this.btnMusteriAra.Size = new System.Drawing.Size(169, 35);
            this.btnMusteriAra.TabIndex = 103;
            this.btnMusteriAra.Text = "Müşteri Seçim";
            this.btnMusteriAra.UseVisualStyleBackColor = true;
            this.btnMusteriAra.Click += new System.EventHandler(this.btnMusteriAra_Click);
            // 
            // btnUnvanTemizle
            // 
            this.btnUnvanTemizle.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.btnUnvanTemizle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUnvanTemizle.Location = new System.Drawing.Point(279, 17);
            this.btnUnvanTemizle.Name = "btnUnvanTemizle";
            this.btnUnvanTemizle.Size = new System.Drawing.Size(76, 36);
            this.btnUnvanTemizle.TabIndex = 96;
            this.btnUnvanTemizle.Text = "Sorgula";
            this.btnUnvanTemizle.UseVisualStyleBackColor = true;
            this.btnUnvanTemizle.Click += new System.EventHandler(this.btnUnvanTemizle_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(9, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 14);
            this.label2.TabIndex = 95;
            this.label2.Text = "VKN/UNVAN :";
            // 
            // txtUnvan
            // 
            this.txtUnvan.Location = new System.Drawing.Point(113, 24);
            this.txtUnvan.Name = "txtUnvan";
            this.txtUnvan.Size = new System.Drawing.Size(160, 22);
            this.txtUnvan.TabIndex = 94;
            // 
            // dateSatisTarihiBit
            // 
            this.dateSatisTarihiBit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateSatisTarihiBit.Checked = false;
            this.dateSatisTarihiBit.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateSatisTarihiBit.Location = new System.Drawing.Point(697, 23);
            this.dateSatisTarihiBit.Name = "dateSatisTarihiBit";
            this.dateSatisTarihiBit.Size = new System.Drawing.Size(127, 22);
            this.dateSatisTarihiBit.TabIndex = 39;
            this.dateSatisTarihiBit.Value = new System.DateTime(2078, 12, 31, 18, 12, 0, 0);
            // 
            // labelIslemTarihiTire
            // 
            this.labelIslemTarihiTire.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIslemTarihiTire.AutoSize = true;
            this.labelIslemTarihiTire.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelIslemTarihiTire.Location = new System.Drawing.Point(678, 27);
            this.labelIslemTarihiTire.Name = "labelIslemTarihiTire";
            this.labelIslemTarihiTire.Size = new System.Drawing.Size(13, 14);
            this.labelIslemTarihiTire.TabIndex = 38;
            this.labelIslemTarihiTire.Text = "-";
            // 
            // dateSatisTarihiBas
            // 
            this.dateSatisTarihiBas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateSatisTarihiBas.Checked = false;
            this.dateSatisTarihiBas.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateSatisTarihiBas.Location = new System.Drawing.Point(545, 23);
            this.dateSatisTarihiBas.Name = "dateSatisTarihiBas";
            this.dateSatisTarihiBas.Size = new System.Drawing.Size(127, 22);
            this.dateSatisTarihiBas.TabIndex = 37;
            this.dateSatisTarihiBas.Value = new System.DateTime(1900, 1, 1, 18, 12, 0, 0);
            // 
            // labelIslemTarihi
            // 
            this.labelIslemTarihi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIslemTarihi.AutoSize = true;
            this.labelIslemTarihi.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelIslemTarihi.Location = new System.Drawing.Point(489, 27);
            this.labelIslemTarihi.Name = "labelIslemTarihi";
            this.labelIslemTarihi.Size = new System.Drawing.Size(50, 14);
            this.labelIslemTarihi.TabIndex = 36;
            this.labelIslemTarihi.Text = "Tarih :";
            // 
            // btnUrunAra
            // 
            this.btnUrunAra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUrunAra.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.btnUrunAra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUrunAra.Location = new System.Drawing.Point(837, 17);
            this.btnUrunAra.Name = "btnUrunAra";
            this.btnUrunAra.Size = new System.Drawing.Size(169, 35);
            this.btnUrunAra.TabIndex = 7;
            this.btnUrunAra.Text = "Fatura Ara";
            this.btnUrunAra.UseVisualStyleBackColor = true;
            this.btnUrunAra.Click += new System.EventHandler(this.btnUrunAra_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolFiltrele,
            this.toolStripButton1,
            this.toolFaturaGoster,
            this.toolFaturaCikart,
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStrip2.Location = new System.Drawing.Point(0, 485);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1211, 25);
            this.toolStrip2.TabIndex = 64;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // panelMagazaEnvanter
            // 
            this.panelMagazaEnvanter.Controls.Add(this.txtFaturaAdedi);
            this.panelMagazaEnvanter.Controls.Add(this.txtToplamTutar);
            this.panelMagazaEnvanter.Controls.Add(this.txtToplamKdv);
            this.panelMagazaEnvanter.Controls.Add(this.txtToplamKdvsiz);
            this.panelMagazaEnvanter.Controls.Add(this.label1);
            this.panelMagazaEnvanter.Controls.Add(this.label4);
            this.panelMagazaEnvanter.Controls.Add(this.label5);
            this.panelMagazaEnvanter.Controls.Add(this.label6);
            this.panelMagazaEnvanter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMagazaEnvanter.Location = new System.Drawing.Point(0, 459);
            this.panelMagazaEnvanter.Name = "panelMagazaEnvanter";
            this.panelMagazaEnvanter.Size = new System.Drawing.Size(1211, 26);
            this.panelMagazaEnvanter.TabIndex = 90;
            // 
            // txtFaturaAdedi
            // 
            this.txtFaturaAdedi.AutoSize = true;
            this.txtFaturaAdedi.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtFaturaAdedi.ForeColor = System.Drawing.Color.Red;
            this.txtFaturaAdedi.Location = new System.Drawing.Point(805, 5);
            this.txtFaturaAdedi.Name = "txtFaturaAdedi";
            this.txtFaturaAdedi.Size = new System.Drawing.Size(16, 14);
            this.txtFaturaAdedi.TabIndex = 91;
            this.txtFaturaAdedi.Text = "0";
            // 
            // txtToplamTutar
            // 
            this.txtToplamTutar.AutoSize = true;
            this.txtToplamTutar.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtToplamTutar.ForeColor = System.Drawing.Color.Red;
            this.txtToplamTutar.Location = new System.Drawing.Point(532, 5);
            this.txtToplamTutar.Name = "txtToplamTutar";
            this.txtToplamTutar.Size = new System.Drawing.Size(16, 14);
            this.txtToplamTutar.TabIndex = 90;
            this.txtToplamTutar.Text = "0";
            // 
            // txtToplamKdv
            // 
            this.txtToplamKdv.AutoSize = true;
            this.txtToplamKdv.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtToplamKdv.ForeColor = System.Drawing.Color.Red;
            this.txtToplamKdv.Location = new System.Drawing.Point(321, 5);
            this.txtToplamKdv.Name = "txtToplamKdv";
            this.txtToplamKdv.Size = new System.Drawing.Size(16, 14);
            this.txtToplamKdv.TabIndex = 89;
            this.txtToplamKdv.Text = "0";
            // 
            // txtToplamKdvsiz
            // 
            this.txtToplamKdvsiz.AutoSize = true;
            this.txtToplamKdvsiz.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtToplamKdvsiz.ForeColor = System.Drawing.Color.Red;
            this.txtToplamKdvsiz.Location = new System.Drawing.Point(119, 5);
            this.txtToplamKdvsiz.Name = "txtToplamKdvsiz";
            this.txtToplamKdvsiz.Size = new System.Drawing.Size(16, 14);
            this.txtToplamKdvsiz.TabIndex = 88;
            this.txtToplamKdvsiz.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(652, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 14);
            this.label1.TabIndex = 83;
            this.label1.Text = "Toplam Fatura Adedi :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(7, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 14);
            this.label4.TabIndex = 73;
            this.label4.Text = "Toplam Kdvsiz :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(227, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 14);
            this.label5.TabIndex = 74;
            this.label5.Text = "Toplam Kdv :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(428, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 14);
            this.label6.TabIndex = 79;
            this.label6.Text = "Toplam Tutar :";
            // 
            // checkFaturaSec
            // 
            this.checkFaturaSec.AutoSize = true;
            this.checkFaturaSec.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.checkFaturaSec.Location = new System.Drawing.Point(10, 81);
            this.checkFaturaSec.Name = "checkFaturaSec";
            this.checkFaturaSec.Size = new System.Drawing.Size(183, 18);
            this.checkFaturaSec.TabIndex = 91;
            this.checkFaturaSec.Text = "Faturaların Tümünü Seç";
            this.checkFaturaSec.UseVisualStyleBackColor = true;
            this.checkFaturaSec.CheckedChanged += new System.EventHandler(this.checkFaturaSec_CheckedChanged);
            // 
            // radioDetayli
            // 
            this.radioDetayli.AutoSize = true;
            this.radioDetayli.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.radioDetayli.Location = new System.Drawing.Point(206, 81);
            this.radioDetayli.Name = "radioDetayli";
            this.radioDetayli.Size = new System.Drawing.Size(72, 18);
            this.radioDetayli.TabIndex = 92;
            this.radioDetayli.Text = "Detaylı";
            this.radioDetayli.UseVisualStyleBackColor = true;
            // 
            // radioKumulatif
            // 
            this.radioKumulatif.AutoSize = true;
            this.radioKumulatif.Checked = true;
            this.radioKumulatif.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.radioKumulatif.Location = new System.Drawing.Point(284, 81);
            this.radioKumulatif.Name = "radioKumulatif";
            this.radioKumulatif.Size = new System.Drawing.Size(88, 18);
            this.radioKumulatif.TabIndex = 93;
            this.radioKumulatif.TabStop = true;
            this.radioKumulatif.Text = "Kümülatif";
            this.radioKumulatif.UseVisualStyleBackColor = true;
            // 
            // listViewUrun
            // 
            this.listViewUrun.CheckBoxes = true;
            this.listViewUrun.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnBos,
            this.columnHeader7,
            this.columnHeader5,
            this.columnHeader10,
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6,
            this.columnHeader21,
            this.columnHeader9,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader2,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader8});
            this.listViewUrun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewUrun.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.listViewUrun.FullRowSelect = true;
            this.listViewUrun.GridLines = true;
            this.listViewUrun.HideSelection = false;
            this.listViewUrun.Location = new System.Drawing.Point(3, 18);
            this.listViewUrun.Name = "listViewUrun";
            this.listViewUrun.Size = new System.Drawing.Size(1181, 335);
            this.listViewUrun.TabIndex = 100000000;
            this.listViewUrun.UseCompatibleStateImageBehavior = false;
            this.listViewUrun.View = System.Windows.Forms.View.Details;
            this.listViewUrun.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewUrun_MouseDoubleClick);
            this.listViewUrun.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewUrun_ColumnClick);
            // 
            // columnBos
            // 
            this.columnBos.Text = "";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Invoice Number";
            this.columnHeader7.Width = 141;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "sFisTipi";
            this.columnHeader5.Width = 84;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "dteFisTarihi";
            this.columnHeader10.Width = 108;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "lToplamMiktar";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "lKdv1";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "nKdvOrani1";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "lKdvMatrahi1";
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Kdv1 Toplam";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "lKdv2";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "nKdvOrani2";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "lKdvMatrahi2";
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Kdv2 Toplam";
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Kdv Toplam";
            this.columnHeader23.Width = 65;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Kdvsiz Toplam";
            this.columnHeader2.Width = 85;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "lNetTutar";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "sYaziIle";
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "EINVOICE";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "sKodu";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "sAciklama";
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "sVergiDairesi";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "sVergiNo";
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "lPesinat";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "dteKayitTarihi";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.listViewUrun);
            this.groupBox2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(12, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1187, 356);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "E-Arşiv ve E-Fatura Listesi";
            // 
            // toolFiltrele
            // 
            this.toolFiltrele.Image = global::siparis_giris.Properties.Resources.Search;
            this.toolFiltrele.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFiltrele.Name = "toolFiltrele";
            this.toolFiltrele.Size = new System.Drawing.Size(109, 22);
            this.toolFiltrele.Text = "Filtrele ( F3 )";
            this.toolFiltrele.Click += new System.EventHandler(this.toolFiltrele_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.Image = global::siparis_giris.Properties.Resources.excel;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(192, 22);
            this.toolStripButton1.Text = "Excel Kaydet ( CTRL + S )";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolFaturaGoster
            // 
            this.toolFaturaGoster.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolFaturaGoster.Image = global::siparis_giris.Properties.Resources.printer;
            this.toolFaturaGoster.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFaturaGoster.Name = "toolFaturaGoster";
            this.toolFaturaGoster.Size = new System.Drawing.Size(214, 22);
            this.toolFaturaGoster.Text = "A4 Fatura Çıkart ( CTRL + F )";
            this.toolFaturaGoster.Click += new System.EventHandler(this.toolFaturaGoster_Click);
            // 
            // toolFaturaCikart
            // 
            this.toolFaturaCikart.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolFaturaCikart.Image = global::siparis_giris.Properties.Resources.page_copy;
            this.toolFaturaCikart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFaturaCikart.Name = "toolFaturaCikart";
            this.toolFaturaCikart.Size = new System.Drawing.Size(250, 22);
            this.toolFaturaCikart.Text = "Seçili Faturaları Çıkart ( CTRL + P )";
            this.toolFaturaCikart.Click += new System.EventHandler(this.toolFaturaCikart_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton2.Image = global::siparis_giris.Properties.Resources._1302562097_list_edit;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(220, 22);
            this.toolStripButton2.Text = "Slip Fatura Çıkart ( CTRL + K )";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::siparis_giris.Properties.Resources.Search;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(107, 22);
            this.toolStripButton3.Text = "Fiş Tipi ( F5 )";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // frmFaturaRapor
            // 
            this.AcceptButton = this.btnUrunAra;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1211, 510);
            this.Controls.Add(this.radioKumulatif);
            this.Controls.Add(this.radioDetayli);
            this.Controls.Add(this.checkFaturaSec);
            this.Controls.Add(this.panelMagazaEnvanter);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmFaturaRapor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "  E-Arşiv ve E-Fatura Listesi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmSiparisGonder_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmFaturaRapor_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panelMagazaEnvanter.ResumeLayout(false);
            this.panelMagazaEnvanter.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUrunAra;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolFiltrele;
        private System.Windows.Forms.DateTimePicker dateSatisTarihiBit;
        private System.Windows.Forms.Label labelIslemTarihiTire;
        private System.Windows.Forms.DateTimePicker dateSatisTarihiBas;
        private System.Windows.Forms.Label labelIslemTarihi;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel panelMagazaEnvanter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton toolFaturaGoster;
        private System.Windows.Forms.ToolStripButton toolFaturaCikart;
        private System.Windows.Forms.CheckBox checkFaturaSec;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button btnUnvanTemizle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUnvan;
        private System.Windows.Forms.Label txtToplamKdvsiz;
        private System.Windows.Forms.Label txtFaturaAdedi;
        private System.Windows.Forms.Label txtToplamTutar;
        private System.Windows.Forms.Label txtToplamKdv;
        private System.Windows.Forms.Button btnMusteriAra;
        private System.Windows.Forms.RadioButton radioDetayli;
        private System.Windows.Forms.RadioButton radioKumulatif;
        private System.Windows.Forms.ListView listViewUrun;
        private System.Windows.Forms.ColumnHeader columnBos;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
    }
}