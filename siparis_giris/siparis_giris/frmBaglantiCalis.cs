﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using DataSiparis;

namespace siparis_giris
{
    public partial class frmBaglantiCalis : Form
    {
        public frmBaglantiCalis()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        int i = 0;

        Thread _thread2;
        Thread _thread;
        private void frmBaglantiCalis_Load(object sender, EventArgs e)
        {
            _thread = new Thread(new ThreadStart(Nokta));
            _thread.Start();
            _thread2 = new Thread(new ThreadStart(Baglanti));
            _thread2.Start();
        }

        private void Baglanti()
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
            }
            catch
            {
                MessageBox.Show("Veritabanı bağlantısında hata bulunmaktadır.Bağlantı ayarları panelinden kontrol ediniz.", "Bağlantı Ayarları", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                con.Close();
            }
            //Baglanti2();
            this.Close();
        }

        private void Nokta()
        {
            Thread.Sleep(500);
            switch (i)
            {
                case 0:
                    labelNokta.Text = ".";
                    i = 1;
                    break;
                case 1:
                    labelNokta.Text = "..";
                    i = 2;
                    break;
                case 2:
                    labelNokta.Text = "...";
                    i = 0;
                    break;
                default:
                    break;
            }
            Nokta();
        }

        private void frmBaglantiCalis_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_thread != null)
                _thread.Abort();
            _thread = null;
            labelNokta.Text = "";
            label1.Text = "Kapatılıyor.Lütfen bekleyiniz.";
            if (_thread2 != null)
                _thread2.Abort();
            _thread2 = null;
        }
    }
}
