﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataSiparis;
using System.Security.Cryptography;
using System.IO;

namespace siparis_giris
{
    public class RegistryProvider
    {
        private static string Encrypt(string clearText)
        {
            string EncryptionKey = "Otr%15";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x54, 0x5d, 0x12, 0x66, 0x34, 0x56, 0x67 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static void BaglantiAyarlari(string _server, string _kullaniciAdi, string _sifre, string _veritabani, Form frm)
        {
            try
            {
                Settings settings = new Settings();
                clsSettings.Server = _server;
                clsSettings.KullaniciAdi = _kullaniciAdi;
                clsSettings.Sifre = _sifre;
                clsSettings.Veritabani = _veritabani;
                settings.PutSetting("Server", Encrypt(_server));
                settings.PutSetting("KullaniciAdi", Encrypt(_kullaniciAdi));
                settings.PutSetting("Sifre", Encrypt(_sifre));
                settings.PutSetting("Veritabani", Encrypt(_veritabani));
            }
            catch
            {
                MessageBox.Show("Bağlantı ayarları kaydedilirken hata ile karşılaşıldı.", "Bağlantı Ayarları", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
