﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace db_fatura
{
    public class tbFatura
    {
        private string _invoiceNumber;
        public string InvoiceNumber { get { return _invoiceNumber; } set { _invoiceNumber = value; } }

        private string _nStokFisiID;
        public string nStokFisiID { get { return _nStokFisiID; } set { _nStokFisiID = value; } }

        private string _nAlisverisID;
        public string nAlisverisID { get { return _nAlisverisID; } set { _nAlisverisID = value; } }

        private string _sFisTipi;
        public string sFisTipi { get { return _sFisTipi; } set { _sFisTipi = value; } }

        private DateTime? _dteFisTarihi;
        public DateTime? dteFisTarihi { get { return _dteFisTarihi; } set { _dteFisTarihi = value; } }

        private decimal _lToplamMiktar;
        public decimal lToplamMiktar { get { return _lToplamMiktar; } set { _lToplamMiktar = value; } }

        private decimal _lMalBedeli;
        public decimal lMalBedeli { get { return _lMalBedeli; } set { _lMalBedeli = value; } }

        private decimal _lKdv1;
        public decimal lKdv1 { get { return _lKdv1; } set { _lKdv1 = value; } }

        private decimal _nKdvOrani1;
        public decimal nKdvOrani1 { get { return _nKdvOrani1; } set { _nKdvOrani1 = value; } }

        private decimal _lKdvMatrahi1;
        public decimal lKdvMatrahi1 { get { return _lKdvMatrahi1; } set { _lKdvMatrahi1 = value; } }

        private decimal _lKdv1Toplam;
        public decimal lKdv1Toplam { get { return _lKdv1Toplam; } set { _lKdv1Toplam = value; } }

        private decimal _lKdv2;
        public decimal lKdv2 { get { return _lKdv2; } set { _lKdv2 = value; } }

        private decimal _nKdvOrani2;
        public decimal nKdvOrani2 { get { return _nKdvOrani2; } set { _nKdvOrani2 = value; } }

        private decimal _lKdv2Toplam;
        public decimal lKdv2Toplam { get { return _lKdv2Toplam; } set { _lKdv2Toplam = value; } }

        private decimal _lKdvMatrahi2;
        public decimal lKdvMatrahi2 { get { return _lKdvMatrahi2; } set { _lKdvMatrahi2 = value; } }

        private decimal _kdvToplam;
        public decimal KdvToplam { get { return _kdvToplam; } set { _kdvToplam = value; } }

        private decimal _kdvsizToplam;
        public decimal KdvsizToplam { get { return _kdvsizToplam; } set { _kdvsizToplam = value; } }

        private decimal _lNetTutar;
        public decimal lNetTutar { get { return _lNetTutar; } set { _lNetTutar = value; } }

        private string _sYaziIle;
        public string sYaziIle { get { return _sYaziIle; } set { _sYaziIle = value; } }

        private string _eInvoice;
        public string eInvoice { get { return _eInvoice; } set { _eInvoice = value; } }

        private string _sKodu;
        public string sKodu { get { return _sKodu; } set { _sKodu = value; } }

        private string _sAciklama;
        public string sAciklama { get { return _sAciklama; } set { _sAciklama = value; } }

        private string _sAdi;
        public string sAdi { get { return _sAdi; } set { _sAdi = value; } }

        private string _sSoyadi;
        public string sSoyadi { get { return _sSoyadi; } set { _sSoyadi = value; } }

        private string _sEvAdresi1;
        public string sEvAdresi1 { get { return _sEvAdresi1; } set { _sEvAdresi1 = value; } }

        private string _sEvAdresi2;
        public string sEvAdresi2 { get { return _sEvAdresi2; } set { _sEvAdresi2 = value; } }

        private string _sEvSemt;
        public string sEvSemt { get { return _sEvSemt; } set { _sEvSemt = value; } }

        private string _sEvIl;
        public string sEvIl { get { return _sEvIl; } set { _sEvIl = value; } }

        private string _sGSM;
        public string sGSM { get { return _sGSM; } set { _sGSM = value; } }

        private string _sCuzdanKayitNO;
        public string sCuzdanKayitNO { get { return _sCuzdanKayitNO; } set { _sCuzdanKayitNO = value; } }

        private string _sVergiDairesi;
        public string sVergiDairesi { get { return _sVergiDairesi; } set { _sVergiDairesi = value; } }

        private string _sVergiNo;
        public string sVergiNo { get { return _sVergiNo; } set { _sVergiNo = value; } }

        private decimal _lPesinat;
        public decimal lPesinat { get { return _lPesinat; } set { _lPesinat = value; } }

        private string _odemeAciklama;
        public string OdemeAciklama { get { return _odemeAciklama; } set { _odemeAciklama = value; } }

        private int _odemeTipi;
        public int OdemeTipi { get { return _odemeTipi; } set { _odemeTipi = value; } }

        private string _sDepo;
        public string sDepo { get { return _sDepo; } set { _sDepo = value; } }

        private DateTime? _dteFisKayitTarihi;
        public DateTime? dteFisKayitTarihi { get { return _dteFisKayitTarihi; } set { _dteFisKayitTarihi = value; } }

        private DateTime? _kayitTarihi;
        public DateTime? KayitTarihi { get { return _kayitTarihi; } set { _kayitTarihi = value; } }

        private string _ettn;
        public string ETTN { get { return _ettn; } set { _ettn = value; } }

        private string _stokAciklama;
        public string StokAciklama { get { return _stokAciklama; } set { _stokAciklama = value; } }

        private int _nGirisCikis;
        public int nGirisCikis { get { return _nGirisCikis; } set { _nGirisCikis = value; } }

        private string _senaryo;
        public string Senaryo { get { return _senaryo; } set { _senaryo = value; } }

        private string _faturaTipi;
        public string FaturaTipi { get { return _faturaTipi; } set { _faturaTipi = value; } }
    }

    public class tbFaturaProvider
    {
        public static bool FaturaVarmi(tbFatura _faturaGuncelle, SqlConnection con)
        {
            string _query = "select COUNT(*) from tbEFatura where InvoiceNumber = '" + _faturaGuncelle.InvoiceNumber + "'";
            bool _deger = false;
            SqlCommand cmd = new SqlCommand(_query, con);
            cmd.CommandTimeout = 300;
            int _varmi = 0;
            try
            {
                if (int.TryParse(cmd.ExecuteScalar().ToString(), out _varmi))
                    _deger = _varmi == 1;
                else
                    _deger = false;
            }
            catch
            {
                return false;
            }

            return _deger;
        }

        public static bool FaturaGuncelle(tbFatura _faturaGuncelle, SqlConnection con)
        {
            string _query = "insert into tbEFatura (InvoiceNumber,nStokFisiID,nAlisverisID,sFisTipi,dteFisTarihi,lToplamMiktar,lMalBedeli,lKdv1,nKdvOrani1,lKdvMatrahi1,lKdv1Toplam,lKdv2,nKdvOrani2,lKdv2Toplam,lKdvMatrahi2,kdvToplam,kdvsizToplam,lNetTutar,sYaziIle,eInvoice,sKodu,sAciklama,sSoyadi,sEvAdresi1,sEvAdresi2,sEvSemt,sEvIl,sGSM,sCuzdanKayitNO,sVergiDairesi,sVergiNo,lPesinat,odemeAciklama,odemeTipi,fisKayitTarihi,sDepo,ETTN,StokAciklama,nGirisCikis,Senaryo,FaturaTipi,kayitTarihi) values ('";
            _query += _faturaGuncelle.InvoiceNumber; // invoice number
            _query += "','" + _faturaGuncelle.nStokFisiID; // nStokFisiID
            _query += "','" + _faturaGuncelle.nAlisverisID; // nStokFisiID
            _query += "','" + _faturaGuncelle.sFisTipi; // fiş tipi
            _query += "','" + (_faturaGuncelle.dteFisTarihi.HasValue ? _faturaGuncelle.dteFisTarihi.Value.Month + "/" + _faturaGuncelle.dteFisTarihi.Value.Day + "/" + _faturaGuncelle.dteFisTarihi.Value.Year + " " + _faturaGuncelle.dteFisTarihi.Value.ToLongTimeString() : ""); // fiş tarihi
            _query += "'," + _faturaGuncelle.lToplamMiktar.ToString().Replace(',', '.'); // toplam miktar
            _query += "," + _faturaGuncelle.lMalBedeli.ToString().Replace(',', '.'); // mal bedeli
            _query += "," + _faturaGuncelle.lKdv1.ToString().Replace(',', '.'); // kdv 1
            _query += "," + _faturaGuncelle.nKdvOrani1.ToString().Replace(',', '.'); // kdv orani 1
            _query += "," + _faturaGuncelle.lKdvMatrahi1.ToString().Replace(',', '.'); // kdv matrahi 1
            _query += "," + _faturaGuncelle.lKdv1Toplam.ToString().Replace(',', '.'); // kdv toplam 1
            _query += "," + _faturaGuncelle.lKdv2.ToString().Replace(',', '.'); // kdv 2
            _query += "," + _faturaGuncelle.nKdvOrani2.ToString().Replace(',', '.'); // kdv orani 2
            _query += "," + _faturaGuncelle.lKdv2Toplam.ToString().Replace(',', '.'); // kdv toplam 2
            _query += "," + _faturaGuncelle.lKdvMatrahi2.ToString().Replace(',', '.'); // kdv matrahı 2
            _query += "," + _faturaGuncelle.KdvToplam.ToString().Replace(',', '.'); // kdv toplam 
            _query += "," + _faturaGuncelle.KdvsizToplam.ToString().Replace(',', '.'); // kdvsiz toplam
            _query += "," + _faturaGuncelle.lNetTutar.ToString().Replace(',', '.'); // net tutar
            _query += ",'" + _faturaGuncelle.sYaziIle; // yazı ile
            _query += "','" + _faturaGuncelle.eInvoice; // e invoice
            _query += "','" + _faturaGuncelle.sKodu; // stok kodu
            _query += "','" + _faturaGuncelle.sAciklama; // stok açıklaması
            _query += "','" + _faturaGuncelle.sSoyadi; // soyad
            _query += "','" + _faturaGuncelle.sEvAdresi1; // sEvAdresi1
            _query += "','" + _faturaGuncelle.sEvAdresi2; // sEvAdresi2
            _query += "','" + _faturaGuncelle.sEvSemt; // sEvSemt
            _query += "','" + _faturaGuncelle.sEvIl; // sEvIl
            _query += "','" + _faturaGuncelle.sGSM; // sGSM
            _query += "','" + _faturaGuncelle.sCuzdanKayitNO; // cüzdan kayıt no
            _query += "','" + _faturaGuncelle.sVergiDairesi; // vergi dairesi
            _query += "','" + _faturaGuncelle.sVergiNo; // vergi no
            _query += "'," + _faturaGuncelle.lPesinat.ToString().Replace(',', '.'); // peşinat
            _query += ",'" + _faturaGuncelle.OdemeAciklama; // ödeme açıklama
            _query += "'," + _faturaGuncelle.OdemeTipi.ToString().Replace(',', '.'); // ödeme tipi
            _query += ",'" + (_faturaGuncelle.dteFisKayitTarihi.HasValue ? _faturaGuncelle.dteFisKayitTarihi.Value.Month + "/" + _faturaGuncelle.dteFisKayitTarihi.Value.Day + "/" + _faturaGuncelle.dteFisKayitTarihi.Value.Year + " " + _faturaGuncelle.dteFisKayitTarihi.Value.ToLongTimeString() : ""); // fiş kayıt tarihi
            _query += "','" + _faturaGuncelle.sDepo; // depo
            _query += "','" + _faturaGuncelle.ETTN; // ETTN
            _query += "','" + _faturaGuncelle.StokAciklama; // Stok Açıklama
            _query += "'," + _faturaGuncelle.nGirisCikis;
            _query += ",'" + _faturaGuncelle.Senaryo;
            _query += "','" + _faturaGuncelle.FaturaTipi;
            _query += "',getdate())";

            bool _deger = false;
            SqlCommand cmd = new SqlCommand(_query, con);
            cmd.CommandTimeout = 300;
            try
            {
                cmd.ExecuteNonQuery();
                _deger = true;
            }
            catch
            {
                return false;
            }

            return _deger;
        }

        public static bool FaturaTablosunuSil()
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _query = "delete from tbEFatura";

            bool _deger = false;
            SqlCommand cmd = new SqlCommand(_query, con);
            cmd.CommandTimeout = 300;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                _deger = true;
            }
            catch
            {
                return false;
            }
            finally
            {
                con.Close();
            }

            return _deger;
        }

        public static tbFatura FaturaSonIslemGetir()
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _query = "select top 1 fisKayitTarihi from tbEFatura order by fisKayitTarihi desc";

            SqlCommand cmd = new SqlCommand(_query, con);
            DataTable _table = new DataTable("dsTablo");
            _table.Columns.Add("fisKayitTarihi", typeof(string));
            cmd.CommandTimeout = 300;
            tbFatura _fatura = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFatura> listFatura = new List<tbFatura>();
            try
            {
                con.Open();
                da.Fill(_table);
                DateTime _date = DateTime.Now;
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _fatura = new tbFatura();
                    if (DateTime.TryParse(_table.Rows[i][0].ToString(), out _date))
                        _fatura.dteFisKayitTarihi = _date;
                    else
                        _fatura.dteFisKayitTarihi = null;
                    listFatura.Add(_fatura);
                }
            }
            catch
            {
                return null;
            }
            finally { con.Close(); }
            if (listFatura.Count > 0)
                return listFatura[0];
            else
                return new tbFatura();
        }

        public static List<tbFatura> FaturalariGetir(ProgressBar progressIslem)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select pk.InvoiceNumber,pk.nStokFisiID,pk.nAlisverisID,od.sFisTipi,pk.dteFaturaTarihi,pk.lToplamMiktar,pk.lKdv1,pk.nKdvOrani1,pk.lKdvMatrahi1,pk.lKdv2,pk.nKdvOrani2,pk.lKdvMatrahi2,pk.lNetTutar";
            _command += ",pk.sYaziIle,pk.EINVOICE,pk.lKodu,pk.sAdi,pk.sSoyadi,pk.sEvAdresi1,pk.sEvAdresi2,pk.sEvSemt,pk.sEvIl,pk.sGSM,pk.sCuzdanKayitNo,pk.sVergiDairesi,pk.sVergiNo,od.lPesinat";
            _command += ",od.sAciklama as OdemeAciklama,od.nOdemeTipi as OdemeTipi,ft.sAciklama as OdemeTipiAciklama,pk.dteKayitTarihi,pk.sMagaza,pk.ETTN,st.sAciklama1,st.sAciklama2,st.sAciklama3,st.sAciklama4,st.sAciklama5 from PKFatura pk ";
            _command += "left join PKOdemeSekli od on pk.InvoiceNumber = od.InvoiceNumber left join tbOdemeTipiFatura ft on od.nOdemeTipi = ft.nOdemeTipi left join PFStokFisiAciklamasi st on st.InvoiceNumber = pk.InvoiceNumber ";
            SqlCommand cmd = new SqlCommand(_command, con);
            DataTable _table = new DataTable("dsTablo");
            _table.Columns.Add("InvoiceNumber", typeof(string));
            _table.Columns.Add("nStokFisiID", typeof(string));
            _table.Columns.Add("nAlisverisID", typeof(string));
            _table.Columns.Add("sFisTipi", typeof(string));
            _table.Columns.Add("dteFaturaTarihi", typeof(string));
            _table.Columns.Add("lToplamMiktar", typeof(decimal));
            _table.Columns.Add("lKdv1", typeof(decimal));
            _table.Columns.Add("nKdvOrani1", typeof(decimal));
            _table.Columns.Add("lKdvMatrahi1", typeof(decimal));
            _table.Columns.Add("lKdv2", typeof(decimal));
            _table.Columns.Add("nKdvOrani2", typeof(decimal));
            _table.Columns.Add("lKdvMatrahi2", typeof(decimal));
            _table.Columns.Add("lNetTutar", typeof(decimal));
            _table.Columns.Add("sYaziIle", typeof(string));
            _table.Columns.Add("EINVOICE", typeof(int));
            _table.Columns.Add("lKodu", typeof(string));
            _table.Columns.Add("sAdi", typeof(string));
            _table.Columns.Add("sSoyadi", typeof(string));
            _table.Columns.Add("sEvAdresi1", typeof(string));
            _table.Columns.Add("sEvAdresi2", typeof(string));
            _table.Columns.Add("sEvSemt", typeof(string));
            _table.Columns.Add("sEvIl", typeof(string));
            _table.Columns.Add("sGSM", typeof(string));
            _table.Columns.Add("sCuzdanKayitNo", typeof(string));
            _table.Columns.Add("sVergiDairesi", typeof(string));
            _table.Columns.Add("sVergiNo", typeof(string));
            _table.Columns.Add("lPesinat", typeof(decimal));
            _table.Columns.Add("OdemeAciklama", typeof(string));
            _table.Columns.Add("OdemeTipi", typeof(int));
            _table.Columns.Add("OdemeTipiAciklama", typeof(string));
            _table.Columns.Add("dteKayitTarihi", typeof(string));
            _table.Columns.Add("sMagaza", typeof(string));
            _table.Columns.Add("ETTN", typeof(string));
            _table.Columns.Add("sAciklama1", typeof(string));
            _table.Columns.Add("sAciklama2", typeof(string));
            _table.Columns.Add("sAciklama3", typeof(string));
            _table.Columns.Add("sAciklama4", typeof(string));
            _table.Columns.Add("sAciklama5", typeof(string));
            cmd.CommandTimeout = 1800;
            tbFatura _fatura = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            progressIslem.Minimum = 0;
            progressIslem.Value = 0;
            progressIslem.Refresh();
            Application.DoEvents();
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            try
            {
                con.Open();
                da.Fill(_table);
                progressIslem.Maximum = _table.Rows.Count;
                StringBuilder _stokAciklama = new StringBuilder();
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _fatura = new tbFatura();
                    _fatura.InvoiceNumber = _table.Rows[i][0].ToString();
                    _fatura.nStokFisiID = _table.Rows[i][1].ToString();
                    _fatura.nAlisverisID = _table.Rows[i][2].ToString();
                    int _odemeTipi = 0;
                    if (int.TryParse(_table.Rows[i][28].ToString(), out _odemeTipi))
                        _fatura.OdemeTipi = _odemeTipi;
                    else
                        _fatura.OdemeTipi = 0;
                    switch (_table.Rows[i][3].ToString().Trim())
                    {
                        case "SK":
                        case "K":
                            _fatura.sFisTipi = "SENET";
                            break;
                        case "SP":
                        case "P":
                            if (_fatura.OdemeTipi == 1)
                                _fatura.sFisTipi = "PEŞİN NAKİT";
                            else if (_fatura.OdemeTipi == 2)
                                _fatura.sFisTipi = "PEŞİN KREDİ KARTI";
                            else if (_fatura.OdemeTipi == 5)
                                _fatura.sFisTipi = "PEŞİN HAVALE";
                            else
                                _fatura.sFisTipi = "PEŞİN";
                            break;
                        default:
                            if (_fatura.InvoiceNumber.ToUpper().Contains("EMR") || _fatura.InvoiceNumber.ToUpper().Contains("AMR"))
                                _fatura.sFisTipi = "DİĞER";
                            else
                                _fatura.sFisTipi = "SENET";
                            break;
                    }
                    DateTime _dteFisTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][4].ToString(), out _dteFisTarihi))
                        _fatura.dteFisTarihi = _dteFisTarihi;
                    else
                        _fatura.dteFisTarihi = null;
                    if (!_fatura.dteFisTarihi.HasValue)
                        _fatura.dteFisTarihi = Convert.ToDateTime("01/01/1900");

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _fatura.lToplamMiktar = _miktar;
                    else
                        _fatura.lToplamMiktar = 0;

                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _fatura.lKdv1 = _miktar;
                    else
                        _fatura.lKdv1 = 0;

                    if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                        _fatura.nKdvOrani1 = _miktar;
                    else
                        _fatura.nKdvOrani1 = 0;

                    if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                        _fatura.lKdvMatrahi1 = _miktar;
                    else
                        _fatura.lKdvMatrahi1 = 0;
                    _fatura.lKdv1Toplam = _fatura.lKdv1 + _fatura.lKdvMatrahi1;

                    if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                        _fatura.lKdv2 = _miktar;
                    else
                        _fatura.lKdv2 = 0;

                    if (decimal.TryParse(_table.Rows[i][10].ToString(), out _miktar))
                        _fatura.nKdvOrani2 = _miktar;
                    else
                        _fatura.nKdvOrani2 = 0;

                    if (decimal.TryParse(_table.Rows[i][11].ToString(), out _miktar))
                        _fatura.lKdvMatrahi2 = _miktar;
                    else
                        _fatura.lKdvMatrahi2 = 0;
                    _fatura.lKdv2Toplam = _fatura.lKdv2 + _fatura.lKdvMatrahi2;
                    _fatura.KdvToplam = _fatura.lKdv1 + _fatura.lKdv2;
                    _fatura.KdvsizToplam = _fatura.lKdvMatrahi1 + _fatura.lKdvMatrahi2;
                    if (decimal.TryParse(_table.Rows[i][12].ToString(), out _miktar))
                        _fatura.lNetTutar = _miktar;
                    else
                        _fatura.lNetTutar = 0;
                    _fatura.sYaziIle = _table.Rows[i][13].ToString();
                    _fatura.eInvoice = _table.Rows[i][14].ToString();
                    _fatura.sKodu = _table.Rows[i][15].ToString();
                    _fatura.sAciklama = _table.Rows[i][16].ToString();
                    _fatura.sSoyadi = _table.Rows[i][17].ToString();
                    _fatura.sEvAdresi1 = _table.Rows[i][18].ToString();
                    _fatura.sEvAdresi2 = _table.Rows[i][19].ToString();
                    _fatura.sEvSemt = _table.Rows[i][20].ToString();
                    _fatura.sEvIl = _table.Rows[i][21].ToString();
                    _fatura.sGSM = _table.Rows[i][22].ToString();
                    _fatura.sCuzdanKayitNO = _table.Rows[i][23].ToString();
                    _fatura.sVergiDairesi = _table.Rows[i][24].ToString();
                    _fatura.sVergiNo = _table.Rows[i][25].ToString();

                    if (decimal.TryParse(_table.Rows[i][26].ToString(), out _miktar))
                        _fatura.lPesinat = _miktar;
                    else
                        _fatura.lPesinat = 0;
                    if (_fatura.lPesinat != 0)
                        _fatura.OdemeAciklama = _table.Rows[i][27].ToString();
                    else
                        _fatura.OdemeAciklama = "";
                    DateTime _dteFisKayitTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][30].ToString(), out _dteFisKayitTarihi))
                        _fatura.dteFisKayitTarihi = _dteFisKayitTarihi;
                    else
                        _fatura.dteFisKayitTarihi = null;
                    _fatura.sDepo = _table.Rows[i][31].ToString();
                    _fatura.ETTN = _table.Rows[i][32].ToString();
                    if (_stokAciklama.Length != 0)
                        _stokAciklama.Remove(0, _stokAciklama.Length);
                    if (_table.Rows[i][33].ToString().Trim().Length != 0)
                        _stokAciklama.Append(_table.Rows[i][33].ToString().Trim());
                    if (_table.Rows[i][34].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][33].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][34].ToString().Trim());
                    }
                    if (_table.Rows[i][35].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][34].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][35].ToString().Trim());
                    }
                    if (_table.Rows[i][36].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][35].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][36].ToString().Trim());
                    }
                    if (_table.Rows[i][37].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][36].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][37].ToString().Trim());
                    }
                    _fatura.StokAciklama = _stokAciklama.ToString();
                    _fatura.Senaryo = "TEMELFATURA";
                    _fatura.FaturaTipi = "SATIŞ";
                    _fatura.KayitTarihi = DateTime.Now;
                    listFaturaListesi.Add(_fatura);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaListesi;
        }

        public static List<tbFatura> PFFaturalariGetir(ProgressBar progressIslem)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select pf.InvoiceNumber,pf.nStokFisiID,pf.nAlisverisID,od.sFisTipi,pf.dteFisTarihi,pf.lToplamMiktar,pf.lKdv1,pf.nKdvOrani1,pf.lKdvMatrahi1,pf.lKdv2,pf.nKdvOrani2,pf.lKdvMatrahi2";
            _command += ",pf.lNetTutar,pf.sYaziIle,pf.EINVOICE,pf.lKodu,pf.sAdi,pf.sSoyadi,pf.sEvAdresi1,pf.sEvAdresi2,pf.sEvSemt,pf.sEvIl,pf.sGSM,pf.sCuzdanKayitNo,pf.sVergiDairesi,pf.sVergiNo,od.lPesinat";
            _command += ",od.sAciklama as OdemeAciklama,od.sFisTipi as OdemeFisTipi,od.nOdemeTipi as OdemeTipi,ft.sAciklama as OdemeTipiAciklama,pf.dteKayitTarihi,(select top 1 sDepo from PFFaturaDetay where InvoiceNumber = pf.InvoiceNumber) as sDepo,pf.ETTN,st.sAciklama1,st.sAciklama2,st.sAciklama3,st.sAciklama4,st.sAciklama5 from ";
            _command += "PFFatura pf left join PFOdemeSekli od on pf.InvoiceNumber = od.InvoiceNumber left join tbOdemeTipiFatura ft ";
            _command += "on od.nOdemeTipi = ft.nOdemeTipi left join PFStokFisiAciklamasi st on st.InvoiceNumber = pf.InvoiceNumber ";
            SqlCommand cmd = new SqlCommand(_command, con);
            DataTable _table = new DataTable("dsTablo");
            _table.Columns.Add("InvoiceNumber", typeof(string));
            _table.Columns.Add("nStokFisiID", typeof(string));
            _table.Columns.Add("nAlisverisID", typeof(string));
            _table.Columns.Add("sFisTipi", typeof(string));
            _table.Columns.Add("dteFisTarihi", typeof(string));
            _table.Columns.Add("lToplamMiktar", typeof(decimal));
            _table.Columns.Add("lKdv1", typeof(decimal));
            _table.Columns.Add("nKdvOrani1", typeof(decimal));
            _table.Columns.Add("lKdvMatrahi1", typeof(decimal));
            _table.Columns.Add("lKdv2", typeof(decimal));
            _table.Columns.Add("nKdvOrani2", typeof(decimal));
            _table.Columns.Add("lKdvMatrahi2", typeof(decimal));
            _table.Columns.Add("lNetTutar", typeof(decimal));
            _table.Columns.Add("sYaziIle", typeof(string));
            _table.Columns.Add("EINVOICE", typeof(int));
            _table.Columns.Add("lKodu", typeof(string));
            _table.Columns.Add("sAdi", typeof(string));
            _table.Columns.Add("sSoyadi", typeof(string));
            _table.Columns.Add("sEvAdresi1", typeof(string));
            _table.Columns.Add("sEvAdresi2", typeof(string));
            _table.Columns.Add("sEvSemt", typeof(string));
            _table.Columns.Add("sEvIl", typeof(string));
            _table.Columns.Add("sGSM", typeof(string));
            _table.Columns.Add("sCuzdanKayitNo", typeof(string));
            _table.Columns.Add("sVergiDairesi", typeof(string));
            _table.Columns.Add("sVergiNo", typeof(string));
            _table.Columns.Add("lPesinat", typeof(decimal));
            _table.Columns.Add("OdemeAciklama", typeof(string));
            _table.Columns.Add("OdemeTipi", typeof(int));
            _table.Columns.Add("OdemeTipiAciklama", typeof(string));
            _table.Columns.Add("dteKayitTarihi", typeof(string));
            _table.Columns.Add("sDepo", typeof(string));
            _table.Columns.Add("ETTN", typeof(string));
            _table.Columns.Add("sAciklama1", typeof(string));
            _table.Columns.Add("sAciklama2", typeof(string));
            _table.Columns.Add("sAciklama3", typeof(string));
            _table.Columns.Add("sAciklama4", typeof(string));
            _table.Columns.Add("sAciklama5", typeof(string));
            cmd.CommandTimeout = 1800;
            tbFatura _fatura = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            progressIslem.Minimum = 0;
            progressIslem.Value = 0;
            progressIslem.Refresh();
            Application.DoEvents();
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            try
            {
                con.Open();
                da.Fill(_table);
                progressIslem.Maximum = _table.Rows.Count;
                StringBuilder _stokAciklama = new StringBuilder();
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _fatura = new tbFatura();
                    _fatura.InvoiceNumber = _table.Rows[i][0].ToString();
                    _fatura.nStokFisiID = _table.Rows[i][1].ToString();
                    _fatura.nAlisverisID = _table.Rows[i][2].ToString();
                    int _odemeTipi = 0;
                    if (int.TryParse(_table.Rows[i][28].ToString(), out _odemeTipi))
                        _fatura.OdemeTipi = _odemeTipi;
                    else
                        _fatura.OdemeTipi = 0;
                    switch (_table.Rows[i][3].ToString().Trim())
                    {
                        case "SK":
                        case "K":
                            _fatura.sFisTipi = "SENET";
                            break;
                        case "SP":
                        case "P":
                            if (_fatura.OdemeTipi == 1)
                                _fatura.sFisTipi = "PEŞİN NAKİT";
                            else if (_fatura.OdemeTipi == 2)
                                _fatura.sFisTipi = "PEŞİN KREDİ KARTI";
                            else if (_fatura.OdemeTipi == 5)
                                _fatura.sFisTipi = "PEŞİN HAVALE";
                            else
                                _fatura.sFisTipi = "PEŞİN";
                            break;
                        default:
                            if (_fatura.InvoiceNumber.ToUpper().Contains("EMR") || _fatura.InvoiceNumber.ToUpper().Contains("AMR"))
                                _fatura.sFisTipi = "DİĞER";
                            else
                                _fatura.sFisTipi = "SENET";
                            break;
                    }
                    DateTime _dteFisTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][4].ToString(), out _dteFisTarihi))
                        _fatura.dteFisTarihi = _dteFisTarihi;
                    else
                        _fatura.dteFisTarihi = null;
                    if (!_fatura.dteFisTarihi.HasValue)
                        _fatura.dteFisTarihi = Convert.ToDateTime("01/01/1900");

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _fatura.lToplamMiktar = _miktar;
                    else
                        _fatura.lToplamMiktar = 0;

                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _fatura.lKdv1 = _miktar;
                    else
                        _fatura.lKdv1 = 0;

                    if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                        _fatura.nKdvOrani1 = _miktar;
                    else
                        _fatura.nKdvOrani1 = 0;

                    if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                        _fatura.lKdvMatrahi1 = _miktar;
                    else
                        _fatura.lKdvMatrahi1 = 0;

                    _fatura.lKdv1Toplam = _fatura.lKdv1 + _fatura.lKdvMatrahi1;

                    if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                        _fatura.lKdv2 = _miktar;
                    else
                        _fatura.lKdv2 = 0;

                    if (decimal.TryParse(_table.Rows[i][10].ToString(), out _miktar))
                        _fatura.nKdvOrani2 = _miktar;
                    else
                        _fatura.nKdvOrani2 = 0;

                    if (decimal.TryParse(_table.Rows[i][11].ToString(), out _miktar))
                        _fatura.lKdvMatrahi2 = _miktar;
                    else
                        _fatura.lKdvMatrahi2 = 0;

                    _fatura.lKdv2Toplam = _fatura.lKdv2 + _fatura.lKdvMatrahi2;
                    _fatura.KdvToplam = _fatura.lKdv1 + _fatura.lKdv2;
                    _fatura.KdvsizToplam = _fatura.lKdvMatrahi1 + _fatura.lKdvMatrahi2;
                    if (decimal.TryParse(_table.Rows[i][12].ToString(), out _miktar))
                        _fatura.lNetTutar = _miktar;
                    else
                        _fatura.lNetTutar = 0;
                    _fatura.sYaziIle = _table.Rows[i][13].ToString();
                    _fatura.eInvoice = _table.Rows[i][14].ToString();
                    _fatura.sKodu = _table.Rows[i][15].ToString();
                    _fatura.sAciklama = _table.Rows[i][16].ToString();
                    _fatura.sSoyadi = _table.Rows[i][17].ToString();
                    _fatura.sEvAdresi1 = _table.Rows[i][18].ToString();
                    _fatura.sEvAdresi2 = _table.Rows[i][19].ToString();
                    _fatura.sEvSemt = _table.Rows[i][20].ToString();
                    _fatura.sEvIl = _table.Rows[i][21].ToString();
                    _fatura.sGSM = _table.Rows[i][22].ToString();
                    _fatura.sCuzdanKayitNO = _table.Rows[i][23].ToString();
                    _fatura.sVergiDairesi = _table.Rows[i][24].ToString();
                    _fatura.sVergiNo = _table.Rows[i][25].ToString();

                    if (decimal.TryParse(_table.Rows[i][26].ToString(), out _miktar))
                        _fatura.lPesinat = _miktar;
                    else
                        _fatura.lPesinat = 0;
                    if (_fatura.lPesinat != 0)
                        _fatura.OdemeAciklama = _table.Rows[i][27].ToString();
                    else
                        _fatura.OdemeAciklama = "";
                    DateTime _dteFisKayitTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][30].ToString(), out _dteFisKayitTarihi))
                        _fatura.dteFisKayitTarihi = _dteFisKayitTarihi;
                    else
                        _fatura.dteFisKayitTarihi = null;
                    _fatura.sDepo = _table.Rows[i][31].ToString();
                    _fatura.ETTN = _table.Rows[i][32].ToString();
                    if (_stokAciklama.Length != 0)
                        _stokAciklama.Remove(0, _stokAciklama.Length);
                    if (_table.Rows[i][33].ToString().Trim().Length != 0)
                        _stokAciklama.Append(_table.Rows[i][33].ToString().Trim());
                    if (_table.Rows[i][34].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][33].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][34].ToString().Trim());
                    }
                    if (_table.Rows[i][35].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][34].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][35].ToString().Trim());
                    }
                    if (_table.Rows[i][36].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][35].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][36].ToString().Trim());
                    }
                    if (_table.Rows[i][37].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][36].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][37].ToString().Trim());
                    }
                    _fatura.StokAciklama = _stokAciklama.ToString();
                    _fatura.KayitTarihi = DateTime.Now;
                    _fatura.Senaryo = "TEMELFATURA";
                    _fatura.FaturaTipi = "SATIŞ";
                    listFaturaListesi.Add(_fatura);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaListesi;
        }

        public static List<tbFatura> PFASFaturalariGetir(ProgressBar progressIslem)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select pfa.*, (select top 1 sDepo from PFFaturaDetay where InvoiceNumber = pfa.InvoiceNumber) as sMagaza,st.sAciklama1,st.sAciklama2,st.sAciklama3,st.sAciklama4,st.sAciklama5 from PFASFatura pfa left join PFStokFisiAciklamasi st on st.InvoiceNumber = pfa.InvoiceNumber ";
            SqlCommand cmd = new SqlCommand(_command, con);
            DataTable _table = new DataTable("dsTablo");
            _table.Columns.Add("InvoiceNumber", typeof(string));
            _table.Columns.Add("nStokFisiID", typeof(string));
            _table.Columns.Add("nAlisverisID", typeof(string));
            _table.Columns.Add("sFisTipi", typeof(string));
            _table.Columns.Add("dteFisTarihi", typeof(string));
            _table.Columns.Add("lToplamMiktar", typeof(decimal));
            _table.Columns.Add("lKdv1", typeof(decimal));
            _table.Columns.Add("nKdvOrani1", typeof(decimal));
            _table.Columns.Add("lKdvMatrahi1", typeof(decimal));
            _table.Columns.Add("lKdv2", typeof(decimal));
            _table.Columns.Add("nKdvOrani2", typeof(decimal));
            _table.Columns.Add("lKdvMatrahi2", typeof(decimal));
            _table.Columns.Add("lNetTutar", typeof(decimal));
            _table.Columns.Add("sYaziIle", typeof(string));
            _table.Columns.Add("EINVOICE", typeof(int));
            _table.Columns.Add("sKodu", typeof(string));
            _table.Columns.Add("sAciklama", typeof(string));
            _table.Columns.Add("sAdres1", typeof(string));
            _table.Columns.Add("sAdres2", typeof(string));
            _table.Columns.Add("sSemt", typeof(string));
            _table.Columns.Add("sIl", typeof(string));
            _table.Columns.Add("sVergiDairesi", typeof(string));
            _table.Columns.Add("sVergiNo", typeof(string));
            _table.Columns.Add("dteKayitTarihi", typeof(string));
            _table.Columns.Add("ETTN", typeof(string));
            _table.Columns.Add("Senaryo", typeof(string));
            _table.Columns.Add("FaturaTipi", typeof(string));
            _table.Columns.Add("nGirisCikis", typeof(int));
            _table.Columns.Add("sMagaza", typeof(string));
            _table.Columns.Add("sAciklama1", typeof(string));
            _table.Columns.Add("sAciklama2", typeof(string));
            _table.Columns.Add("sAciklama3", typeof(string));
            _table.Columns.Add("sAciklama4", typeof(string));
            _table.Columns.Add("sAciklama5", typeof(string));
            cmd.CommandTimeout = 1800;
            tbFatura _fatura = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            progressIslem.Minimum = 0;
            progressIslem.Value = 0;
            progressIslem.Refresh();
            Application.DoEvents();
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            try
            {
                con.Open();
                da.Fill(_table);
                progressIslem.Maximum = _table.Rows.Count;
                StringBuilder _stokAciklama = new StringBuilder();
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _fatura = new tbFatura();
                    _fatura.InvoiceNumber = _table.Rows[i][0].ToString();
                    _fatura.nStokFisiID = _table.Rows[i][1].ToString();
                    _fatura.nAlisverisID = _table.Rows[i][2].ToString();
                    bool _faturaOlustumu = false;
                    if (!bool.TryParse(_table.Rows[i][23].ToString(), out _faturaOlustumu))
                        _faturaOlustumu = false;
                    if (_faturaOlustumu)
                        _fatura.sFisTipi = "FİYAT FARKI";
                    else
                    {
                        switch (_table.Rows[i][3].ToString().Trim())
                        {
                            case "SK":
                            case "K":
                                _fatura.sFisTipi = "SENET";
                                break;
                            case "SP":
                            case "P":
                                _fatura.sFisTipi = "PEŞİN";
                                break;
                            case "FA":
                                _fatura.sFisTipi = "İADE";
                                break;
                            default:
                                if (_fatura.InvoiceNumber.ToUpper().Contains("EMR") || _fatura.InvoiceNumber.ToUpper().Contains("AMR"))
                                    _fatura.sFisTipi = "DİĞER";
                                else
                                    _fatura.sFisTipi = "SENET";
                                break;
                        }
                    }
                    DateTime _dteFisTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][4].ToString(), out _dteFisTarihi))
                        _fatura.dteFisTarihi = _dteFisTarihi;
                    else
                        _fatura.dteFisTarihi = null;
                    if (!_fatura.dteFisTarihi.HasValue)
                        _fatura.dteFisTarihi = Convert.ToDateTime("01/01/1900");

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _fatura.lToplamMiktar = _miktar;
                    else
                        _fatura.lToplamMiktar = 0;

                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _fatura.lKdv1 = _miktar;
                    else
                        _fatura.lKdv1 = 0;

                    if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                        _fatura.nKdvOrani1 = _miktar;
                    else
                        _fatura.nKdvOrani1 = 0;

                    if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                        _fatura.lKdvMatrahi1 = _miktar;
                    else
                        _fatura.lKdvMatrahi1 = 0;

                    _fatura.lKdv1Toplam = _fatura.lKdv1 + _fatura.lKdvMatrahi1;

                    if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                        _fatura.lKdv2 = _miktar;
                    else
                        _fatura.lKdv2 = 0;

                    if (decimal.TryParse(_table.Rows[i][10].ToString(), out _miktar))
                        _fatura.nKdvOrani2 = _miktar;
                    else
                        _fatura.nKdvOrani2 = 0;

                    if (decimal.TryParse(_table.Rows[i][11].ToString(), out _miktar))
                        _fatura.lKdvMatrahi2 = _miktar;
                    else
                        _fatura.lKdvMatrahi2 = 0;

                    _fatura.lKdv2Toplam = _fatura.lKdv2 + _fatura.lKdvMatrahi2;
                    _fatura.KdvToplam = _fatura.lKdv1 + _fatura.lKdv2;
                    _fatura.KdvsizToplam = _fatura.lKdvMatrahi1 + _fatura.lKdvMatrahi2;
                    if (decimal.TryParse(_table.Rows[i][12].ToString(), out _miktar))
                        _fatura.lNetTutar = _miktar;
                    else
                        _fatura.lNetTutar = 0;
                    _fatura.sYaziIle = _table.Rows[i][13].ToString();
                    _fatura.eInvoice = _table.Rows[i][14].ToString();
                    _fatura.sKodu = _table.Rows[i][15].ToString();
                    _fatura.sAciklama = _table.Rows[i][16].ToString();
                    _fatura.sEvAdresi1 = _table.Rows[i][17].ToString();
                    _fatura.sEvAdresi2 = _table.Rows[i][18].ToString();
                    _fatura.sEvSemt = _table.Rows[i][19].ToString();
                    _fatura.sEvIl = _table.Rows[i][20].ToString();
                    _fatura.sVergiDairesi = _table.Rows[i][21].ToString();
                    _fatura.sVergiNo = _table.Rows[i][22].ToString();
                    _fatura.lPesinat = 0;
                    _fatura.OdemeTipi = 0;
                    DateTime _dteFisKayitTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][23].ToString(), out _dteFisKayitTarihi))
                        _fatura.dteFisKayitTarihi = _dteFisKayitTarihi;
                    else
                        _fatura.dteFisKayitTarihi = null;
                    _fatura.ETTN = _table.Rows[i][24].ToString();
                    _fatura.Senaryo = _table.Rows[i][25].ToString();
                    _fatura.FaturaTipi = _table.Rows[i][26].ToString();
                    _fatura.nGirisCikis = _table.Rows[i].IsNull("nGirisCikis") ? 0 : Convert.ToInt32(_table.Rows[i][27].ToString());
                    _fatura.sDepo = _table.Rows[i][28].ToString();
                    if (_stokAciklama.Length != 0)
                        _stokAciklama.Remove(0, _stokAciklama.Length);
                    if (_table.Rows[i][29].ToString().Trim().Length != 0)
                        _stokAciklama.Append(_table.Rows[i][29].ToString().Trim());
                    if (_table.Rows[i][30].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][29].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][30].ToString().Trim());
                    }
                    if (_table.Rows[i][31].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][30].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][31].ToString().Trim());
                    }
                    if (_table.Rows[i][32].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][31].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][32].ToString().Trim());
                    }
                    if (_table.Rows[i][33].ToString().Trim().Length != 0)
                    {
                        if (_table.Rows[i][32].ToString().Trim().Length != 0)
                            _stokAciklama.Append(" - ");
                        _stokAciklama.Append(_table.Rows[i][33].ToString().Trim());
                    }
                    _fatura.StokAciklama = _stokAciklama.ToString();
                    listFaturaListesi.Add(_fatura);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaListesi;
        }

        public static List<tbFatura> FaturaListesiHepsi()
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from tbEFatura ";
            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 1800;
            DataTable _table = new DataTable("dsTablo");
            tbFatura _fatura = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFatura> listFaturaListesi = new List<tbFatura>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _fatura = new tbFatura();
                    _fatura.InvoiceNumber = _table.Rows[i][0].ToString();
                    _fatura.nStokFisiID = _table.Rows[i][1].ToString();
                    _fatura.nAlisverisID = _table.Rows[i][2].ToString();
                    int _odemeTipi = 0;
                    if (int.TryParse(_table.Rows[i][33].ToString(), out _odemeTipi))
                        _fatura.OdemeTipi = _odemeTipi;
                    else
                        _fatura.OdemeTipi = 0;
                    switch (_table.Rows[i][3].ToString().Trim())
                    {
                        case "SK":
                        case "K":
                            _fatura.sFisTipi = "SENET";
                            break;
                        case "SP":
                        case "P":
                            if (_fatura.OdemeTipi == 1)
                                _fatura.sFisTipi = "PEŞİN NAKİT";
                            else if (_fatura.OdemeTipi == 2)
                                _fatura.sFisTipi = "PEŞİN KREDİ KARTI";
                            else if (_fatura.OdemeTipi == 5)
                                _fatura.sFisTipi = "PEŞİN HAVALE";
                            else
                                _fatura.sFisTipi = "PEŞİN";
                            break;
                        default:
                            if (_fatura.InvoiceNumber.ToUpper().Contains("EMR") || _fatura.InvoiceNumber.ToUpper().Contains("AMR"))
                                _fatura.sFisTipi = "DİĞER";
                            else
                                _fatura.sFisTipi = "SENET";
                            break;
                    }
                    DateTime _dteFisTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][4].ToString(), out _dteFisTarihi))
                        _fatura.dteFisTarihi = _dteFisTarihi;
                    else
                        _fatura.dteFisTarihi = null;
                    if (!_fatura.dteFisTarihi.HasValue)
                        _fatura.dteFisTarihi = Convert.ToDateTime("01/01/1900");

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _fatura.lToplamMiktar = _miktar;
                    else
                        _fatura.lToplamMiktar = 0;

                    if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                        _fatura.lKdv1 = _miktar;
                    else
                        _fatura.lKdv1 = 0;

                    if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                        _fatura.nKdvOrani1 = _miktar;
                    else
                        _fatura.nKdvOrani1 = 0;

                    if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                        _fatura.lKdvMatrahi1 = _miktar;
                    else
                        _fatura.lKdvMatrahi1 = 0;
                    _fatura.lKdv1Toplam = _fatura.lKdv1 + _fatura.lKdvMatrahi1;

                    if (decimal.TryParse(_table.Rows[i][11].ToString(), out _miktar))
                        _fatura.lKdv2 = _miktar;
                    else
                        _fatura.lKdv2 = 0;

                    if (decimal.TryParse(_table.Rows[i][12].ToString(), out _miktar))
                        _fatura.nKdvOrani2 = _miktar;
                    else
                        _fatura.nKdvOrani2 = 0;

                    if (decimal.TryParse(_table.Rows[i][13].ToString(), out _miktar))
                        _fatura.lKdvMatrahi2 = _miktar;
                    else
                        _fatura.lKdvMatrahi2 = 0;
                    _fatura.lKdv2Toplam = _fatura.lKdv2 + _fatura.lKdvMatrahi2;
                    _fatura.KdvToplam = _fatura.lKdv1 + _fatura.lKdv2;
                    _fatura.KdvsizToplam = _fatura.lKdvMatrahi1 + _fatura.lKdvMatrahi2;
                    if (decimal.TryParse(_table.Rows[i][17].ToString(), out _miktar))
                        _fatura.lNetTutar = _miktar;
                    else
                        _fatura.lNetTutar = 0;
                    _fatura.sYaziIle = _table.Rows[i][18].ToString();
                    _fatura.eInvoice = _table.Rows[i][19].ToString();
                    _fatura.sKodu = _table.Rows[i][20].ToString();
                    _fatura.sAciklama = _table.Rows[i][21].ToString();
                    _fatura.sSoyadi = _table.Rows[i][22].ToString();
                    _fatura.sEvAdresi1 = _table.Rows[i][23].ToString();
                    _fatura.sEvAdresi2 = _table.Rows[i][24].ToString();
                    _fatura.sEvSemt = _table.Rows[i][25].ToString();
                    _fatura.sEvIl = _table.Rows[i][26].ToString();
                    _fatura.sGSM = _table.Rows[i][27].ToString();
                    _fatura.sCuzdanKayitNO = _table.Rows[i][28].ToString();
                    _fatura.sVergiDairesi = _table.Rows[i][29].ToString();
                    _fatura.sVergiNo = _table.Rows[i][30].ToString();

                    if (decimal.TryParse(_table.Rows[i][31].ToString(), out _miktar))
                        _fatura.lPesinat = _miktar;
                    else
                        _fatura.lPesinat = 0;
                    if (_fatura.lPesinat != 0)
                        _fatura.OdemeAciklama = _table.Rows[i][32].ToString();
                    else
                        _fatura.OdemeAciklama = "";
                    DateTime _dteFisKayitTarihi = DateTime.Now;
                    if (DateTime.TryParse(_table.Rows[i][35].ToString(), out _dteFisKayitTarihi))
                        _fatura.dteFisKayitTarihi = _dteFisKayitTarihi;
                    else
                        _fatura.dteFisKayitTarihi = null;
                    _fatura.sDepo = _table.Rows[i][36].ToString();
                    _fatura.ETTN = _table.Rows[i][37].ToString();
                    _fatura.StokAciklama = _table.Rows[i][38].ToString();
                    _fatura.nGirisCikis = _table.Rows[i].IsNull("nGirisCikis") ? 0 : Convert.ToInt32(_table.Rows[i][39].ToString());
                    _fatura.Senaryo = _table.Rows[i][40].ToString();
                    _fatura.FaturaTipi = _table.Rows[i][41].ToString();
                    listFaturaListesi.Add(_fatura);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaListesi;
        }
    }
}
