﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace db_fatura
{
    public partial class Form1 : Form
    {
        private NotifyIcon TrayIcon;
        private ContextMenuStrip TrayIconContextMenu;
        private ToolStripMenuItem CloseMenuItem;

        private void OnApplicationExit(object sender, EventArgs e)
        {
            TrayIcon.Dispose();
        }

        public Form1()
        {
            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
            InitializeComponent();

            TrayIcon = new NotifyIcon();

            TrayIcon.BalloonTipIcon = ToolTipIcon.Info;
            TrayIcon.Text = "Banco Fatura Listesi Güncelleme";

            //The icon is added to the project resources. Here I assume that the name of the file is 'TrayIcon.ico'
            TrayIcon.Icon = Properties.Resources.banco_logo;
            //Optional - handle doubleclicks on the icon:
            TrayIcon.DoubleClick += TrayIcon_DoubleClick;

            //Optional - Add a context menu to the TrayIcon:
            TrayIconContextMenu = new ContextMenuStrip();
            CloseMenuItem = new ToolStripMenuItem();
            TrayIconContextMenu.SuspendLayout();

            // 
            // TrayIconContextMenu
            // 
            this.TrayIconContextMenu.Items.AddRange(new ToolStripItem[] {
            this.CloseMenuItem});
            this.TrayIconContextMenu.Name = "TrayIconContextMenu";
            this.TrayIconContextMenu.Size = new Size(153, 70);
            // 
            // CloseMenuItem
            // 
            this.CloseMenuItem.Name = "CloseMenuItem";
            this.CloseMenuItem.Size = new Size(152, 22);
            this.CloseMenuItem.Image = Properties.Resources.close_2;
            this.CloseMenuItem.Text = "Programı Kapat";
            this.CloseMenuItem.Click += new EventHandler(this.CloseMenuItem_Click);

            TrayIconContextMenu.ResumeLayout(false);

            TrayIcon.ContextMenuStrip = TrayIconContextMenu;

            TrayIcon.Visible = true;
        }

        private void TrayIcon_DoubleClick(object sender, EventArgs e)
        {
            if (this != null)
                this.Visible = true;
        }

        bool _kapat = false;
        private void CloseMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Fatura güncelleme programını kapatmak istediğinizden emin misiniz?",
                                "Emin misiniz?", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                _kapat = true;
                Application.Exit();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            checkGuncellemeSaati.Checked = false;
            dateGuncellemeSaati.Enabled = false;
            tbFatura _fatura = tbFaturaProvider.FaturaSonIslemGetir();
            if (_fatura != null)
            {
                labelSonKayitTarihi.Text = "Son Fiş Kayıt Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() + " " + _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "Bulunamadı");
                labelSonKayitTarihi.Tag = _fatura;
            }
            else
            {
                labelSonGuncellemeTarihi.Text = "Veritabanı bağlantısı sağlanamadı.";
                btnGecikmeGuncelle.Enabled = false;
                toolOtomatikGuncelleme.Enabled = false;
                checkGuncellemeSaati.Enabled = false;
                dateGuncellemeSaati.Enabled = false;
                MessageBox.Show("Bağlantıda hata bulunmaktadır.Ağ ayarlarınızı kontrol ediniz.", "Fatura Güncelle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            if (!_kapat)
                e.Cancel = true;
        }

        private void btnGecikmeGuncelle_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            btnGecikmeGuncelle.Enabled = false;
            //labelSonGuncellemeTarihi.Text = "Fatura tablosu siliniyor.";
            //tbFaturaProvider.FaturaTablosunuSil();
            progressIslem.Visible = true;
            progressIslem.Minimum = 0;
            progressIslem.Value = 0;
            labelSonGuncellemeTarihi.Text = "Fatura listesi için veritabanına bağlanıyor ...";
            List<tbFatura> lstTBFatura = tbFaturaProvider.FaturaListesiHepsi();
            List<tbFatura> listFaturaDoldur = new List<tbFatura>();
            tbFatura _sonKayit = ((tbFatura)labelSonKayitTarihi.Tag);
            List<tbFatura> listFatura = tbFaturaProvider.FaturalariGetir(progressIslem);
            if (listFatura != null)
                listFaturaDoldur.AddRange(listFatura);
            List<tbFatura> listFatura2 = tbFaturaProvider.PFFaturalariGetir(progressIslem);
            if (listFatura2 != null)
                listFaturaDoldur.AddRange(listFatura2);
            List<tbFatura> listFatura3 = tbFaturaProvider.PFASFaturalariGetir(progressIslem);
            if (listFatura3 != null)
                listFaturaDoldur.AddRange(listFatura3);
            string[] _invoiceNumberSil = lstTBFatura.Select(o => o.InvoiceNumber).ToArray<string>();
            listFaturaDoldur.RemoveAll(i => _invoiceNumberSil.Contains(i.InvoiceNumber));
            labelSonGuncellemeTarihi.Text = "Fatura listesi getirildi.";
            Application.DoEvents();
            if (listFaturaDoldur != null)
                FaturaRaporunuGuncelle(listFaturaDoldur);
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Fatura listesi getirilemedi.", "Fatura Güncelleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            labelSonGuncellemeTarihi.Text = "Fatura detay listesi için veritabanına bağlanıyor ...";
            List<tbFaturaDetay> lstTBFaturaDetay = tbFaturaDetayProvider.FaturaDetaylariHepsi();
            List<tbFaturaDetay> listFaturaDetayDoldur = new List<tbFaturaDetay>();
            List<tbFaturaDetay> listFaturaDetay = tbFaturaDetayProvider.PFFaturaDetayGetir();
            if (listFaturaDetay != null)
                listFaturaDetayDoldur.AddRange(listFaturaDetay);
            List<tbFaturaDetay> listFaturaDetay2 = tbFaturaDetayProvider.PKFaturaDetayGetir();
            if (listFaturaDetay2 != null)
                listFaturaDetayDoldur.AddRange(listFaturaDetay2);
            string[] _invoiceNumberDetaySil = lstTBFaturaDetay.Select(o => o.InvoiceNumber).ToArray<string>();
            listFaturaDetayDoldur.RemoveAll(i => _invoiceNumberDetaySil.Contains(i.InvoiceNumber));
            labelSonGuncellemeTarihi.Text = "Fatura detay listesi getirildi.";
            Application.DoEvents();
            if (listFaturaDetayDoldur != null)
                FaturaDetayRaporunuGuncelle(listFaturaDetayDoldur);
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Fatura detay listesi getirilemedi.", "Fatura Güncelleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            tbFatura _fatura = tbFaturaProvider.FaturaSonIslemGetir();
            if (_fatura != null)
            {
                labelSonKayitTarihi.Text = "Son Fiş Kayıt Tarihi : " + (_fatura.dteFisKayitTarihi.HasValue ? _fatura.dteFisKayitTarihi.Value.ToShortDateString() + " " + _fatura.dteFisKayitTarihi.Value.ToShortTimeString() : "Bulunamadı");
                labelSonKayitTarihi.Tag = _fatura;
            }
            else
            {
                labelSonGuncellemeTarihi.Text = "Veritabanı bağlantısı sağlanamadı.";
                btnGecikmeGuncelle.Enabled = false;
                toolOtomatikGuncelleme.Enabled = false;
                checkGuncellemeSaati.Enabled = false;
                dateGuncellemeSaati.Enabled = false;
                MessageBox.Show("Bağlantıda hata bulunmaktadır.Ağ ayarlarınızı kontrol ediniz.", "Fatura Güncelle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            btnGecikmeGuncelle.Enabled = true;
        }

        private void FaturaRaporunuGuncelle(List<tbFatura> listFatura)
        {
            progressIslem.Maximum = 0;
            progressIslem.Minimum = 0;
            progressIslem.Value = 0;

            bool _hata = false;
            labelSonGuncellemeTarihi.Text = "Fatura tablosu güncelleme işlemi başlatıldı.";
            progressIslem.Minimum = 0;
            progressIslem.Maximum = 0;
            progressIslem.Value = 0;
            progressIslem.Refresh();
            Application.DoEvents();
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            try
            { con.Open(); }
            catch { }
            labelSonGuncellemeTarihi.Text = "Fatura tablosu güncelleme işlemi yapılıyor ...";
            int _toplamFatura = listFatura.Count<tbFatura>();
            progressIslem.Maximum = _toplamFatura;
            Application.DoEvents();
            for (int i = 0; i < listFatura.Count; i++)
            {
                if (!tbFaturaProvider.FaturaGuncelle(listFatura[i], con))
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Fatura güncelleme sırasında hata ile karşılaşıldı.Bağlantı ayarlarını kontrol ediniz.", "Güncelleme Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _hata = true;
                    break;
                }
                progressIslem.Value++;
                labelSonGuncellemeTarihi.Text = "Fatura tablosu güncellemesi yapılıyor ( " + progressIslem.Value.ToString("N0") + " / " + _toplamFatura.ToString("N0") + " ) ...";
                progressIslem.Refresh();
                Application.DoEvents();
            }
            try
            { con.Close(); }
            catch { }
            if (!_hata)
            {
                this.Cursor = Cursors.Default;
                progressIslem.Visible = false;
                labelSonGuncellemeTarihi.Text = "Fatura listesi güncellemesi başarıyla gerçekleştirilmiştir.";
            }
            this.Cursor = Cursors.Default;
        }

        private void FaturaDetayRaporunuGuncelle(List<tbFaturaDetay> listFaturaDetay)
        {
            progressIslem.Maximum = 0;
            progressIslem.Minimum = 0;
            progressIslem.Value = 0;

            bool _hata = false;
            labelSonGuncellemeTarihi.Text = "Fatura detay tablosu güncelleme işlemi başlatıldı.";
            progressIslem.Minimum = 0;
            progressIslem.Maximum = 0;
            progressIslem.Value = 0;
            progressIslem.Refresh();
            Application.DoEvents();
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            try
            { con.Open(); }
            catch { }
            labelSonGuncellemeTarihi.Text = "Fatura detay tablosu güncelleme işlemi yapılıyor ...";
            int _toplamFaturaDetay = listFaturaDetay.Count<tbFaturaDetay>();
            progressIslem.Maximum = _toplamFaturaDetay;
            Application.DoEvents();
            for (int i = 0; i < listFaturaDetay.Count; i++)
            {
                if (!tbFaturaDetayProvider.FaturaDetayGuncelle(listFaturaDetay[i], con))
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Fatura detay güncelleme sırasında hata ile karşılaşıldı.Bağlantı ayarlarını kontrol ediniz.", "Güncelleme Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _hata = true;
                    break;
                }
                progressIslem.Value++;
                labelSonGuncellemeTarihi.Text = "Fatura detay tablosu güncellemesi yapılıyor ( " + progressIslem.Value.ToString("N0") + " / " + _toplamFaturaDetay.ToString("N0") + " ) ...";
                progressIslem.Refresh();
                Application.DoEvents();
            }
            try
            { con.Close(); }
            catch { }
            if (!_hata)
            {
                this.Cursor = Cursors.Default;
                progressIslem.Visible = false;
                labelSonGuncellemeTarihi.Text = "Fatura detay listesi güncellemesi başarıyla gerçekleştirilmiştir.";
                MessageBox.Show("Fatura tablo güncellemesi başarıyla tamamlanmıştır.", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Cursor = Cursors.Default;
        }

        private void toolKapat_Click(object sender, EventArgs e)
        {
            CloseMenuItem_Click(null, null);
        }

        private void checkGuncellemeSaati_CheckedChanged(object sender, EventArgs e)
        {
            if (checkGuncellemeSaati.Checked)
            {
                toolOtomatikGuncelleme.Text = " Otomatik Güncellemeyi Başlat ( F5 )";
                toolOtomatikGuncelleme.Image = Properties.Resources.Ok;
                dateGuncellemeSaati.Enabled = true;
            }
            else
            {
                toolOtomatikGuncelleme.Text = " Otomatik Güncellemeyi Durdur ( F5 )";
                toolOtomatikGuncelleme.Image = Properties.Resources.close_2;
                dateGuncellemeSaati.Enabled = false;
            }
        }
    }
}
