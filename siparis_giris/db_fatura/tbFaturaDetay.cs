﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace db_fatura
{
    public class tbFaturaDetay
    {
        private string _invoiceNumber;
        public string InvoiceNumber { get { return _invoiceNumber; } set { _invoiceNumber = value; } }

        private string _sKodu;
        public string sKodu { get { return _sKodu; } set { _sKodu = value; } }

        public string _sAciklama;
        public string sAciklama { get { return _sAciklama; } set { _sAciklama = value; } }

        public decimal _lCikisMiktar1;
        public decimal lCikisMiktar1 { get { return _lCikisMiktar1; } set { _lCikisMiktar1 = value; } }

        public decimal _lCikisFiyat;
        public decimal lCikisFiyat { get { return _lCikisFiyat; } set { _lCikisFiyat = value; } }

        public decimal _lCikisTutar;
        public decimal lCikisTutar { get { return _lCikisTutar; } set { _lCikisTutar = value; } }

        public decimal _nKdvOrani;
        public decimal nKdvOrani { get { return _nKdvOrani; } set { _nKdvOrani = value; } }

        public decimal _lGirisMiktar1;
        public decimal lGirisMiktar1 { get { return _lGirisMiktar1; } set { _lGirisMiktar1 = value; } }

        public decimal _lGirisFiyat;
        public decimal lGirisFiyat { get { return _lGirisFiyat; } set { _lGirisFiyat = value; } }

        public decimal _lGirisTutar;
        public decimal lGirisTutar { get { return _lGirisTutar; } set { _lGirisTutar = value; } }

        public string _nStokFisiID;
        public string nStokFisiID { get { return _nStokFisiID; } set { _nStokFisiID = value; } }

        public string _sDepo;
        public string sDepo { get { return _sDepo; } set { _sDepo = value; } }
    }

    public class tbFaturaDetayProvider
    {
        public static List<tbFaturaDetay> FaturaDetaylariHepsi()
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from tbEFaturaDetay ";
            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 1800;
            DataTable _table = new DataTable("dsTablo");
            tbFaturaDetay _faturaDetay = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFaturaDetay> listFaturaDetay = new List<tbFaturaDetay>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _faturaDetay = new tbFaturaDetay();
                    _faturaDetay.InvoiceNumber = _table.Rows[i][0].ToString();
                    _faturaDetay.sKodu = _table.Rows[i][1].ToString();
                    _faturaDetay.sAciklama = _table.Rows[i][2].ToString();

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][3].ToString(), out _miktar))
                        _faturaDetay.lCikisMiktar1 = _miktar;
                    else
                        _faturaDetay.lCikisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][4].ToString(), out _miktar))
                        _faturaDetay.lCikisFiyat = _miktar;
                    else
                        _faturaDetay.lCikisFiyat = 0;
                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _faturaDetay.lCikisTutar = _miktar;
                    else
                        _faturaDetay.lCikisTutar = 0;
                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _faturaDetay.nKdvOrani = _miktar;
                    else
                        _faturaDetay.nKdvOrani = 0;
                    if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                        _faturaDetay.lGirisMiktar1 = _miktar;
                    else
                        _faturaDetay.lGirisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                        _faturaDetay.lGirisFiyat = _miktar;
                    else
                        _faturaDetay.lGirisFiyat = 0;
                    if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                        _faturaDetay.lGirisTutar = _miktar;
                    else
                        _faturaDetay.lGirisTutar = 0;
                    _faturaDetay.nStokFisiID = _table.Rows[i][10].ToString();
                    _faturaDetay.sDepo = _table.Rows[i][11].ToString();
                    listFaturaDetay.Add(_faturaDetay);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaDetay;
        }

        public static List<tbFaturaDetay> PFFaturaDetayGetir()
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from PFFaturaDetay ";
            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 1800;
            DataTable _table = new DataTable("dsTablo");
            tbFaturaDetay _faturaDetay = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFaturaDetay> listFaturaDetay = new List<tbFaturaDetay>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _faturaDetay = new tbFaturaDetay();
                    _faturaDetay.InvoiceNumber = _table.Rows[i][0].ToString();
                    _faturaDetay.sKodu = _table.Rows[i][1].ToString();
                    _faturaDetay.sAciklama = _table.Rows[i][2].ToString();

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][3].ToString(), out _miktar))
                        _faturaDetay.lCikisMiktar1 = _miktar;
                    else
                        _faturaDetay.lCikisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][4].ToString(), out _miktar))
                        _faturaDetay.lCikisFiyat = _miktar;
                    else
                        _faturaDetay.lCikisFiyat = 0;
                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _faturaDetay.lCikisTutar = _miktar;
                    else
                        _faturaDetay.lCikisTutar = 0;
                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _faturaDetay.nKdvOrani = _miktar;
                    else
                        _faturaDetay.nKdvOrani = 0;
                    if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                        _faturaDetay.lGirisMiktar1 = _miktar;
                    else
                        _faturaDetay.lGirisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                        _faturaDetay.lGirisFiyat = _miktar;
                    else
                        _faturaDetay.lGirisFiyat = 0;
                    if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                        _faturaDetay.lGirisTutar = _miktar;
                    else
                        _faturaDetay.lGirisTutar = 0;
                    _faturaDetay.nStokFisiID = _table.Rows[i][10].ToString();
                    _faturaDetay.sDepo = _table.Rows[i][11].ToString();
                    listFaturaDetay.Add(_faturaDetay);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaDetay;
        }

        public static List<tbFaturaDetay> PKFaturaDetayGetir()
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from PKFaturaDetay ";
            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 1800;
            DataTable _table = new DataTable("dsTablo");
            tbFaturaDetay _faturaDetay = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFaturaDetay> listFaturaDetay = new List<tbFaturaDetay>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _faturaDetay = new tbFaturaDetay();
                    _faturaDetay.InvoiceNumber = _table.Rows[i][0].ToString();
                    _faturaDetay.sKodu = _table.Rows[i][1].ToString();
                    _faturaDetay.sAciklama = _table.Rows[i][2].ToString();

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][3].ToString(), out _miktar))
                        _faturaDetay.lCikisMiktar1 = _miktar;
                    else
                        _faturaDetay.lCikisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][4].ToString(), out _miktar))
                        _faturaDetay.lCikisFiyat = _miktar;
                    else
                        _faturaDetay.lCikisFiyat = 0;
                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _faturaDetay.lCikisTutar = _miktar;
                    else
                        _faturaDetay.lCikisTutar = 0;
                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _faturaDetay.nKdvOrani = _miktar;
                    else
                        _faturaDetay.nKdvOrani = 0;
                    if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                        _faturaDetay.lGirisMiktar1 = _miktar;
                    else
                        _faturaDetay.lGirisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                        _faturaDetay.lGirisFiyat = _miktar;
                    else
                        _faturaDetay.lGirisFiyat = 0;
                    if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                        _faturaDetay.lGirisTutar = _miktar;
                    else
                        _faturaDetay.lGirisTutar = 0;
                    _faturaDetay.nStokFisiID = "0";
                    _faturaDetay.sDepo = _table.Rows[i][10].ToString();
                    listFaturaDetay.Add(_faturaDetay);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaDetay;
        }

        public static bool FaturaDetayGuncelle(tbFaturaDetay _faturaGuncelle, SqlConnection con)
        {
            string _query = "insert into tbEFaturaDetay (InvoiceNumber,sKodu,sAciklama,lCikisMiktar1,lCikisFiyat,lCikisTutar,nKdvOrani,lGirisMiktar1,lGirisFiyat,lGirisTutar,nStokFisiID,sDepo,kayitTarihi) values ('";
            _query += _faturaGuncelle.InvoiceNumber; // invoice number
            _query += "','" + _faturaGuncelle.sKodu; // skodu
            _query += "','" + _faturaGuncelle.sAciklama; // saciklama
            _query += "'," + _faturaGuncelle.lCikisMiktar1.ToString().Replace(',', '.'); // lgirismiktar1
            _query += "," + _faturaGuncelle.lCikisFiyat.ToString().Replace(',', '.'); // lcikisfiyat
            _query += "," + _faturaGuncelle.lCikisTutar.ToString().Replace(',', '.'); // lcikistutar
            _query += "," + _faturaGuncelle.nKdvOrani.ToString().Replace(',', '.'); // nkdvorani
            _query += "," + _faturaGuncelle.lGirisMiktar1.ToString().Replace(',', '.'); // lgirismiktar1
            _query += "," + _faturaGuncelle.lGirisFiyat.ToString().Replace(',', '.'); // lgirisfiyat
            _query += "," + _faturaGuncelle.lGirisTutar.ToString().Replace(',', '.'); // lgiristutar
            _query += ",'" + _faturaGuncelle.nStokFisiID.ToString().Replace(',', '.'); // nstokfisiID
            _query += "','" + _faturaGuncelle.sDepo.ToString().Replace(',', '.'); // sdepo
            _query += "',getdate())";

            bool _deger = false;
            SqlCommand cmd = new SqlCommand(_query, con);
            cmd.CommandTimeout = 300;
            try
            {
                cmd.ExecuteNonQuery();
                _deger = true;
            }
            catch
            {
                return false;
            }

            return _deger;
        }
    }
}
