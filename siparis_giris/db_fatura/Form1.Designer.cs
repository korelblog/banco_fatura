﻿namespace db_fatura
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.progressIslem = new System.Windows.Forms.ProgressBar();
            this.labelSonGuncellemeTarihi = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkGuncellemeSaati = new System.Windows.Forms.CheckBox();
            this.btnGecikmeGuncelle = new System.Windows.Forms.Button();
            this.dateGuncellemeSaati = new System.Windows.Forms.DateTimePicker();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolKapat = new System.Windows.Forms.ToolStripButton();
            this.toolOtomatikGuncelleme = new System.Windows.Forms.ToolStripButton();
            this.timerGuncelleme = new System.Windows.Forms.Timer(this.components);
            this.labelSonKayitTarihi = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressIslem
            // 
            this.progressIslem.Location = new System.Drawing.Point(12, 89);
            this.progressIslem.Name = "progressIslem";
            this.progressIslem.Size = new System.Drawing.Size(734, 23);
            this.progressIslem.TabIndex = 1;
            this.progressIslem.Visible = false;
            // 
            // labelSonGuncellemeTarihi
            // 
            this.labelSonGuncellemeTarihi.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelSonGuncellemeTarihi.Location = new System.Drawing.Point(302, 11);
            this.labelSonGuncellemeTarihi.Name = "labelSonGuncellemeTarihi";
            this.labelSonGuncellemeTarihi.Size = new System.Drawing.Size(444, 23);
            this.labelSonGuncellemeTarihi.TabIndex = 2;
            this.labelSonGuncellemeTarihi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelSonKayitTarihi);
            this.panel1.Controls.Add(this.checkGuncellemeSaati);
            this.panel1.Controls.Add(this.btnGecikmeGuncelle);
            this.panel1.Controls.Add(this.dateGuncellemeSaati);
            this.panel1.Controls.Add(this.labelSonGuncellemeTarihi);
            this.panel1.Controls.Add(this.progressIslem);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(758, 125);
            this.panel1.TabIndex = 3;
            // 
            // checkGuncellemeSaati
            // 
            this.checkGuncellemeSaati.AutoSize = true;
            this.checkGuncellemeSaati.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.checkGuncellemeSaati.Location = new System.Drawing.Point(503, 62);
            this.checkGuncellemeSaati.Name = "checkGuncellemeSaati";
            this.checkGuncellemeSaati.Size = new System.Drawing.Size(150, 18);
            this.checkGuncellemeSaati.TabIndex = 18;
            this.checkGuncellemeSaati.Text = "Güncelleme Saati :";
            this.checkGuncellemeSaati.UseVisualStyleBackColor = true;
            this.checkGuncellemeSaati.CheckedChanged += new System.EventHandler(this.checkGuncellemeSaati_CheckedChanged);
            // 
            // btnGecikmeGuncelle
            // 
            this.btnGecikmeGuncelle.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGecikmeGuncelle.Image = global::db_fatura.Properties.Resources.save;
            this.btnGecikmeGuncelle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGecikmeGuncelle.Location = new System.Drawing.Point(12, 11);
            this.btnGecikmeGuncelle.Name = "btnGecikmeGuncelle";
            this.btnGecikmeGuncelle.Size = new System.Drawing.Size(284, 69);
            this.btnGecikmeGuncelle.TabIndex = 0;
            this.btnGecikmeGuncelle.Text = "Fatura Raporunu Güncelle";
            this.btnGecikmeGuncelle.UseVisualStyleBackColor = true;
            this.btnGecikmeGuncelle.Click += new System.EventHandler(this.btnGecikmeGuncelle_Click);
            // 
            // dateGuncellemeSaati
            // 
            this.dateGuncellemeSaati.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateGuncellemeSaati.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateGuncellemeSaati.Location = new System.Drawing.Point(659, 60);
            this.dateGuncellemeSaati.Name = "dateGuncellemeSaati";
            this.dateGuncellemeSaati.Size = new System.Drawing.Size(87, 23);
            this.dateGuncellemeSaati.TabIndex = 17;
            this.dateGuncellemeSaati.Value = new System.DateTime(1900, 1, 1, 18, 12, 0, 0);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolKapat,
            this.toolOtomatikGuncelleme});
            this.toolStrip2.Location = new System.Drawing.Point(0, 148);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(782, 25);
            this.toolStrip2.TabIndex = 4;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolKapat
            // 
            this.toolKapat.Image = global::db_fatura.Properties.Resources.close_2;
            this.toolKapat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolKapat.Name = "toolKapat";
            this.toolKapat.Size = new System.Drawing.Size(70, 22);
            this.toolKapat.Text = " Kapat";
            this.toolKapat.Click += new System.EventHandler(this.toolKapat_Click);
            // 
            // toolOtomatikGuncelleme
            // 
            this.toolOtomatikGuncelleme.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolOtomatikGuncelleme.Image = global::db_fatura.Properties.Resources.close_2;
            this.toolOtomatikGuncelleme.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolOtomatikGuncelleme.Name = "toolOtomatikGuncelleme";
            this.toolOtomatikGuncelleme.Size = new System.Drawing.Size(249, 22);
            this.toolOtomatikGuncelleme.Text = "  Otomatik Güncellemeyi Bitir ( F5 )";
            // 
            // labelSonKayitTarihi
            // 
            this.labelSonKayitTarihi.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelSonKayitTarihi.Location = new System.Drawing.Point(302, 34);
            this.labelSonKayitTarihi.Name = "labelSonKayitTarihi";
            this.labelSonKayitTarihi.Size = new System.Drawing.Size(444, 23);
            this.labelSonKayitTarihi.TabIndex = 19;
            this.labelSonKayitTarihi.Text = "Son Fiş Kayıt Tarihi : 22.04.2016 22:10";
            this.labelSonKayitTarihi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 173);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "  Fatura Raporunu Güncelle";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGecikmeGuncelle;
        private System.Windows.Forms.ProgressBar progressIslem;
        private System.Windows.Forms.Label labelSonGuncellemeTarihi;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolKapat;
        private System.Windows.Forms.ToolStripButton toolOtomatikGuncelleme;
        private System.Windows.Forms.DateTimePicker dateGuncellemeSaati;
        private System.Windows.Forms.CheckBox checkGuncellemeSaati;
        private System.Windows.Forms.Timer timerGuncelleme;
        private System.Windows.Forms.Label labelSonKayitTarihi;
    }
}

