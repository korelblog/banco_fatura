﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Fatura Tablo Güncelleme")]
[assembly: AssemblyDescription("Fatura Tablo Güncelleme")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Korel Yazılım")]
[assembly: AssemblyProduct("Fatura Tablo Güncelleme")]
[assembly: AssemblyCopyright("Copyright © Korel Yazılım 2016")]
[assembly: AssemblyTrademark("Korel Yazılım http://www.korelblog.com")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("aef370b2-8242-4550-9c5c-2f988a49e7df")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.1.17")]
[assembly: AssemblyFileVersion("1.0.1.17")]
