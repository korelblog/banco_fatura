﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DataSiparis
{
    public class tbEFaturaXML
    {
        private string _identifier;
        public string Identifier { get { return _identifier; } set { _identifier = value; } }

        private string _title;
        public string Title { get { return _title; } set { _title = value; } }

        private string _tarih;
        public string Tarih { get { return _tarih; } set { _tarih = value; } }
    }

    public class tbEFaturaXMLProvider
    {
        public static List<tbEFaturaXML> FaturaXMLListesiniGetir(string _kod)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _query = "select * from tbEFaturaXML where identifier like '%" + _kod + "%' or title like '%" + _kod + "%'";
            SqlCommand cmd = new SqlCommand(_query, con);
            DataTable _table = new DataTable("dsTablo");
            _table.Columns.Add("identifier", typeof(string));
            _table.Columns.Add("title", typeof(string));
            cmd.CommandTimeout = 300;
            tbEFaturaXML _fatura = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbEFaturaXML> listFatura = new List<tbEFaturaXML>();
            try
            {
                con.Open();
                da.Fill(_table);
                DateTime _tarih = DateTime.Now;
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _fatura = new tbEFaturaXML();
                    _fatura.Identifier = _table.Rows[i][0].ToString();
                    _fatura.Title = _table.Rows[i][1].ToString().Trim();
                    listFatura.Add(_fatura);
                }
            }
            catch
            {
                return null;
            }
            finally { con.Close(); }
            if (listFatura.Count > 0)
                return listFatura;
            else
                return null;
        }
    }
}
