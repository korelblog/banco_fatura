﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Forms;
using System.Web.UI;
using System.Data;

namespace DataSiparis
{
    public class clsGenel
    {
        public static string _cnstring = "Data Source=SERVER;Initial Catalog=CELIK;Uid=sa;Pwd=00560056";

        //public static string _cnstring = "Data Source=RDPSERVER,1453;Initial Catalog=CELIKs;Uid=sa;Password=00560056";
        //public static string _cnstring = "Data Source=SERVER,1453;Initial Catalog=CELIK;Uid=sa;Password=00560056";
        //public static string _cnstring = "Data Source=195.244.36.147,1453;Initial Catalog=CELIKs;Uid=sa;Password=00560056";
        //public static string _cnstring = "Data Source=195.244.36.166,5850;Initial Catalog=CELIK;Uid=sa;Password=00560056";
        public static string _cnCheckLK = "Data Source=94.73.170.20;Initial Catalog=db2F3CD914A0;Uid=user2F3CD914A0;Pwd=HFyy96R3";

        public static string Turkcelestir(string _deger)
        {
            return _deger.Replace('Ş', 'S').Replace('İ', 'I').Replace('Ç', 'C').Replace('Ü', 'U').Replace('Ğ', 'G').Replace('Ö', 'O');
        }

        public static string MSTurkce(string windowsTurkce)
        {
            return Encoding.Default.GetString(Encoding.GetEncoding(1252).GetBytes(windowsTurkce));
        }

        public static void Export2Excel(ListView list, string _tip)
        {
            SaveFileDialog _dialog;
            try
            {
                _dialog = new SaveFileDialog();
                switch (_tip)
                {
                    case "xls":
                        _dialog.Filter = "Excel Dosyası | *.xls";
                        break;
                    case "doc":
                        _dialog.Filter = "Word Dosyası | *.doc";
                        break;
                }
                string _path = "";
                if (_dialog.ShowDialog() == DialogResult.OK)
                    _path = _dialog.FileName;
                else
                {
                    switch (_tip)
                    {
                        case "xls":
                            MessageBox.Show("İşlem iptal edildi.", "Excele Aktar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        case "doc":
                            MessageBox.Show("İşlem iptal edildi.", "Worde Aktar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                    return;
                }
                DataTable _table = new DataTable(_dialog.FileName);
                DataColumn c;
                for (int i = 0; i < list.Columns.Count; i++)
                {
                    c = new DataColumn(MSTurkce(list.Columns[i].Text));
                    _table.Columns.Add(c);
                }
                DataRow r;
                for (int i = 0; i < list.Items.Count; i++)
                {
                    r = _table.NewRow();
                    for (int j = 0; j < list.Columns.Count; j++)
                        r[MSTurkce(list.Columns[j].Text)] = list.Items[i].SubItems[j].Text;
                    _table.Rows.Add(r);
                }
                System.Web.UI.WebControls.DataGrid grid = new System.Web.UI.WebControls.DataGrid();

                grid.HeaderStyle.Font.Bold = true;
                grid.DataSource = _table;
                grid.DataMember = _table.TableName;

                grid.DataBind();

                // render the DataGrid control to a file
                using (StreamWriter sw = new StreamWriter(_dialog.FileName, false, Encoding.UTF8))
                {

                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        grid.RenderControl(hw);
                    }
                }
                switch (_tip)
                {
                    case "xls":
                        MessageBox.Show("İşlem tamamlandı.", "Excele Aktar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    case "doc":
                        MessageBox.Show("İşlem tamamlandı.", "Worde Aktar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aktarma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void Degistir(bool _deger, object _textBOX)
        {
            TextBox _text = null;
            MaskedTextBox _maskedText = null;
            if (_textBOX.GetType() == typeof(TextBox))
                _text = (TextBox)_textBOX;
            else if (_textBOX.GetType() == typeof(MaskedTextBox))
                _maskedText = (MaskedTextBox)_textBOX;
            else
                return;
            if (_deger)
            {
                if (_text != null)
                    _text.BackColor = System.Drawing.Color.FromArgb(246, 184, 187);
                else
                    _maskedText.BackColor = System.Drawing.Color.FromArgb(246, 184, 187);
            }
            else
            {
                if (_text != null)
                    _text.BackColor = System.Drawing.Color.White;
                else
                    _maskedText.BackColor = System.Drawing.Color.White;
            }
        }

        public static void Boyama(ListView _list)
        {
            bool _boya = false;
            try
            {
                for (int i = 0; i < _list.Items.Count; i++)
                {
                    if (_list != null)
                    {
                        if (_list.Items[i].BackColor != System.Drawing.SystemColors.Window)
                            continue;
                        if (!_boya)
                        {
                            _list.Items[i].BackColor = System.Drawing.Color.FromArgb(246, 184, 187);
                            _boya = true;
                        }
                        else
                        {
                            _boya = false;
                            _list.Items[i].BackColor = System.Drawing.Color.White;
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public static bool sayi_varmi(string deger)
        {
            bool _deger = false;
            for (int i = 0; i < deger.Length; i++)
            {
                if (Char.IsNumber(deger[i]))
                {
                    _deger = true;
                    break;
                }
            }
            return _deger;
        }

        public static bool karakter_varmi(string deger)
        {
            bool _deger = false;
            for (int i = 0; i < deger.Length; i++)
            {
                if (Char.IsLetter(deger[i]))
                {
                    _deger = true;
                    break;
                }
            }
            return _deger;
        }

        public static void txtDegistir_TextChanged(object sender, EventArgs e)
        {
            bool _degisim;
            if (sender.GetType() == typeof(TextBox))
            {
                TextBox _textBox = (TextBox)sender;
                _degisim = (bool)_textBox.Tag;
            }
            else if (sender.GetType() == typeof(MaskedTextBox))
            {
                MaskedTextBox _maskedText = (MaskedTextBox)sender;
                _degisim = (bool)_maskedText.Tag;
            }
            _degisim = true;
        }

        public static void comboDegistir_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox _combo = (ComboBox)sender;
            bool _degisim = (bool)_combo.Tag;
            _degisim = true;
        }

        public static void listPersonel_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox _list = (ListBox)sender;
            bool _degisim = (bool)_list.Tag;
            _degisim = true;
        }

        public static void checkDegistir_CheckedChanged(object sender, EventArgs e)
        {
            bool _degisim;
            if (sender.GetType() == typeof(CheckBox))
            {
                CheckBox _checkBox = (CheckBox)sender;
                _degisim = (bool)_checkBox.Tag;
            }
            else if (sender.GetType() == typeof(RadioButton))
            {
                RadioButton _radioButton = (RadioButton)sender;
                _degisim = (bool)_radioButton.Tag;
            }
            _degisim = true;
        }

        public static void dateTarih_ValueChanged(object sender, EventArgs e)
        {
            DateTimePicker _dateTime = (DateTimePicker)sender;
            bool _degisim = (bool)_dateTime.Tag;
            _degisim = true;
        }

        public static void txtDegistir_Leave(object sender, EventArgs e)
        {
            clsGenel.Degistir(false, sender);
        }

        public static void txtDegistir_Enter(object sender, EventArgs e)
        {
            clsGenel.Degistir(true, sender);
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "Otr%15";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = null;
            try
            {
                cipherBytes = Convert.FromBase64String(cipherText);
            }
            catch
            {
                return "";
            }
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x54, 0x5d, 0x12, 0x66, 0x34, 0x56, 0x67 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }

    public enum FormDurum
    {
        Ekle, Duzenle, Sil, Goster
    }
}
