﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace DataSiparis
{
    public class ListViewItemComparer : IComparer
    {
        private int col;
        private SortOrder order;
        public ListViewItemComparer()
        {
            col = 0;
            order = SortOrder.Ascending;
        }
        public ListViewItemComparer(int column, SortOrder order)
        {
            col = column;
            this.order = order;
        }
        public int Compare(object x, object y)
        {
            int returnVal = -1;

            decimal _sayi;
            try
            {
                Convert.ToDateTime(((ListViewItem)x).SubItems[col].Text.Substring(0, 10) + " " + ((ListViewItem)x).SubItems[col].Text.Substring(13, 5));
                returnVal = DateTime.Compare(Convert.ToDateTime(((ListViewItem)x).SubItems[col].Text.Substring(0, 10) + " " + ((ListViewItem)x).SubItems[col].Text.Substring(13, 5)),
                                      Convert.ToDateTime(((ListViewItem)y).SubItems[col].Text.Substring(0, 10) + " " + ((ListViewItem)y).SubItems[col].Text.Substring(13, 5)));
            }
            catch
            {
                if (Decimal.TryParse(((ListViewItem)x).SubItems[col].Text, out _sayi))
                {
                    if (Decimal.TryParse(((ListViewItem)y).SubItems[col].Text, out _sayi))
                        returnVal = Decimal.Compare(Convert.ToDecimal(((ListViewItem)x).SubItems[col].Text), Convert.ToDecimal(((ListViewItem)y).SubItems[col].Text));
                    else
                        returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                                              ((ListViewItem)y).SubItems[col].Text);
                }
                else
                    returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                                            ((ListViewItem)y).SubItems[col].Text);
            }
            if (order == SortOrder.Descending)
                returnVal *= -1;
            return returnVal;
        }

        private static object obj = new object();
        public static void AutoResizeByContentAndHeader(ListView list)
        {
            for (int i = 0; i < list.Columns.Count; i++)
            {
                list.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                int contentAutoWidth = list.Columns[i].Width;
                list.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                if (contentAutoWidth > list.Columns[i].Width)
                {
                    list.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                }
            }
        }
    }
}
