﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DataSiparis
{
    public class tbFaturaDepo
    {
        private string _webSiteuri;
        public string WebSiteURI { get { return _webSiteuri; } set { _webSiteuri = value; } }

        private string _vkn;
        public string VKN { get { return _vkn; } set { _vkn = value; } }

        private string _name;
        public string Name { get { return _name; } set { _name = value; } }

        private string _streetName;
        public string StreetName { get { return _streetName; } set { _streetName = value; } }

        private string _buildingNumber;
        public string BuildingNumber { get { return _buildingNumber; } set { _buildingNumber = value; } }

        private string _citySubdivisionName;
        public string CitySubDivisionName { get { return _citySubdivisionName; } set { _citySubdivisionName = value; } }

        private string _cityName;
        public string CityName { get { return _cityName; } set { _cityName = value; } }

        private string _postalZone;
        public string PostalZone { get { return _postalZone; } set { _postalZone = value; } }

        private string _country;
        public string Country { get { return _country; } set { _country = value; } }

        private string _taxScheme;
        public string TaxScheme { get { return _taxScheme; } set { _taxScheme = value; } }

        private string _telephone;
        public string Telephone { get { return _telephone; } set { _telephone = value; } }

        private string _teleFax;
        public string Telefax { get { return _teleFax; } set { _teleFax = value; } }

        private string _electronicMail;
        public string ElectronicMail { get { return _electronicMail; } set { _electronicMail = value; } }

        private string _mersisNo;
        public string MersisNo { get { return _mersisNo; } set { _mersisNo = value; } }

        private string _firmNo;
        public string FirmNo { get { return _firmNo; } set { _firmNo = value; } }

        private string _periodNo;
        public string PeriodNo { get { return _periodNo; } set { _periodNo = value; } }

        private string _depoMagazaKodu;
        public string DepoMagazaKodu { get { return _depoMagazaKodu; } set { _depoMagazaKodu = value; } }

        private string _depo;
        public string Depo { get { return _depo; } set { _depo = value; } }

        private string _ticaretSicil;
        public string TicaretSicil { get { return _ticaretSicil; } set { _ticaretSicil = value; } }

        private string _versiyon;
        public string Versiyon { get { return _versiyon; } set { _versiyon = value; } }
    }

    public class tbFaturaDepoProvider
    {
        public static tbFaturaDepo FaturaDepolariGetir(string _sDepo)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from Tbefaturadepo where DepoMagazaKodu = '" + _sDepo + "'";
            SqlCommand cmd = new SqlCommand(_command, con);
            DataTable _table = new DataTable("dsTablo");
            _table.Columns.Add("WebSiteURI", typeof(string));
            _table.Columns.Add("VKN", typeof(string));
            _table.Columns.Add("Name", typeof(string));
            _table.Columns.Add("StreetName", typeof(string));
            _table.Columns.Add("BuildingNumber", typeof(string));
            _table.Columns.Add("CitySubdivisionName", typeof(string));
            _table.Columns.Add("CityName", typeof(string));
            _table.Columns.Add("PostalZone", typeof(string));
            _table.Columns.Add("Country", typeof(string));
            _table.Columns.Add("TaxScheme", typeof(string));
            _table.Columns.Add("Telephone", typeof(string));
            _table.Columns.Add("Telefax", typeof(string));
            _table.Columns.Add("ElectronicMail", typeof(string));
            _table.Columns.Add("MersisNo", typeof(string));
            _table.Columns.Add("FirmNo", typeof(string));
            _table.Columns.Add("PeridNo", typeof(string));
            _table.Columns.Add("DepoMagazaKodu", typeof(string));
            _table.Columns.Add("Depo", typeof(string));
            _table.Columns.Add("TicaretSicil", typeof(string));
            _table.Columns.Add("Versiyon", typeof(string));
            cmd.CommandTimeout = 300;
            tbFaturaDepo _faturaDepo = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFaturaDepo> listFaturaDepo = new List<tbFaturaDepo>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _faturaDepo = new tbFaturaDepo();
                    _faturaDepo.WebSiteURI = _table.Rows[i][0].ToString();
                    _faturaDepo.VKN = _table.Rows[i][1].ToString();
                    _faturaDepo.Name = _table.Rows[i][2].ToString();
                    _faturaDepo.StreetName = _table.Rows[i][3].ToString();
                    _faturaDepo.BuildingNumber = _table.Rows[i][4].ToString();
                    _faturaDepo.CitySubDivisionName = _table.Rows[i][5].ToString();
                    _faturaDepo.CityName = _table.Rows[i][6].ToString();
                    _faturaDepo.PostalZone = _table.Rows[i][7].ToString();
                    _faturaDepo.Country = _table.Rows[i][8].ToString();
                    _faturaDepo.TaxScheme = _table.Rows[i][9].ToString();
                    _faturaDepo.Telephone = _table.Rows[i][10].ToString();
                    _faturaDepo.Telefax = _table.Rows[i][11].ToString();
                    _faturaDepo.ElectronicMail = _table.Rows[i][12].ToString();
                    _faturaDepo.MersisNo = _table.Rows[i][13].ToString();
                    _faturaDepo.FirmNo = _table.Rows[i][14].ToString();
                    _faturaDepo.PeriodNo = _table.Rows[i][15].ToString();
                    _faturaDepo.DepoMagazaKodu = _table.Rows[i][16].ToString();
                    _faturaDepo.Depo = _table.Rows[i][17].ToString();
                    _faturaDepo.TicaretSicil = _table.Rows[i][18].ToString();
                    _faturaDepo.Versiyon = _table.Rows[i][19].ToString();
                    listFaturaDepo.Add(_faturaDepo);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            if (listFaturaDepo.Count > 0)
                return listFaturaDepo[0];
            else
                return null;
        }
    }
}
