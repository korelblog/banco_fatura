﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DataSiparis
{
    public class tbFisTipi
    {
        private string _sFisTipi;
        public string sFisTipi { get { return _sFisTipi; } set { _sFisTipi = value; } }
    }

    public class FisTipiProvider
    {
        public static List<tbFisTipi> FisTipiListesi(string _sAciklama)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select distinct sFisTipi from tbEFatura f where ";
            if (_sAciklama != null)
                _command += " f.sFisTipi like '%" + _sAciklama + "%'";

            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 300;
            DataTable _table = new DataTable("dsTablo");
            tbFisTipi _fisTipi = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFisTipi> listFisTipi = new List<tbFisTipi>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _fisTipi = new tbFisTipi();
                    _fisTipi.sFisTipi = _table.Rows[i][0].ToString();
                    listFisTipi.Add(_fisTipi);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFisTipi;
        }
    }
}
