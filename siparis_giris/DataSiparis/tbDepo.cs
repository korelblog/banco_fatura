﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DataSiparis
{
    public class tbDepo
    {
        private string _sDepo;
        public string sDepo { get { return _sDepo; } set { _sDepo = value; } }

        private string _sAciklama;
        public string sAciklama { get { return _sAciklama; } set { _sAciklama = value; } }
    }

    public class tbDepoProvider
    {
        public static List<tbDepo> DepolariniGetir(string _sAciklama)
        {
            List<tbDepo> listDepo = null;
            using (SqlConnection con = new SqlConnection(clsGenel._cnstring))
            {
                string _command = @"set dateformat dmy
select * from tbDepo where sAciklama not like '%BEKLEME%'";
                if (!string.IsNullOrEmpty(_sAciklama))
                    _command += " and sAciklama like '%" + _sAciklama + "%'";
                SqlCommand cmd = new SqlCommand(_command, con);
                cmd.CommandTimeout = 300;
                DataTable _table = new DataTable("dsTablo");
                tbDepo _depo = null;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                listDepo = new List<tbDepo>();
                try
                {
                    con.Open();
                    da.Fill(_table);
                    for (int i = 0; i < _table.Rows.Count; i++)
                    {
                        _depo = new tbDepo();
                        _depo.sDepo = _table.Rows[i][0].ToString();
                        _depo.sAciklama = _table.Rows[i][1].ToString();
                        listDepo.Add(_depo);
                    }
                }
                catch
                {
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }

            return listDepo;
        }
    }
}
