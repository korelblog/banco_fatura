﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DataSiparis
{
    public class tbMusteri
    {
        private int _no;
        public int NO { get { return _no; } set { _no = value; } }

        private int _musteriID;
        public int MusteriID { get { return _musteriID; } set { _musteriID = value; } }

        private string _musteriKod;
        public string MusteriKod { get { return _musteriKod; } set { _musteriKod = value; } }

        private string _ad;
        public string Ad { get { return _ad; } set { _ad = value; } }

        private string _soyad;
        public string Soyad { get { return _soyad; } set { _soyad = value; } }

        private string _musteriAd;
        public string MusteriAd { get { return _musteriAd; } set { _musteriAd = value; } }

        private string _gsm;
        public string GSM { get { return _gsm; } set { _gsm = value; } }

        private string _evTelefon;
        public string EvTelefon { get { return _evTelefon; } set { _evTelefon = value; } }

        private string _evAdresi1;
        public string EvAdresi1 { get { return _evAdresi1; } set { _evAdresi1 = value; } }

        private string _evAdresi2;
        public string EvAdresi2 { get { return _evAdresi2; } set { _evAdresi2 = value; } }

        private string _evSehir;
        public string EvSehir { get { return _evSehir; } set { _evSehir = value; } }

        private string _evSemt;
        public string EvSemt { get { return _evSemt; } set { _evSemt = value; } }

        private string _kimlikResim;
        public string KimlikResim { get { return _kimlikResim; } set { _kimlikResim = value; } }
    }

    public class tbMusteriProvider
    {
        public static List<tbMusteri> MusterileriGetir(string _sKod, string _sAdSoyad, string _tcKimlik)
        {
            List<tbMusteri> listMusteri = null;
            using (SqlConnection con = new SqlConnection(clsGenel._cnstring))
            {
                StringBuilder _stringBuilder = new StringBuilder();
                _stringBuilder.Append("select m.nMusteriID,m.lKodu,n.sCuzdanKayitNo,m.sAdi,m.sSoyadi,(m.sAdi + ' ' + m.sSoyadi) as sMusteriAd,m.sGSM,m.sEvTelefonu,m.sEvAdresi1,m.sEvAdresi2,m.sEvIl,m.sEvSemt,m.sResimAdi from tbMusteri m inner join tbMusteriNufusu n on m.nMusteriID = n.nMusteriID");
                bool _where = false;
                if (!string.IsNullOrEmpty(_sKod))
                {
                    if (!_where)
                    {
                        _stringBuilder.Append(" where");
                        _where = true;
                    }
                    _stringBuilder.Append(" m.lKodu = '" + _sKod + "'");
                }
                if (!string.IsNullOrEmpty(_sAdSoyad))
                {
                    if (!_where)
                        _stringBuilder.Append(" where");
                    else
                        _stringBuilder.Append(" and");
                    _stringBuilder.Append(" (m.sAdi + ' ' + m.sSoyadi) like '%" + _sAdSoyad + "%'");
                }
                if (!string.IsNullOrEmpty(_tcKimlik))
                {
                    if (!_where)
                        _stringBuilder.Append(" where");
                    else
                        _stringBuilder.Append(" and");
                    _stringBuilder.Append(" n.sCuzdanKayitNo like '%" + _tcKimlik + "%'");
                }
                SqlCommand cmd = new SqlCommand(_stringBuilder.ToString(), con);
                DataTable _table = new DataTable("dsTablo");
                _table.Columns.Add("nMusteriID", typeof(int));
                _table.Columns.Add("lKodu", typeof(string));
                _table.Columns.Add("sAdi", typeof(string));
                _table.Columns.Add("sSoyadi", typeof(string));
                _table.Columns.Add("sMusteriAd", typeof(string));
                _table.Columns.Add("sGSM", typeof(string));
                _table.Columns.Add("sEvTelefonu", typeof(string));
                _table.Columns.Add("sEvAdresi1", typeof(string));
                _table.Columns.Add("sEvAdresi2", typeof(string));
                _table.Columns.Add("sEvIl", typeof(string));
                _table.Columns.Add("sEvSemt", typeof(string));
                _table.Columns.Add("sResimAdi", typeof(string));
                tbMusteri _musteri = null;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                listMusteri = new List<tbMusteri>();
                try
                {
                    con.Open();
                    da.Fill(_table);
                    string[] _strAd = null;
                    string[] _strSoyad = null;
                    for (int i = 0; i < _table.Rows.Count; i++)
                    {
                        _musteri = new tbMusteri();
                        _musteri.MusteriID = Convert.ToInt32(_table.Rows[i][0].ToString());
                        _musteri.MusteriKod = _table.Rows[i][1].ToString();
                        _strAd = _table.Rows[i][2].ToString().Split(' ');
                        if (_strAd.Length > 1)
                            _musteri.Ad = _strAd[0] + " " + _strAd[1];
                        else
                            _musteri.Ad = _strAd[0];
                        _strSoyad = _table.Rows[i][3].ToString().Split(' ');
                        if (_strSoyad.Length > 1)
                            _musteri.Soyad = _strSoyad[0] + " " + _strSoyad[1];
                        else
                            _musteri.Soyad = _strSoyad[0];
                        _musteri.MusteriAd = _musteri.Ad + " " + _musteri.Soyad;
                        _musteri.GSM = _table.Rows[i][5].ToString();
                        _musteri.EvTelefon = _table.Rows[i][6].ToString();
                        _musteri.EvAdresi1 = _table.Rows[i][7].ToString();
                        _musteri.EvAdresi2 = _table.Rows[i][8].ToString();
                        _musteri.EvSehir = _table.Rows[i][9].ToString();
                        _musteri.EvSemt = _table.Rows[i][10].ToString();
                        _musteri.KimlikResim = _table.Rows[i][11].ToString();
                        listMusteri.Add(_musteri);
                    }
                }
                catch
                {
                    return null;
                }
                finally { con.Close(); }
                if (listMusteri.Count > 0)
                    return listMusteri;
                else
                    return null;
            }
        }
    }
}
