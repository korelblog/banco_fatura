﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DataSiparis
{
    public class tbFirma
    {
        private int _nFirmaID;
        public int nFirmaID { get { return _nFirmaID; } set { _nFirmaID = value; } }

        private string _sKodu;
        public string sKodu { get { return _sKodu; } set { _sKodu = value; } }

        private string _sAciklama;
        public string sAciklama { get { return _sAciklama; } set { _sAciklama = value; } }

        private string _sAdres1;
        public string Adres1 { get { return _sAdres1; } set { _sAdres1 = value; } }

        private string _sAdres2;
        public string Adres2 { get { return _sAdres2; } set { _sAdres2 = value; } }

        private string _semt;
        public string Semt { get { return _semt; } set { _semt = value; } }

        private string _il;
        public string Il { get { return _il; } set { _il = value; } }

        private string _sOzelNot;
        public string sOzelNot { get { return _sOzelNot; } set { _sOzelNot = value; } }

        private string _sVergiNo;
        public string sVergiNo { get { return _sVergiNo; } set { _sVergiNo = value; } }
    }

    public class tbFirmaProvider
    {
        public static tbFirma FirmayiGetir(int _nFirmaID)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            SqlCommand cmd = new SqlCommand("select nFirmaID,sKodu,sAciklama,sAdres1,sAdres2,sSemt,sIl,sOzelNot,sVergiNo from tbFirma where nFirmaID = " + _nFirmaID, con);
            DataTable _table = new DataTable("dsTablo");
            _table.Columns.Add("nFirmaID", typeof(int));
            _table.Columns.Add("sKodu", typeof(string));
            _table.Columns.Add("sAciklama", typeof(string));
            _table.Columns.Add("sAdres1", typeof(string));
            _table.Columns.Add("sAdres2", typeof(string));
            _table.Columns.Add("sSemt", typeof(string));
            _table.Columns.Add("sIl", typeof(string));
            _table.Columns.Add("sOzelNot", typeof(string));
            _table.Columns.Add("sVergiNo", typeof(string));
            _table.Columns.Add("sAdres1", typeof(string));
            _table.Columns.Add("sAdres2", typeof(string));
            tbFirma _firma = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFirma> listFirma = new List<tbFirma>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _firma = new tbFirma();
                    int nFirmaID = 0;
                    if (int.TryParse(_table.Rows[i][0].ToString(), out nFirmaID))
                        _firma.nFirmaID = nFirmaID;
                    else
                        _firma.nFirmaID = _nFirmaID;
                    _firma.sKodu = _table.Rows[i][1].ToString();
                    _firma.sAciklama = _table.Rows[i][2].ToString();
                    _firma.Adres1 = _table.Rows[i][3].ToString();
                    _firma.Adres2 = _table.Rows[i][4].ToString();
                    _firma.Semt = _table.Rows[i][5].ToString();
                    _firma.Il = _table.Rows[i][6].ToString();
                    _firma.sOzelNot = _table.Rows[i][7].ToString();
                    _firma.sVergiNo = _table.Rows[i][8].ToString();
                    _firma.Adres1 = _table.Rows[i][9].ToString();
                    _firma.Adres2 = _table.Rows[i][10].ToString();
                    listFirma.Add(_firma);
                }
            }
            catch
            {
                return null;
            }
            finally { con.Close(); }
            if (listFirma.Count > 0)
                return listFirma[0];
            else
                return null;
        }

        public static List<tbFirma> FirmalariGetir(string _sKod, string _sAdSoyad, string _vergiNo)
        {
            List<tbFirma> listFirma = null;
            using (SqlConnection con = new SqlConnection(clsGenel._cnstring))
            {
                StringBuilder _stringBuilder = new StringBuilder();
                _stringBuilder.Append("select nFirmaID,sKodu,sAciklama,sAdres1,sAdres2,sSemt,sIl,sOzelNot,sVergiNo,sAdres1,sAdres2 from tbFirma");
                bool _where = false;
                if (!string.IsNullOrEmpty(_sKod))
                {
                    if (!_where)
                    {
                        _stringBuilder.Append(" where");
                        _where = true;
                    }
                    _stringBuilder.Append(" sKodu = '" + _sKod + "'");
                }
                if (!string.IsNullOrEmpty(_sAdSoyad))
                {
                    if (!_where)
                        _stringBuilder.Append(" where");
                    else
                        _stringBuilder.Append(" and");
                    _stringBuilder.Append(" sAciklama like '%" + _sAdSoyad + "%'");
                }
                if (!string.IsNullOrEmpty(_vergiNo))
                {
                    if (!_where)
                        _stringBuilder.Append(" where");
                    else
                        _stringBuilder.Append(" and");
                    _stringBuilder.Append(" sVergiNo like '%" + _vergiNo + "%'");
                }
                SqlCommand cmd = new SqlCommand(_stringBuilder.ToString(), con);
                DataTable _table = new DataTable("dsTablo");
                _table.Columns.Add("nFirmaID", typeof(int));
                _table.Columns.Add("sKodu", typeof(string));
                _table.Columns.Add("sAciklama", typeof(string));
                _table.Columns.Add("sAdres1", typeof(string));
                _table.Columns.Add("sAdres2", typeof(string));
                _table.Columns.Add("sSemt", typeof(string));
                _table.Columns.Add("sIl", typeof(string));
                _table.Columns.Add("sOzelNot", typeof(string));
                _table.Columns.Add("sVergiNo", typeof(string));
                tbFirma _firma = null;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                listFirma = new List<tbFirma>();
                try
                {
                    con.Open();
                    da.Fill(_table);
                    for (int i = 0; i < _table.Rows.Count; i++)
                    {
                        _firma = new tbFirma();
                        int nFirmaID = 0;
                        if (int.TryParse(_table.Rows[i][0].ToString(), out nFirmaID))
                            _firma.nFirmaID = nFirmaID;
                        else
                            _firma.nFirmaID = nFirmaID;
                        _firma.sKodu = _table.Rows[i][1].ToString();
                        _firma.sAciklama = _table.Rows[i][2].ToString();
                        _firma.Adres1 = _table.Rows[i][3].ToString();
                        _firma.Adres2 = _table.Rows[i][4].ToString();
                        _firma.Semt = _table.Rows[i][5].ToString();
                        _firma.Il = _table.Rows[i][6].ToString();
                        _firma.sOzelNot = _table.Rows[i][7].ToString();
                        _firma.sVergiNo = _table.Rows[i][8].ToString();
                        listFirma.Add(_firma);
                    }
                }
                catch
                {
                    return null;
                }
                finally { con.Close(); }
                if (listFirma.Count > 0)
                    return listFirma;
                else
                    return null;
            }
        }
    }
}
