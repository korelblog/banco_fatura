﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DataSiparis
{
    public class tbFaturaDetay
    {
        private string _invoiceNumber;
        public string InvoiceNumber { get { return _invoiceNumber; } set { _invoiceNumber = value; } }

        private string _sKodu;
        public string sKodu { get { return _sKodu; } set { _sKodu = value; } }

        public string _sAciklama;
        public string sAciklama { get { return _sAciklama; } set { _sAciklama = value; } }

        public decimal _lCikisMiktar1;
        public decimal lCikisMiktar1 { get { return _lCikisMiktar1; } set { _lCikisMiktar1 = value; } }

        public decimal _lCikisFiyat1;
        public decimal lCikisFiyat1 { get { return _lCikisFiyat1; } set { _lCikisFiyat1 = value; } }

        public decimal _lCikisTutar;
        public decimal lCikisTutar { get { return _lCikisTutar; } set { _lCikisTutar = value; } }

        public decimal _nKdvOrani;
        public decimal nKdvOrani { get { return _nKdvOrani; } set { _nKdvOrani = value; } }
    }

    public class tbFaturaDetayProvider
    {
        public static List<tbFaturaDetay> FaturaDetaylariniGetir(string _invoiceNumber)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from tbEFaturaDetay ";
            if (!string.IsNullOrEmpty(_invoiceNumber))
                _command += " where InvoiceNumber like '%" + _invoiceNumber + "%'";
            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 300;
            DataTable _table = new DataTable("dsTablo");
            tbFaturaDetay _faturaDetay = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFaturaDetay> listFaturaDetay = new List<tbFaturaDetay>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _faturaDetay = new tbFaturaDetay();
                    _faturaDetay.InvoiceNumber = _table.Rows[i][0].ToString();
                    _faturaDetay.sKodu = _table.Rows[i][1].ToString();
                    _faturaDetay.sAciklama = _table.Rows[i][2].ToString();

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][3].ToString(), out _miktar))
                        _faturaDetay.lCikisMiktar1 = _miktar;
                    else
                        _faturaDetay.lCikisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _faturaDetay.nKdvOrani = _miktar;
                    else
                        _faturaDetay.nKdvOrani = 0;
                    if (_faturaDetay.lCikisMiktar1 > 0)
                    {
                        if (decimal.TryParse(_table.Rows[i][4].ToString(), out _miktar))
                            _faturaDetay.lCikisFiyat1 = _miktar;
                        else
                            _faturaDetay.lCikisFiyat1 = 0;
                        if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                            _faturaDetay.lCikisTutar = _miktar;
                        else
                            _faturaDetay.lCikisTutar = 0;

                        if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                            _faturaDetay.lCikisTutar = _miktar;
                        else
                            _faturaDetay.lCikisTutar = 0;
                    }
                    else
                    {
                        if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                            _faturaDetay.lCikisMiktar1 = _miktar < 0 ? ((-1) * _miktar) : _miktar;
                        else
                            _faturaDetay.lCikisMiktar1 = 0;

                        if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                            _faturaDetay.lCikisFiyat1 = _miktar < 0 ? ((-1) * _miktar) : _miktar;
                        else
                            _faturaDetay.lCikisFiyat1 = 0;
                        if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                            _faturaDetay.lCikisTutar = _miktar < 0 ? ((-1) * _miktar) : _miktar;
                        else
                            _faturaDetay.lCikisTutar = 0;
                    }


                    listFaturaDetay.Add(_faturaDetay);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaDetay;
        }

        public static List<tbFaturaDetay> PFFaturaDetayGetir(string _invoiceNumber)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from PFFaturaDetay where ";
            if (!string.IsNullOrEmpty(_invoiceNumber))
                _command += " InvoiceNumber like '%" + _invoiceNumber + "%'";
            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 300;
            DataTable _table = new DataTable("dsTablo");
            tbFaturaDetay _faturaDetay = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFaturaDetay> listFaturaDetay = new List<tbFaturaDetay>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _faturaDetay = new tbFaturaDetay();
                    _faturaDetay.InvoiceNumber = _table.Rows[i][0].ToString();
                    _faturaDetay.sKodu = _table.Rows[i][1].ToString();
                    _faturaDetay.sAciklama = _table.Rows[i][2].ToString();

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][3].ToString(), out _miktar))
                        _faturaDetay.lCikisMiktar1 = _miktar;
                    else
                        _faturaDetay.lCikisMiktar1 = 0;
                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _faturaDetay.nKdvOrani = _miktar;
                    else
                        _faturaDetay.nKdvOrani = 0;
                    if (_faturaDetay.lCikisMiktar1 > 0)
                    {
                        if (decimal.TryParse(_table.Rows[i][4].ToString(), out _miktar))
                            _faturaDetay.lCikisFiyat1 = _miktar;
                        else
                            _faturaDetay.lCikisFiyat1 = 0;
                        if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                            _faturaDetay.lCikisTutar = _miktar;
                        else
                            _faturaDetay.lCikisTutar = 0;

                        if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                            _faturaDetay.lCikisTutar = _miktar;
                        else
                            _faturaDetay.lCikisTutar = 0;
                    }
                    else
                    {
                        if (decimal.TryParse(_table.Rows[i][7].ToString(), out _miktar))
                            _faturaDetay.lCikisMiktar1 = _miktar < 0 ? ((-1)*_miktar) : _miktar;
                        else
                            _faturaDetay.lCikisMiktar1 = 0;

                        if (decimal.TryParse(_table.Rows[i][8].ToString(), out _miktar))
                            _faturaDetay.lCikisFiyat1 = _miktar < 0 ? ((-1)*_miktar) : _miktar;
                        else
                            _faturaDetay.lCikisFiyat1 = 0;
                        if (decimal.TryParse(_table.Rows[i][9].ToString(), out _miktar))
                            _faturaDetay.lCikisTutar = _miktar < 0 ? ((-1)*_miktar) : _miktar;
                        else
                            _faturaDetay.lCikisTutar = 0;
                    }


                    listFaturaDetay.Add(_faturaDetay);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaDetay;
        }

        public static List<tbFaturaDetay> PKFaturaDetayGetir(string _invoiceNumber)
        {
            SqlConnection con = new SqlConnection(clsGenel._cnstring);
            string _command = @"set dateformat dmy
select * from PKFaturaDetay where ";
            if (!string.IsNullOrEmpty(_invoiceNumber))
                _command += " InvoiceNumber like '%" + _invoiceNumber + "%'";
            SqlCommand cmd = new SqlCommand(_command, con);
            cmd.CommandTimeout = 300;
            DataTable _table = new DataTable("dsTablo");
            tbFaturaDetay _faturaDetay = null;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            List<tbFaturaDetay> listFaturaDetay = new List<tbFaturaDetay>();
            try
            {
                con.Open();
                da.Fill(_table);
                for (int i = 0; i < _table.Rows.Count; i++)
                {
                    _faturaDetay = new tbFaturaDetay();
                    _faturaDetay.InvoiceNumber = _table.Rows[i][0].ToString();
                    _faturaDetay.sKodu = _table.Rows[i][1].ToString();
                    _faturaDetay.sAciklama = _table.Rows[i][2].ToString();

                    decimal _miktar = 0;
                    if (decimal.TryParse(_table.Rows[i][3].ToString(), out _miktar))
                        _faturaDetay.lCikisMiktar1 = _miktar;
                    else
                        _faturaDetay.lCikisMiktar1 = 0;

                    if (decimal.TryParse(_table.Rows[i][4].ToString(), out _miktar))
                        _faturaDetay.lCikisFiyat1 = _miktar;
                    else
                        _faturaDetay.lCikisFiyat1 = 0;

                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _faturaDetay.lCikisTutar = _miktar;
                    else
                        _faturaDetay.lCikisTutar = 0;

                    if (decimal.TryParse(_table.Rows[i][5].ToString(), out _miktar))
                        _faturaDetay.lCikisTutar = _miktar;
                    else
                        _faturaDetay.lCikisTutar = 0;

                    if (decimal.TryParse(_table.Rows[i][6].ToString(), out _miktar))
                        _faturaDetay.nKdvOrani = _miktar;
                    else
                        _faturaDetay.nKdvOrani = 0;
                    listFaturaDetay.Add(_faturaDetay);
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }
            return listFaturaDetay;
        }
    }
}
